.. _HowTosModuleCreation:

*******************************************************************
How to create a module ?
*******************************************************************


In *Sight*, there are mostly two types of modules:

- the modules containing only resource files, for instance XML configurations
- the modules containing services or other C++ code

It is possible to contain at the same time configurations and services (or C++ code), but for the sake of clarity and
reusability we recommend to separate the two.

Resource modules
--------------------------

These modules do not contain C++ code, they only contain resource files and the ``CMakeLists.txt`` file.
In the module folder, there is only the ``CMakeLists.txt`` files and the *rc* folder. The content of
the *rc* folder will just be copied into the build directory when building, and into the install directory
when installing.

CMakeLists.txt
~~~~~~~~~~~~~~~~

The CMakeLists.txt uses the ``sight_add_target()`` macro to define a module target.

Then, dependencies are explicitly defined to ensure the modules used in the configuration are actually built.

Example:

.. code-block:: cmake

    sight_add_target( config_ui_data_manager TYPE MODULE )

    # required modules used in the plugin.xml
    add_dependencies( config_ui_data_manager
                      module_ui_base
                      module_ui_qt
                      module_data
                      module_ui_icons
                      module_service
                      data
    )

Configurations
~~~~~~~~~~~~~~~

A module can contain several application configurations, by implementing the extension point
``sight::app::extension::config`` in its ``plugin.xml`` description file.

.. code-block:: xml

    <plugin id="dataManagerConfig" >

        <requirement id="sight::module::ui" />
        <requirement id="sight::module::service" />

        <!-- ... extensions ... -->

    </plugin>

The ``<requirement>`` tags contain the modules that must be started before to start your module
(see https://sight.pages.ircad.fr/sight/group__requirement.html). Indeed some modules perform important sub-systems
initialization, and that must be done before everything.

Then the extensions are defined. There are different types of extensions, the most common are:

- ``sight::app::extension::config`` to define configurations for applications (see :ref:`TutorialsTuto01Basic`)
- ``sight::activity::extension`` to define activities
- ``sight::service::extension::config`` to define configurations of services (mostly used to configure readers/writers)
- ``sight::service::extension::factory`` to define services

.. TODO add links to documentation for the extensions

.. note::

    To separate the configuration in several files, you can use ``<xi:include href="..." />``

Code modules
----------------

Those kind of module contain service header and source files. It must also contain a ``plugin`` class used to register
the module.

The ``plugin.hpp`` in the *include* folder should look like:

.. code-block:: cpp

    #pragma once

    #include <core/runtime/plugin.hpp>

    namespace my_module
    {

    class MYMODULE_CLASS_API plugin : public sight::core::runtime::plugin
    {

    public:

        /// plugin destructor
        ~plugin() noexcept;

        /// This method is used by runtime to start the module.
        void start();

        /// This method is used by runtime to stop the module.
        void stop() noexcept;

        /// This method is used by runtime to initialize the module.
        void initialize();

        /// This method is used by runtime to uninitialize the module.
        void uninitialize() noexcept;

    };

    } // namespace my_module


The ``plugin.cpp`` in the *src* folder should be like:

.. code-block:: cpp

    #include "my_module/plugin.hpp"

    namespace my_module
    {

    //-----------------------------------------------------------------------------

    SIGHT_REGISTER_PLUGIN("my_module::plugin");

    //-----------------------------------------------------------------------------

    plugin::~plugin() noexcept
    {
    }

    //-----------------------------------------------------------------------------

    void plugin::start()
    {
    }

    //-----------------------------------------------------------------------------

    void plugin::stop() noexcept
    {
    }

    //-----------------------------------------------------------------------------

    void plugin::initialize()
    {
    }

    //-----------------------------------------------------------------------------

    void plugin::uninitialize() noexcept
    {
    }

    //-----------------------------------------------------------------------------

    } // namespace my_module


.. warning::

    The ``SIGHT_REGISTER_PLUGIN("my_module::plugin");`` is the most important line, it allows to register the module to
    be used in a XML based application.

    **Don't forget to register the module with the correct namespace with '::'.**

The methods ``start()`` and ``stop`` must be implemented but are usually empty. They are called when the application is
started and stopped. The ``initialize()`` method is executed after the *start* of all the modules and ``uninitialize()``
before the *stop*.

CMakeLists.txt
~~~~~~~~~~~~~~~~

The CMakeLists.txt uses the ``sight_add_target()`` macro to define a module target.

Then, compile time dependencies may explicitly be defined to build the module code.

Example:

.. code-block:: cmake

    sight_add_target( my_module TYPE MODULE )

    # required modules used in the plugin.xml
    target_link_libraries( my_module PUBLIC core service)

    # 3rd part dependencies
    target_link_libraries( Qt5 PRIVATE Qt5::Core)
