.. _HowTosServiceCreation:

*************************
How to create a service ?
*************************

Implementation
===============
A service is a C++ class inherited from ``sight::service::base``. It will implement at least the following methods:

- ``configuring(const config_t& _config)``: parses the configuration (usually from the XML)
- ``starting()``: initializes the service (creates the gui, retrieves/initializes the data, ...)
- ``updating()``: processes
- ``stopping()``: clears all (clears the gui, releases the data, ...)

These methods are called by the *configure()*, *start()*, *update()* and *stop()* slots of the base class
``sight::service::base``.

For the example, we will create a service ``mesher`` in a module ``operators``. The service will have a
``sight::data::image`` as input and a ``sight::data::mesh`` as output.

The header file ``mesher.hpp`` should be placed in the folder ``<src_dir>/modules/operators/``:

.. code-block:: cpp

    #pragma once

    #include "modules/operators/config.hpp"

    #include "operator.hpp"
    #include <sight/data/image.hpp>
    #include <sight/data/mesh.hpp>

    namespace operators
    {

    /**
     * @brief Generate a ModelSeries from an ImageSeries
     *
     * @section XML XML Configuration
     *
     * @code{.xml}
         <service type="::operators::mesher">
             <in key="image" uid="..." />
             <out key="mesh" uid="..." />
             <generate_normals>true</generate_normals>
         </service>
       @endcode
     * @subsection Input Input
     * - \b image [sight::data::image]: image used to generate the mesh.
     * @subsection Output Output
     * - \b mesh [sight::data::mesh]: generated mesh.
     * @subsection Configuration Configuration
     * - \b generate_normals (optional, default: false): if true, the mesh normals will be generated.
     */
    class OPERATORS_CLASS_API mesher : public sight::service::operator
    {
    public:
        SIGHT_DECLARE_SERVICE(mesher, sight::service::operator);

        /// Constructor.
        OPERATORS_API mesher() noexcept;

        /// Destructor.
        OPERATORS_API ~mesher() noexcept override;

    protected:

        /// Parse the configuration to retrieve 'generate_normals' option.
        OPERATORS_API void configuring(const config_t& _config) override;

        /// Do nothing.
        OPERATORS_API void starting() override;

        /// Generate the mesh.
        OPERATORS_API void updating() override;

        /// Unregister the output
        OPERATORS_API void stopping() override;

    private:

        /// Option to generate or not the normals.
        bool m_generate_normals{false};

        /// Input image declaration
        sight::data::ptr<sight::data::image, sight::data::Access::in> m_image {this, "image"};

        /// Output mesh declaration
        sight::data::ptr<sight::data::mesh, sight::data::Access::out> m_mesh {this, "mesh"};
    };

    } // namespace operators


The file ``config.hpp`` is automatically generated, it provides ``OPERATORS_CLASS_API`` and ``OPERATORS_API``
macros that allow to expose symbols in the shared library.

.. warning::

    **The doxygen section of the service is very important for users** (see :ref:`Documentation` Rule: 43).
    The `Input`, `Output` and `InOut` sections must follow the defined format:

        \\- \\b ``key_name`` [``object_type``]: ``description``

    - *key_name*: the name of the key (used to retrieve the object in the service)
    - *object_type*: class of the object
    - *description*: the purpose of this input/output


The source file ``mesher.cpp`` should be placed in the folder ``<src_dir>/modules/operators/``:

.. code-block:: cpp


    #include "operators/mesher.hpp"

    namespace operators
    {

    //-----------------------------------------------------------------------------

    mesher::mesher() noexcept
    {

    }

    //-----------------------------------------------------------------------------

    mesher::~mesher() noexcept
    {
    }

    //-----------------------------------------------------------------------------

    void mesher::configuring(const config_t& _config)
    {
        m_generate_normals = _config.get<bool>("generate_normals", false);
    }

    //------------------------------------------------------------------------------

    void mesher::starting()
    {

    }

    //------------------------------------------------------------------------------

    void mesher::updating()
    {
        // retrieve the image
        auto image = m_image.lock();
        SIGHT_ASSERT("Input image is not defined", image);

        auto mesh = std::make_shared<sight::data::mesh>();

        // generate the mesh
        // ...

        if (m_generate_normals)
        {
            // ...
        }

        // set the output mesh to be available in the configuration
        m_mesh = mesh;
    }

    //------------------------------------------------------------------------------

    void mesher::stopping()
    {
        // unregister output mesh
        m_mesh.reset();
    }

    }// namespace operators


Usage
========

This service is defined in xml configuration like:

.. code-block:: xml

    <extension implements="sight::app::extension::config">
    <!-- ..... -->

    <object uid="image" type="sight::data::image" />
    <object uid="generated_mesh" type="sight::data::mesh" src="deferred" />

    <!-- ..... -->

     <service uid="mesher" type="operators::mesher">
         <in key="image" uid="image" />
         <out key="mesh" uid="generated_mesh" />
         <generate_normals>true</generate_normals>
     </service>

     <!-- ..... -->

     <start uid="mesher" />
     <update uid="mesher" />


You can also use this service in C++:

.. code-block:: cpp

    sight::service::bases::config_t config;
    config.add("generate_normals", "true");

    sight::service::base::sptr mesher = sight::service::add("operators::mesher");
    mesher->set_input(image, "image") // use to register the input
    mesher->set_config(config);
    mesher->configure();
    mesher->start();
    mesher->update();
    sight::data::mesh::sptr obj = mesher->getOutput<sight::data::mesh >("mesh");
    mesher->stop();
    sight::service::remove( mesher );


Connection
===========

It should be necessary to reimplement ``auto_connections()`` if you want to automatically connect the input data
signals to the service. In our example, we want to call ``update()`` method when the image is modified.

.. code-block:: cpp

    sight::service::base::connections_t mesher::auto_connections() const
    {
        return {
            {"image", data::object::signals::MODIFIED, sight::service::slots::UPDATE}
        };
    }

It connects the ``signals::MODIFIED`` (``"modified"``) signal of the image with the key ``"image"`` with the
service slot registered as ``slots::UPDATE`` (``"update"``).

To enable this connection at runtime, you have to add ``auto_connect="true"`` in the XML declaration of the service.

.. code-block:: xml

    <service uid="mesher" type="::operators::mesher">
        <in key="image" uid="image" auto_connect="true" />
        <out key="mesh" uid="generated_mesh" />
        <generate_normals>true</generate_normals>
    </service>

In C++ you must register the image with a third parameter as "true":

.. code-block:: cpp

    sight::service::base::sptr mesher = sight::service::add("operators::mesher");
    mesher->set_input(image, "image", true) // use to register the input
    // ...

.. tip::

    If you have some problem to use your service in your application, see :ref:`HowTosTroubleshootingServiceNotFound`.
