.. _HowTosCMake:

How to use CMake with sight ?
===============================

Introduction
-------------

Sight and its dependencies are built with `CMake <http://www.cmake.org/>`_ .
Note that the minimal required version of CMake is 3.13.

.. _HowTosCMakeLists:

CMakeLists.txt
---------------

Each target (application, module, library) contains one ``CMakeLists.txt``. We use
standard CMake commands and properties, and a few custom macros.

The *CMakeLists.txt* should contain the macro *sight_add_target()* which will create the correct CMake target depending
on the TYPE parameter:

- APP for a XML-based application
- MODULE for a module
- LIBRARY for a library
- EXECUTABLE for an executable

Then you can use classic CMake commands to link with others sight libraries or with external libraries.

Here is an example of CMakeLists.txt of sample module which links with Qt5:

.. code-block:: cmake

    sight_add_target(module_foo TYPE MODULE)

    find_package(Qt5 COMPONENTS Core Gui Widgets REQUIRED)

    target_link_libraries(module_foo PRIVATE Qt5::Core Qt5::Gui Qt5::Widgets)

    set_target_properties(module_foo PROPERTIES AUTOMOC TRUE)

The next lines allows to compile with the support of some external libraries (sight-deps), in this example this is Qt.
The first thing to do is to discover where Qt is installed. This is done with the regular CMake command
``find_package(The_lib COMPONENTS The_component)``.

Eventually, you can also add custom properties to your target with ``set_target_properties``.

Modules without C++ code, like XML-based applications or activities need to ensure that the necessary modules are
built. To achieve that, we use the ``add_dependencies()`` CMake command.

Last, in XML-based in applications, you will find the line:

.. code-block:: cmake

    module_param(module_app PARAM_LIST config PARAM_VALUES tutoBasicConfig)

This custom CMake macro allows to pass parameters to a module. The parameters are defined like:

.. code-block:: cmake

    module_param(<module>
                PARAM_LIST <param1_name> <param2_name> <param3_name>
                PARAM_VALUES <param1_value> <param2_value> <param3_value>
                )

These parameters can be retrieved in the ``plugin.cpp`` like:

.. code-block:: cpp

    void plugin::start()
    {
        if( this->module()->has_parameter("param1_name") )
        {
            const std::string param1Value = this->module()->get_parameter_value("param1_name");
        }
        if( this->module()->has_parameter("param2_name") )
        {
            const std::string param2Value = this->module()->get_parameter_value("param2_name");
        }
        // ...
    }

For the application, this macro defines the main configuration to launch when the application is started.
