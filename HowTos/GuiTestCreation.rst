*************************
How to create a GUI test?
*************************

Writing unit tests is a good practice, but might be not sufficient. Graphical user interface tests can be a good supplement to improve test coverage.

Create a new class that inherits from sight::ui::test::ITest
------------------------------------------------------------

This new class will typically be located in a subdirectory of the tested target, for instance, GUI tests for sight_viewer
are located at "apps/sight_viewer/test/ui". This class must inherit from ``sight::ui::test::base``, which already implements
the setUp and tearDown methods required by CppUnit to correctly handle the application resources.
This class must implement the getProfilePath method, which is the path to the profile.xml file relative to the build
folder. For instance, for sight_viewer it would be "share/sight/sight_viewer/profile.xml".
Don't forget to use the CppUnit macros in order to register your new test. You should put only one test per class, so
that they can be launched in different processes. Sight doesn't support launching a profile several time in one process.

Write the GUI test body
-----------------------

If your test class inherits from ``sight::ui::test::ITest``, then it is sufficient to call the ``ITest::start`` method with
the name of the test and the code. A minimal test example would be:

.. code-block:: cpp

    void GuiTest::test()
    {
        start("testName",
            [](sight::ui::test::Tester& tester)
            {
                // The most important part: This is were the actual test code must be written.
            });
    }

This test checks whether the main window shows up and then closes when using the application. A pretty simple test
indeed, the test can be enhanced by adding code in the lambda passed to the Tester::start method call.

Find GUI components in the window
---------------------------------

The goal of a GUI test is to interact with the interface by simulating mouse clicks and key presses for example. To do
so, one must be able to get the GUI components to be interacted with in the first place.
``Tester::take`` allows one to wait for a component to appears. It must be called with the name of that component and a
lambda which returns the awaited component or nullptr if it didn't appear. Most of the time, to get a component will
imply to use the ``QObject::findChild`` method on the main window. This method must be called with the object name of
the component which must be get: the name of the component can be deduced from the plugin.xml of the
application. This object name is typically the ``uid`` of the Sight service. If it is a subcomponent, it would be the
``uid`` of the parent service, followed with a `/` and the name of the subcomponent. For example, let's assume one want
to get the "Load series" button of the sight_viewer interface. It is located here:

.. code-block:: xml

    <service uid="toolBarView" type="sight::module::ui::tool_bar" >
        <gui>
            <layout>
                <menuItem name="Load series" icon="sight::module::ui::flaticons/BlueLoad.svg" /> <!-- Here! -->
                <menuItem name="Open PACS" icon="sight::module::ui::flaticons/GreenServerConnected.svg" style="check" />
                <separator/>
                <!-- ... -->
            </layout>
        </gui>
        <registry>
            <menuItem sid="openSeriesDBAct" start="true" />
            <menuItem sid="pacsAct" start="true" />
            <!-- ... -->
        </registry>
    </service>

This button name is "Load series" and it is a subcomponent of the "toolBarView" service. So, its object
name is "toolBarView/Load series". To get this button, one may write this:

.. code-block:: cpp

    tester.take(
        "Load series button",
        [&tester]() -> QObject*
        {
            QAction* action = tester.getMainWindow()->findChild<QAction*>("toolBarView/Load series");
            return sight::ui::test::Tester::getWidgetFromAction(action);
        });

It is worth noting that tool_bar menuItem are actually QAction, which is some kind of "abstract" component which can't
be interacted with. ``Tester::getWidgetFromAction`` will allow to get the underlying "concrete" component which can be
interacted with.

This method can also be called with another lambda which checks if that component checks a condition. That lambda gets
the component and must return a boolean. It can be useful, for example, to wait for a dialog window to be visible and
fully operational before interacting with it.

.. code-block:: cpp

    tester.take<QWidget*>(
        "dialog window",
        []() -> QWidget* {return qApp->activeModalWidget();},
        [](QWidget* obj) -> bool {return obj->isVisible();}
    );

``QApplication::activeModalWidget`` can be used to get the current dialog window.

One may see that the templated version of the method is used here. When using the templated version, the component will
be casted to the templated type before being passed as an argument to the second lambda if the cast succeeds, else
simply discards the component. This is mostly for convenience, to avoid having to do a qobject_cast in the provided
lambda and having to deal with the failed cast case.

The tester will wait up to 5 seconds by default for the component to appear, this might be modified with a third
parameter, the number of milliseconds to wait for the component. It can be provided as the multiple of the default
timeout, for example ``10000`` and ``sight::ui::test::Tester::DEFAULT_TIMEOUT*2`` are equivalent.

.. code-block:: cpp

    tester.take(
        "Load series button",
        [&tester]() -> QObject*
        {
            QAction* action = tester.getMainWindow()->findChild<QAction*>("toolBarView/Load series");
            return sight::ui::test::Tester::getWidgetFromAction(action);
        }, sight::ui::test::alwaysTrue, sight::ui::test::Tester::DEFAULT_TIMEOUT*2);

This timeout is the fourth parameter, which means you must provide the third one, the condition lambda, in order to call
that method. ``sight::ui::test::alwaysTrue`` can be used to always accept the component.

When using ``Tester::take`` and the component doesn't appear (or appears but doesn't verify the condition of the
condition lambda) and the timeout is reached, then the test fails. There are two other variants. The first one is
``Tester::maybeTake``. It is similar to ``Tester::take``, but the test won't fail if the component doesn't appear.
Instead, the next commands are ignored, until the next ``Tester::take``. This can be useful, for example, when a dialog
might appear in certain circonstances and must be dealt with, but the test should continue as normal if it doesn't
appear. For example, the following will wait for a dialog window and click on a button if it appears, and simply
continue if it doesn't appear.

.. code-block:: cpp

    tester.maybeTake(
        "dialog window which may not appear",
        []() -> QObject* {return qApp->activeModalWidget();});
    tester.yields("ok button", [](QObject* old) -> QObject* {return old->findChildren<QPushButton*>()[0];});
    tester.interact(std::make_unique<sight::ui::test::MouseClick>());

``Tester::yields`` is close in meaning to ``Tester::take``, with a few differences: Firstly, the first lambda passed to
the method takes a parameter, which is the current graphic component, the one which is about to be replaced. Secondly,
when a ``Tester::maybeTake`` fails, ``Tester::yields`` won't be executed, as opposed to ``Tester::take``. Finally, the
error message is slightly different if the component doesn't appear, there is an emphasis on the fact that the component
appears thanks to the previous one.

Interact with GUI components
----------------------------

The ``Tester::take``, ``Tester::maybeTake`` and ``Tester::yields`` methods internally set the current graphic component.
This current graphic component is the argument given to the lambda of the ``Tester::yields`` method, but is also the
target of the interactions of ``Tester::interact``.
There are four Interaction subclasses which can be used as arguments to the ``Tester::interact`` method: ``MouseClick``,
``MouseDrag``, ``KeyboardClick`` and ``KeyboardSequence``. Their names should be self-explaining.
A few examples:

.. code-block:: cpp

    // A mouse click with the left button on the center of the widget:
    tester.interact(std::make_unique<sight::ui::test::MouseClick>());
    // A mouse drag from the left to the right of the widget while holding the left button:
    QWidget* widget = tester.get<QWidget*>();
    tester.interact(std::make_unique<sight::ui::test::MouseDrag>(sight::ui::test::Tester::leftOf(widget), sight::ui::test::Tester::rightOf(widget));
    // Typing "Hello world!", presumably in a text box
    tester.interact(std::make_unique<sight::ui::test::KeyboardSequence>("Hello world!");
    // Tap on the backspace key
    tester.interact(std::make_unique<sight::ui::test::KeyboardClick>(Qt::Key::Key_Backspace);

Verify assertions
-----------------

Naturally, an important aspect of the test is to check whether the feedbacks of the graphical user interface after the
interaction are correct. For example: Is the text contained in the label correct? Did the dialog window close when the
"OK" button was clicked? Was a file created when the "Save" button is clicked, does it have the right format? Does the
image looks like what it is supposed to look like?
The method ``Tester::doubt`` exists to verify assertions. Its first parameter is the name of the assertion and the
second is a lambda which takes the current graphic component and should return true if the assertion is verified and
false if not. If the assertion is false, ``Tester::doubt`` will wait up to 5 seconds by default for it to become true.
This timeout may be modified with a third parameter, like in the ``Tester::take``, ``Tester::maybeTake`` and
``Tester::yields`` methods.
A few examples:

.. code-block:: cpp

    // Check whether a file was created
    tester.doubt("the file was created", [](QObject*) -> bool {return std::filesystem::exists("the_file.txt");});
    // Check whether two images (an actual and a reference) are similar
    compareImages("referenceImage.png", "actualImage.png");

``ITest::compareImages`` is a convenient method to check whether two images are similar. It uses the
``Tester::compareImagesMSE``, ``Tester::compareImagesHistogram``, ``Tester::compareImagesCorrelation`` and
``Tester::compareImagesVoodoo`` methods behind the scenes to achieve this, those methods can be called directly instead.

In order to test assertions on a graphical component, the templated version may be used to cast that component to a
particular type. If the cast fails, the whole test fails as well.
A few examples:

.. code-block:: cpp

    // Check whether the text contained in a label is correct
    tester.doubt<QLabel*>("the text contained in a label is correct",  [](QLabel* label) -> bool {return label->text() == "correct";});
    // Check whether the a dialog window is closed
    tester.doubt<QWidget*>("the dialog window is closed", [](QWidget* dialog) -> bool {return !dialog->isVisible();});
