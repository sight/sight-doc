What is sight?
===============

The framework Sight (Surgical Image Guidance and Health-care Toolkit) is an open-source
framework, developed by IRCAD (research institute against cancer and disease).
The purpose of Sight is to ease the creation of applications in the medical imaging field.
Therefore it provides features like digital image
processing in 2D and 3D, visualization or simulation of medical interactions.

Building an application with Sight only requires to write one or several XML files.
Its functionalities can be also extended by writing new components in C++,
which is the coding language of the framework.

What does sight mean?
======================

Sight means Surgical Image Guidance and Health-care Toolkit.

What are the features of sight?
=======================================

Sight is based on a component architecture composed of C++ libraries.
The three main concepts of the architecture, explained in the following sections, are:

-  object-service concept
-  component approach
-  signal-slot communication

The framework is multi-platform and runs under Windows and Linux.
Building an application with Sight only requires to write one or several XML files.
Its functionalities can be also extended by writing new components in C++,
which is the coding language of the framework.


Which platforms does sight run on?
===================================

This framework can run under Windows and Linux.

Where can I find applications developed with sight ?
======================================================

Some tutorials are provided with the framework and you can also build VRRender,
a free visualization software or ARCalibration, a user-friendly camera calibration tool.

Which prerequisites do I need to develop new services and modules ?
=====================================================================

You must have a good knowledge in C++. Concerning the configuration files, they are written in XML.

Is it difficult to compile an application with sight?
======================================================

No, it isn't. You just have to compile all the modules and libraries used by the application.
Please follow the :ref:`installation instructions<Installation>` for your platform.

Why does sight provide a launcher?
===================================

The launcher is used to create the entry point of the application.
It parses the profile and configuration xml file to build it.

How can I debug my program ?
=============================

First, you can watch the log of the application. On the Windows platform,
log messages are saved on filesystem in **sight.0.log** file, in the working directory.
On other systems, they are displayed in the terminal.

You can increase or decrease the log level of a sub-project in the CMake configuration,
by passing an argument on the command line: ``--log--debug``, ``--log--info``, ``--log--warn``, ``--log--error``.

The default value is ``--log--error``, which is enough in most cases. ``--log--warning`` is recommended when you are
developing on a application.
Lower levels are really designed to be activated punctually when debugging a specific piece of code. They are much more
verbose and it is recommended to use a tool like ``glogger`` (Linux/Windows) to filter it out, for instance with the
name of the class or function you are debugging.

.. note::
    Printing many log messages ( by activating trace on all sub-projects for ex. ) can be very time consuming for the application.


Secondly, you can of course compile your application in Debug mode (set **CMAKE_BUILD_TYPE** to "Debug" )
and then debug it using **gdb** (Linux), **QtCreator**, **VSCode** (Linux/Windows), or **Visual Studio** (Windows).

Thirdly, you can manage the program complexity by reducing the number of activated components (in profile.xml)
and the number of created services (in config.xml) to better localize errors.

Fourthly, verify that your profile.xml / plugin.xml and each module plugin.xml are well-formed,
by using xmllint (command line tool provided by libxml2).

I have an assertion/fatal message when I launch my program, any idea to correct the problem ?
===================================================================================================

First, you can read the output message :) and try to solve the problem.
In many cases, there are two kind of problems. The program fails to :

- create the service given in configuration. In this case, four reasons are possible :
    - the name of service implementation in *config.xml* contains typos,
    - the module that contains this service is not activated in the profile,
    - the `plugin.xml` file of the module, that contains this service,
      does not declare the service, or the declaration contains typos.
    - the service is not registered in the Service Factory (forget of macro *SIGHT_REGISTER_SERVICE(...)* in file .cpp)
- manage the configuration of service. In this case, the implementation code
  in .cpp file ( generally configuring() method of service ) does not correspond
  to the description code in config.xml ( Missing arguments, or not well-formed, or mistakes in string parameters ).

Do I need to convert my data object to a ``sight::data::object`` ?
==================================================================================================

Do you need to share this data between services ?

    - If the answer is no, then you don't need to wrap your data.
    - Otherwise, you need to have an object that inherits of sight::data::object.

In this latter case, do you need to share this object between different services which use different third-party libraries,
i.e. for sight::data::image, itk::Image vs vtkImage ?

    - If the answer is yes, then you need create a new object like sight::data::image and a wrapper with
      sight::data::image<=>itk::Image and sight::data::image<=>vtkImage.
    - Otherwise, you can just encapsulated an itk::Image in sight::data::image and create an accessor on it. ( however,
      this choice implies that all applications that use sight::data::image need ITK library to run. )
