************
Introduction
************

Sight, the **S**\urgical **I**\mage **G**\uidance and **H**\ealthcare **T**\oolkit aims to ease the creation of
applications based on medical imaging.
It includes various features such as 2D and 3D digital image processing, visualization, augmented reality and medical
interaction simulation. It runs on Microsoft Windows and Linux, is written in C++, and features rapid interface design
using XML files. It is freely available under the LGPL.

Sight is mainly developed by the Surgical Data Sciences Team of IRCAD France, where it is used everyday to develop
innovative applications for the operating room and medical research centers.

.. toctree::
    :maxdepth: 2

    src/features.rst
    src/applications.rst
    src/samples.rst

