*******
Samples
*******

=========
Tutorials
=========

This is where you should start learning sight. These tutorials assume you have no previous experience except knowledge
of c++ and how to set up a sight project.

---
XML
---

These tutorials explain how to create basic applications and generic scenes in XML.

=========================================================================  ===============================================
 Name                                                                       Concept
=========================================================================  ===============================================
:ref:`tuto01_basic<TutorialsTuto01Basic>`                                  Basic application
:ref:`tuto02_data_service_basic<TutorialsTuto02DataServiceBasic>`          Simple image reading and rendering
:ref:`tuto03_data_service<TutorialsTuto03DataService>`                     Image reading and rendering with signal communication
:ref:`tuto04_signal_slot<TutorialsTuto04SignalSlot>`                       Scenes with 3 point of view synchronized with signal communication
:ref:`tuto05_mesher<TutorialsTuto05Mesher>`                                Simple mesher from a 3D image
:ref:`tuto06_filter<TutorialsTuto06Filter>`                                Simple image filter
:ref:`tuto07_generic_scene<TutorialsTuto07GenericScene>`                   Scene with multi-object rendering
tuto08_mesher_with_generic_scene                                           Scene with multi-object rendering and simple mesher
:ref:`tuto09_matrix_transform_in_gs<TutorialsTuto09MatrixTransformInGS>`   Scene with matrix transformation
tuto10_launch_basic_config                                                 Simple image rendering and a configuration with a scene and the same image
tuto11_picker                                                              Scene with an image picker
tuto12_scene_2d                                                            Scene with a 2D reandering.
Tuto13Multithread                                                          Multi-threading using sight worker
tuto14_gui                                                                 Application containing gui feature (toolbar, menu, action)
tuto15_matrix                                                              Simple matrix transformation applies on a mesh
tuto16_scene_qt_3d                                                         Basic Qt 3D scene displaying a mesh
:ref:`tuto17_simple_ar<TutorialsTuto17SimpleAR>`                           Aplication showing a basic sample of augmented reality
=========================================================================  ===============================================

---
QML
---

These tutorials explain how to create basic applications and generic scenes in QML.

======================================  ================================================================
 Name                                   Concept
======================================  ================================================================
tuto01_basic_qml                         Basic application
tuto06_gui_qml                           Application containing gui feature (toolbar, menu, action)
======================================  ================================================================


========
Examples
========

This is where you can find more advanced applications.

----
Core
----

Samples dedicated to core feature of sight.

======================================  ================================================================
 Name                                   Concept
======================================  ================================================================
ex_activities                            Basic activities
ex_dump                                  Memory management
ex_timeline                              Basic producer-consumer pattern sample with timeLine data
======================================  ================================================================

------
Filter
------

Samples dedicated to filter features or sight (image processing, ...).

======================================  ================================================================
 Name                                   Concept
======================================  ================================================================
ex_detect_cam_motion                     Detects a camera motion
ex_dicom_segmentation                    Performs threshold & meshing of DICOM image
ex_image_substraction                    Subtracts two medical images
ex_registration                          Basic registration between images
ex_rgbd_image_masking                    Generates masks using depth camera (if `SIGHT_ENABLE_REALSENSE` set to `ON`)
======================================  ================================================================

--------
Geometry
--------

Samples dedicated to geometry features of sight (mesh, ...).

======================================  ================================================================
 Name                                   Concept
======================================  ================================================================
ex_rgbd_manual_ar                        Basic AR using depth camera and manual registration (if `SIGHT_ENABLE_REALSENSE` set to `ON`)
ex_solve_pnp                             Register a mesh on a video using cv::SolvePnp (with user interaction)
======================================  ================================================================

---
IO
---

Samples dedicated to input/output features of sight.

======================================  ================================================================
 Name                                   Concept
======================================  ================================================================
ex_frame_by_frame                        Plays a video frame by frame
ex_igtl                                  Some of the *openIGTLink* features
ex_real_sense                            Uses realsense depth camera (if `SIGHT_ENABLE_REALSENSE` set to `ON`)
ex_video_recorder                        Records a video
======================================  ================================================================

----------
Navigation
----------

Samples dedicated to navigation features of sight, (AR, ...).

======================================  ================================================================
 Name                                   Concept
======================================  ================================================================
ex_openvslam                             Slam using OpenVSLAM
ex_stereo_ar                             Basic stereo vision AR
ex_video_tracking                        Basic marker tracking on a video
======================================  ================================================================

---
UI
---

Samples dedicated to user-interface features of sight.

======================================  ================================================================
 Name                                   Concept
======================================  ================================================================
ex_notifications                        Shows notifications popups
ex_parameters                           Shows how to play with multiple UI parameters
======================================  ================================================================
