************
Applications
************

------------
Sight Viewer
------------

**Sight Viewer** is a full-featured medical image and mesh viewer with advanced rendering features such as volume rendering.
It supports most medical image formats, and can also retrieve DICOM files from a PACS. It demonstrates
many useful features of Sight.

.. figure:: ../media/SightViewer01.gif
    :align: center
    :alt: MPR view of a medical 3D image and volume rendering.

    Multi planes reconstruction and volume rendering views of a CT-scan image.

.. figure:: ../media/SightViewer02.gif
    :align: center
    :alt: Volume rendering and transfer function tuning.

    Volume rendering and transfer function tuning.

.. figure:: ../media/mixed_vr_reconstructions.gif
    :align: center
    :alt: Volume rendering mixed with 3D surfacic meshes.

    Volume rendering mixed with 3D surfacic meshes.

------------------
Sight Calibrator
------------------

**Sight Calibrator**  is a user-friendly application to calibrate mono and stereo cameras.
This software is a must-have since camera calibration is a mandatory step in any AR application.

.. figure:: ../media/SightCalibrator01.gif
    :align: center
    :alt: Intrinsic & extrinsic calibration of mono/stereo cameras with live reprojection error display.

    Intrinsic & extrinsic calibration of mono/stereo cameras with live reprojection error display.

