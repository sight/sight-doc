********
Features
********

-------------
Main features
-------------

- 2D/3D visualization of medical images, meshes, and many widgets.
- Import / export medical data from various formats (DICOM, VTK, ...) and sources (files, devices, PACS, ...).
- Playing, recording, processing videos from many different sources (webcams, network streams, Intel RealSense devices, ...).
- Easy GUI configuration and customization (XML description and stylesheets support).
- TimeLine data, allowing to store buffers of various data (video, matrices, markers, etc...).
  These can be used to synchronize these data across time.
- Advanced memory management to support large data. Unused data can be offloaded to disk, saving memory for foreground
  tasks.
- Work session or any part of it, can be saved and restored on disk. The data itself can be encrypted using AES256 to
  ensure a high level of security and privacy


--------------------------
Augmented reality features
--------------------------

- Mono and stereo camera calibration.
- ArUco_ optical markers tracking.
- openIGTLink_ support through clients and servers services.

.. _ArUco: https://sourceforge.net/projects/aruco/
.. _openIGTLink: http://openigtlink.org/


----------------------
Visualization features
----------------------

- regular surfacic meshes rendering for reconstruction,
- 2D and 3D negato medical image display with transfer function support,
- Order-independent transparency (several techniques implemented such as Depth-peeling,
  Weighted-blended order independent transparency, and Hybrid Transparency),
- customizable shaders and parameters edition.
