C++ coding
============

Source and files
-----------------

.. rule :: Files tree

    Source files and public header files shall be placed in the target folder. Private headers and implementation
    should be placed in a ``detail/`` folder.

.. rule :: Files hierarchy

    The file hierarchy should follow the namespace hierarchy. For instance, the implementation of a class
    ``ns1::ns2::service`` should be put in ``ns1/ns2/service.cpp``.

.. rule :: Files extensions

    Header files use the extension ``.hpp``.

    Implementation files use the extension ``.cpp``.

    Files containing implementation of “template” classes use the extension ``.hxx``.

.. recommendation :: Only one class per file

    It is recommended to declare (or to implement) only one class per file. However tiny classes may be declared inside
    the same file.

.. rule :: Includes

    Use the right include directive depending on the context. ``#include "..."`` must be used to import headers from the
    same module, whereas ``#include <...>`` must be used to import headers from other modules.

.. rule :: Include path

    The include path is not an absolute path depending on a local file system. A correct include path does respect the
    letter case of the filenames and folders (since some platforms require it) and uses the character '/' as a separator.

.. rule :: Protection against multiple inclusions

    You must protect your files against multiple inclusions. To this end, use ``#pragma once`` .

    .. code-block :: cpp

        #pragma once

.. recommendation :: Independent headers

    A header should compile alone. All necessary includes should be contained inside the header itself. In the following sample:

    .. code-block :: cpp

        // header.hpp

        class foo
        {
        public:
            std::string m_string;
        }

    you will be forced to include the file in this way to get a successful build :

    .. code-block :: cpp

        // source.hpp

        #include <string>
        #include "header.hpp"

    This is a bad practice, the header should rather be written :

    .. code-block :: cpp

        // header.hpp

        #include <string>

        // header.hpp
        class foo
        {
        public:
            std::string m_string;
        }

    So that people can simply include the header :

    .. code-block :: cpp

        // Source.hpp

        #include "header.hpp"

.. recommendation :: Minimize inclusions

    Try to minimize as much as possible inclusions inside a header file. `Include only what you use
    <https://code.google.com/p/include-what-you-use/>`_.
    Use `forward declarations` when you can (i.e. a type or class structure is not referenced inside the header).
    This will limit dependency between files
    and reduce compile time. Hiding the implementation can also help to minimize inclusions
    (see `Hide implementation`_).

.. rule :: Sort headers inclusions

    You must sort headers in the following order: same module, framework libraries, modules, external libraries,
    standard library. This way, this helps to
    make each header independent. The rule can be broken if a different include order is necessary to get a successful
    build.

    .. code-block :: cpp

        #include "current_module.hpp"

        #include <lib_sample_b/second.hpp>
        #include <lib_sample_a/first.hpp>
        #include <lib_sample_b/sub_module/first.hpp>

        #include <Qt/QtGui>
        #include <vector>
        #include <map>

.. recommendation :: Sort inclusions alphanumerically

    In addition to the previous sort, you may sort includes in alphanumerical order, according to the whole path. Thus
    they will be grouped by module. For a better readability, an empty line can be added between each module.

    .. code-block :: cpp

        #include "currentModule.hpp"

        #include <lib_sample_a/first.hpp>
        #include <lib_sample_b/second.hpp>

        #include <lib_sample_b/sub_module/first.hpp>
        #include <lib_sample_b/sub_module/second.hpp>

        #include <Qt/QtGui>

        #include <map>
        #include <vector>

.. rule :: Deprecated code

    When you want to deprecate a piece of code, you need to warn other developers that the piece of code they are using
    is deprecated.

    1. Use C++17 deprecated keyword (always)

        The general rule is that you need to use the  keyword ``[[deprecated("message")]]``.
        The "message" should indicate **when** the function will be removed.

        We use a time span of **two(2)** major versions before removing previously deprecated function. Ex: you want to
        remove ``functionA()`` and the current release is sight 20.0
        the code will look like:

        .. code-block :: cpp

            [[deprecated("will be removed in sight 22.0, please use functionB().")]]
            void functionA();

        In case of deprecating an entire class, you may need to add the keyword on constructor, or at class level.

    2. Use doxygen tag ``@deprecated`` (when needed)

        In addition of the C++17 keyword, you may also want to add doxygen tag ``@deprecated``,
        like the keyword it must contain the version where the function will be removed.

        .. code-block :: cpp

            /**
            * @brief Computes the A value.
            * @deprecated: will be removed in sight 22.0, please use functionB() instead.
            **/
            [[deprecated("will be removed in sight 22.0, please use functionB().")]]
            void functionA();

    3. Use a logging macro (carefully)

        In some particular cases, you will need to use a macro that prints a deprecation message as output (console or
        SLM.log file).
        Be careful when using macro, because if the function is widely used, it can slow down the whole application.

        That's why deprecated macros are reserved for very specific usages:

        - **slots**: since our slots can be called by an xml configuration, you may need to warn the user using a
          message.
        - **piece of code**: when you want to deprecate just a piece of code (loop, if/else, ...).
        - **Changing XML parameters/config**: when an xml configuration or a parameter is deprecated you may want to
        -  print a message when user use it.

        .. code-block :: cpp

            void negato_2D::new_image_deprecated_slot()
            {
                FW_DEPRECATED_MSG("The 'newImage' slot will be removed in sight 21.0. Call 'update' instead.", "21.0");
                this->newImage();
            }

        For more details about macros, please check the doxygen page of spyLog.hpp


Naming conventions
------------------

.. rule :: Class

    Class names must be written in snake case. It should not repeat a namespace name.
    For instance ``editor::custom_editor`` should be rather called ``editor::custom``.

.. rule :: Interfaces

    We do not require any specific naming for interfaces, abstract or base classes. Use a name that clearly express the
    concept. When you need to avoid redundancy with the namespace, use the term ``base``. Of course it should be unique
    in the namespace. Otherwise, as last resort, you may use ``base`` as a suffix.

    .. code-block :: cpp

        class animal
        {
            //...
        }

        class dog : public animal
        {
            //...
        }

        dog fluffy;

        namespace animal
        {
            class base
            {
            }

            class dog : public base
            {
                //...
            }
        };

        animal::dog fluffy;


.. rule :: File

    The name of the file should be based on the class name defined in it. It must follow the same letter case.

.. rule :: Namespace

    Namespaces must be written in snake case. A comment quoting the namespace must be placed next to the ending '}'.

    .. code-block :: cpp

        namespace namespace_a
        {
        namespace namespace_b
        {
            class sample
            {
            ...
            };
        } // namespace namespace_b
        } // namespace namespace_a

.. rule :: Function and method names

    Functions and methods names must be written in camel case.

.. recommendation :: Correct naming of functions

    Try as much as possible to help the users of your code by using comprehensive names. You may for instance help them
    to indicate the performance cost of a function. A function that executes a search to retrieve an object must not be
    called like a getter. In this case, it is better to call it ``find_objet()`` instead of ``get_object()``.

.. rule :: Variable

    Variable names must be written in snake case. Members of a class are prefixed with a ``m_``.

    .. code-block :: cpp

        class sample_class
        {
        private:
           int m_identifier;
           float m_value;
        };

    Static variables are prefixed with a ``s_``.

    .. code-block :: cpp

        static int s_staticVar;

.. rule :: Constant

    Static constant variables must be written in UPPER_CASE. They do not need the ``s_`` prefix.

    .. code-block :: cpp

        class sample_class
        {
            static const int AAA_BBB_CCC_VALUE = 1;
        };


.. rule :: Type

    Type names must be written in snake case, with the suffix `_t`.

    .. code-block :: cpp

        using custom_t = int;
        using custom_container_t = vector<int>;

.. rule :: Template parameter

    Template parameters must be written in capitals. In addition, they must be short and explicit.

    .. code-block :: cpp

        template< class KEY, class VALUE > class sample_class
        {
            ...
        };

.. rule :: Macro

    Macros without parameters must be written in capitals.

    .. code-block :: cpp

        #define CUSTOM_FLAG_A 1
        #define CUSTOM_FLAG_B 1

        #define CUSTOM_MACRO_A( x ) x

.. rule :: Enumerated type

    An enumerated type name and its values must be written in snake case.
     If a ``typedef`` is defined, it follows the snake case standard.

    .. code-block :: cpp

        typedef enum sample_enum
        {
            VALUE_1,
            VALUE_2
            ...
        } sample_enum_t;

.. recommendation :: Signal

    Signals names and types can be grouped inside a structure called ``signals``, which allows to prefix them it like
    if it was a namespace. Also this make it possible to initialize the string keys directly in the header.
    It should be suffixed by a past action (ex: Updated, Triggered,
    Cancelled, CakeCookedAndBaked). It follows other common variable naming rules

    .. code-block :: cpp

        struct signals
        {
            using buffer_modified_t = core::com::signal<void()>;
            using landmark_added_t  = core::com::signal<void(std::shared_ptr<point>)>;

            static inline const core::com::signals::key_t BUFFER_MODIFIED = "...";
            static inline const core::com::signals::key_t LANDMARK_ADDED  = "...";
        };

.. rule :: Slot

    Slots can be grouped inside a structure called ``slots``, which allows to prefix them it like if it was a
    namespace. Also this makes it possible to initialize the string keys directly in the header. It should be
    prefixed by an imperative order (Ex: Update, Run, Detach,
    Deliver, OpenWebBrowser, GoToFail). It follows other common variable naming rules (member of a class, etc...).

    .. code-block :: cpp

        struct slots
        {
            static inline const core::com::slots::key_t SET_BOOL_PARAMETER   = "setBoolParameter";
            static inline const core::com::slots::key_t SET_INT_PARAMETER    = "setIntParameter";
            static inline const core::com::slots::key_t SET_DOUBLE_PARAMETER = "setDoubleParameter";
        };

.. recommendation :: Service configuration

    Services are configurable at runtime thanks to an XML declaration block. XML tags are parsed inside
    the ``configuring()̀`` method. When these tags are used more than once and outside this method, it is
    recommended to gather their definition in a structure called ``config``. This structure may also hold
    the variable resulting of the parsing of the configuration.

    .. code-block :: cpp

        struct config
        {
            static inline const std::string VISIBLE    = "visible";
            static inline const std::string MATERIAL   = "material";
            static inline const std::string TEXTURE    = "texture";

            bool visible         = false;
            std::string material = "...";
        };

        config m_config = {};

.. recommendation :: Service objects

    Services use ``sight::data::ptr`` to manipulate their input/output. They refer to the objects using
    string keys. When these keys are used more than once, they can be gathered in structures
    ``in``, ``inout`` and ``out``.

    .. code-block :: cpp

        struct inout
        {
            static constexpr std::string_view MASK   = "...";
            static constexpr std::string_view VOLUME = "...";
        };

        struct in
        {
            static constexpr std::string_view MATRIX = "...";
        };


Coding rules
-----------------

Blocks
~~~~~~~~~~~~~~~~~~~~~~~~~

.. rule :: Indentation

    Code block indentation and bracket positioning follow the
    `Allman <http://en.wikipedia.org/wiki/Indent_style#Allman_style>`_ style.

    .. code-block :: cpp

        void function(void)
        {
            if(x == y)
            {
                something1();
                something2();
            }
            else
            {
                somethingElse1();
                somethingElse2();
            }
            finalThing();
        }

.. rule :: Indentation of namespaces

    Namespaces are an exception of the previous rule. They should not be indented.

    .. code-block :: cpp

        namespace namespace_a
        {
        namespace namespace_b
        {
            ...
        } // namespace namespace_b
        } // namespace namespace_a

.. rule :: Blocks are mandatory

    After a control statement (if, else, for, while/do...while, try/catch, switch, foreach, etc...), it is mandatory to
    open a block, whatever is the number of instructions inside the block.

.. rule :: Scope

    The keywords ``public``, ``protected`` and ``private`` are not indented, they should be aligned with the keyword
    ``class``.

    .. code-block :: cpp

        class sample
        {
        public:
            ...
        private:
            ...
        };

Class declaration
~~~~~~~~~~~~~~~~~~~~~~~~~

.. recommendation :: Only three scope sections

    When possible, use only one section of each scope type ``public``, ``protected`` and ``private``. They must be
    declared in this order.

.. recommendation :: Group class members by type

    You may group class members in each scope according to their type: type definitions, constructors, destructor,
    operators, variables, functions.

.. _`Hide implementation`:
.. rule :: Hide implementation

    Avoid non-const public member variables except in very small classes (i.e. a 3D point). The `Pimpl idiom
    <http://c2.com/cgi/wiki?PimplIdiom>`_ may
    also be helpful to separate the implementation from the declaration.

.. recommendation :: Hide implementation

    Try to put variables as much as possible in the ``private`` section.

.. rule :: Accessors

    Since you protect your member variables from the outside, you will have to write accessors, named ``get_xxx()``
    and ``set_xxx()``. Getters are always ``const``.

.. rule :: Template class function definition

    The function definition of a template class must be defined after the declaration of the class.

    .. code-block :: cpp

        template < typename TYPE >
        class sample
        {
        public:
            void function(int i);
        };

        template < typename TYPE >
        inline sample<TYPE>::function(int i)
        {
            ...
        }

.. recommendation :: Separate template class function definition

    In addition of the previous rule, you may put the definition of the function in a ``.hxx`` file. This file will be
    included in the implementation file right after the header file (the compile time will be reduced comparing with an
    inclusion of the ``.hxx`` in the header file itself).

    .. code-block :: cpp

        #include <namespace_a/file.hpp>
        #include <namespace_a/file.hxx>

.. rule :: Getters/setters

    We use Qt/libstd style for getters and setters. This means we do not use the prefix ``get``,
    but rather only use the name of the attribute.

    .. code-block :: cpp

        class A
        {
        public:
            int property();
            void set_property(int);
        private:
            int m_property;
        };

    Also remember to avoid [trivial getters and setters](https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#c131-avoid-trivial-getters-and-setters).


In-class member initialization
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. rule :: Favor initialization of member variables in-class at declaration.

    You should favor in-class initialization for your member variables as shown below.

    .. code-block :: cpp

        class sample_class
        {
            sample_class();
            ~sample_class();

            int m_value {0};

            bool m_condition {true};

            std::string m_string {"Hello World!"};
        };

.. rule :: Avoid constructor initialization

    Constructor initialization should be avoided, only constructor parameters should be initialized here.
    It is possible however to override default in-class initializations on your constructor.

    .. code-block :: cpp

        sample_class::sample_class( const std::string& name, const int value ) :
            base_class_one( name ),
            base_class_two( name ),
            m_string    ( "Goodbye World!" )
        {}

.. rule :: One initializer per line in constructor initialization

    In a class constructor, place one initializer per line. Constructors of base classes should be placed first, do not
    specify an initializer if it is the default one (empty std::string for instance).

.. recommendation :: Align everything that improves readability

    To improve readability, you may align members on one hand and argument lists on the other hand.

    .. code-block :: cpp

        sample_class::sample_class( const std::string& name, const int value ) :
            base_class_one  ( name ),
            base_class_two  ( name ),
            m_value       ( value ),
            m_misc        ( 10 )
         {}



Functions
~~~~~~~~~~~~~~~~~~~~~~~~~

.. rule :: Constant reference

    Whenever possible, use constant references to pass arguments of non-primitive types. This avoids useless and
    expensive copies.

    .. code-block :: cpp

        void bad_function( std::vector<int> array )
        {
            ...
        }

        void good_function( const std::vector<int>& array )
        {
            ...
        }

.. rule :: Passing smart pointers as arguments

    From `Herb Sutter blog <https://herbsutter.com/2013/06/05/gotw-91-solution-smart-pointer-parameters>`_:

    Use a non-const ``shared_ptr&`` parameter only to modify the ``shared_ptr``. Use a ``const shared_ptr&`` as a
    parameter only if you’re not sure whether or not you’ll take a copy and share ownership; otherwise use ``widget*``
    instead (or if not nullable, a ``widget&``).

.. rule :: Constant functions

    Whenever a member function should not modify an attribute of a class, it must be declared as ``const``.

    .. code-block :: cpp

        void read_only_function( const std::vector<int>& array ) const
        {
            ...
        }

.. recommendation :: Limit use of expression in arguments

    When passing arguments, try to limit the use of expressions to the minimum.

    .. code-block :: cpp

        // This is bad
        function( fn1(val1 + val2 / 4 ), fn2( fn3( val3 ), val4) );

        // This is better
        const float res0 = val1 + val2 / 4;

        const float res1 = fn1(res0);
        const float res3 = fn3(val3);
        const float res2 = fn2(res3, val4);

        function( res1 , res2 );

Miscellaneous
~~~~~~~~~~~~~~~~~~~~~~~~~

.. rule :: Enumerator labels

    Each label must be placed on a single line, upper case, followed by a comma. If you assign values to labels,
    align values on the same column.

    .. code-block :: cpp

        enum flag
        {
            VALUE_A     = 1,
            VALUE_B     = 2,
            VALUE_C     = 4,
        };

.. rule :: Use of namespaces

    You have to organize your code inside namespaces. By default, you will have at least one namespace for your module
    (application or module). Inside this namespace, it is recommended to split your code into sub-namespaces. This
    helps notably to prevent naming conflicts.

    It is forbidden to use the expression ``using namespace`` in header files but it is allowed in implementation files.
    Aliases can also be used in this latter case.

    .. code-block :: cpp

        namespace sf = std::filesystem;


.. rule :: Keyword const

    Use this keyword as much as possible for variables, parameters and functions. When used for a variable or a
    parameter, it should be placed on the left of the qualified id, e.g. :

    .. code-block :: cpp

        const double factor = 1.0;
        const auto* p_factor = &factor;
        std::vector< const object* > objects_list;

        void func(const object& param);


.. recommendation :: Keyword auto

    Use this keyword as much as possible to improve maintainability and robustness of the code.

.. rule :: Prefer constants instead of #define

    Use a static constant object or an enumeration instead of a ``#define``. This will help the compiler to make
    type checking. You will also be able to check the content of the constants while debugging. You can also define
    a scope for them, inside the namespace, inside a class, private to a class, etc...

.. rule :: Prefer references over pointers

    When possible, use references instead of pointers, especially for function parameters. Pointer as parameter should
    only be used if it is possible to have a "nullptr" pointer or when passing a C-like array. If you use a pointer,
    always check it if is null in the current scope before dereferencing it.

.. rule :: Type conversion

    For type conversion, use the C++ operators which are ``static_cast<>``, ``dynamic_cast<>``,  ``const_cast<>`` and
    ``reinterpret_cast<>``. Use them wisely in the appropriate case. You may read `this
    documentation <http://www.cplusplus.com/doc/tutorial/typecasting/>`_.
    For smart pointers conversions, use the equivalent ``std::static_pointer_cast<>``, ``std::dynamic_pointer_cast<>``,
    ``std::const_pointer_cast<>``, ``std::reinterpret_pointer_cast<>``.

.. recommendation :: Exceptions

    Exceptions are the preferred mechanism to handle error notifications.

.. rule :: Explicit integer types

    When you do need a specific integer size, use type definitions declared in
    `<cstdint> <http://www.cplusplus.com/reference/cstdint/>`_, for example:

    ======  =========  ==========
     Bits    Signed     Unsigned
    ======  =========  ==========
     8       int8_t     uint8_t
     16      int16_t    uint16_t
     32      int32_t    uint32_t
     64      int64_t    uint64_t
    ======  =========  ==========
