XML coding
==========

.. rule :: uid name

    ``uid`` should have a semantic name. They must be written in snake case.
    Don't add an uid suffix (like `image_uid`).
    Moreover, avoid identifiers like ``my_XXXXX`` or ``custom_XXXXX``.

    .. code-block :: xml

        <object uid="image" type="sight::data::image" />

        <service uid="imageReader" type="sight::io::vtk::image_reader">
            <inout key="image" uid="image" />
        </service>

Parameters
----------

.. rule :: Core parameters

    These parameters are given by launcher services and are capitalized

    * ``WID_PARENT``
    * ``AS_UID``

.. rule :: Data parameters (object)

    Objects ``uid`` should be written in snake case.

.. rule :: Other parameters

    Non-object parameters: channel, icon path, title, ...
    written in capitals.

Configuration
-------------

.. rule :: Configuration documentation

    The configuration must be documented.

    .. code-block :: xml

        <!--
        This config opens a window containing the editors managing the ModelSeries: show/hide reconstructions, change the color, ...

        Example:
        <service uid="..." type="sight::app::config_controller">
            <appConfig id="ModelSeriesManagerWindow" />
            <inout key="modelSeries" uid="..." />
            <parameter replace="ICON_PATH" by="my/icon/path.ico" />
            <parameter replace="WINDOW_TITLE"  by="" />
        </service>

        Object:
        - modelSeries (mandatory): uid of the ModelSeries to manage
        Other:
        - ICON_PATH (mandatory): window icon
        - WINDOW_TITLE(optional): window title
        -->
        <extension implements="sight::app::extension::config">
            <!-- ... -->
        </extension>

.. rule :: Order

    The configuration file must be written following this order:

    #. objects
    #. GUI (frame, view, toolbar, menu, menubar)
    #. actions
    #. ConfigLauncher
    #. reader/writer
    #. editors
    #. extractors
    #. listener/sender
    #. algorithms
    #. renderers
    #. connections
    #. start
    #. update

    Each section should begin with an XML comment.

    .. code-block :: xml

        <!-- *************************************************** begin GUI ************************************************* -->
        <!-- ... frame, view, toolbar, menu and menubar services ... -->

.. rule :: Align the xml attributes

    The XML attributes should be aligned.

    .. code-block :: xml

        <service uid="cfgNegato1" type="sight::app::config_controller">
            <appConfig id="3DNegatoWithAcq" />
            <inout key="imageComposite" uid="${imageComposite}" />
            <inout key="modelSeries"    uid="${modelSeries}" />
            <inout key="landmarks"      uid="${landmarks}" />
            <parameter replace="orientation"              by="axial" />
            <parameter replace="WID_PARENT"               by="view_negato1" />
            <parameter replace="patient_name"             by="${patient_name}" />
            <parameter replace="PickingChannel"           by="pickerChannel" />
            <parameter replace="CrossTypeChannel"         by="crossTypeChannel" />
            <parameter replace="setSagittalCameraChannel" by="setSagittalCameraChannel" />
            <parameter replace="setFrontalCameraChannel"  by="setFrontalCameraChannel" />
            <parameter replace="setAxialCameraChannel"    by="setAxialCameraChannel" />
        </service>

.. rule :: Order the objects

    The objects should be ordered by type (ref, new and deferred), and by class.

    .. code-block :: xml

        <object uid="series_db"         type="sight::data::series_db" src="ref" />
        <object uid="loading_series_db" type="sight::data::series_db" src="ref" />
        <object uid="image_ref"         type="sight::data::image"     src="ref" />
        <object uid="image_src"         type="sight::data::image"     src="ref" />

        <object uid="new_series_db" type="sight::data::series_db" />
        <object uid="selections"    type="sight::data::vector" />

        <object uid="current_activity" type="sight::data::activity_set" src="deferred" />
        <object uid="computed_image"   type="sight::data::image"        src="deferred" />

.. rule :: Comment renderers

    Each scene and its adaptors must begin with an XML comment.

    .. code-block :: xml

        <!-- ************************************************ begin 3Dscene ************************************************ -->

        <service uid="3Dscene" type="sight::viz::scene3d::render">
            <!-- ... -->
        </service>

        <service uid="adaptor1" type="sight::module::viz::scene3d::mesh" />
        <service uid="adaptor2" type="sight::module::viz::scene3d::mesh" />

    The starts of these adaptors must be preceded by a comment with the scene name

    .. code-block :: xml

        <!-- ************************************************* begin start ************************************************* -->

        <start uid="frame" />

        <!-- 3DScene adaptors-->
        <start uid="adaptor1" />
        <start uid="adaptor2" />

Example
-------

    .. code-block :: xml

        <!-- ************************************************** begin data ************************************************* -->

        <object uid="image" type="sight::data::image" />

        <!-- *************************************************** begin GUI ************************************************* -->

        <service uid="frame" type="sight::module::ui::frame">
            <gui>
                <frame>
                    <name>Application</name>
                    <icon>Application/tuto.ico</icon>
                </frame>
            </gui>
            <registry>
                <view sid="mainView" start="true" />
            </registry>
        </service>

        <service uid="main_view" type="sight::module::ui::view">
            <gui>
                <layout type="sight:ui::layout::cardinal">
                    <view align="center" />
                    <view align="bottom" minWidth="400" minHeight="30"/>
                    <view align="bottom" minWidth="40"  minHeight="30"/>
                </layout>
            </gui>
            <registry>
                <view sid="3DScene"        start="true" />
                <view sid="sliceEditor"    start="true" />
                <view sid="snapshotEditor" start="true" />
            </registry>
        </service>

        <!-- ************************************************ begin actions ************************************************ -->

        <service uid="action_open_image" type="sight::module::ui::action" />

        <!-- ******************************************** begin readers/writers ******************************************** -->

        <service uid="image_reader" type="sight::module::ui::io::selector">
            <inout key="data" uid="imageUID" />
            <type mode="reader" />
        </service>

        <!-- *********************************************** begin editors ************************************************* -->

        <service uid="sliceEditor" type="sight::module::ui::qt::image::slice_index_position_editor" auto_connect="true">
            <inout key="image" uid="imageUID" />
            <sliceIndex>axial</sliceIndex>
        </service>

        <service uid="snapshotEditor" type="sight::module::ui::qt::viz::SnapshotEditor" />

        <!-- ************************************************ begin 3Dscene ************************************************ -->

        <service uid="3Dscene" type="sight::viz::scene3d::render">
            <scene>
                <layer id="default">
                    <adaptor uid="imageAdaptor" />
                    <adaptor uid="snapshotAdaptor" />
                </layer>
            </scene>
        </service>

        <service uid="imageAdaptor" type="sight::module::viz::scene3d::negato3d" auto_connect="true">
            <inout key="image" uid="imageUID" />
            <config renderer="default" picker="myPicker" mode="3d" slices="3" sliceIndex="axial" />
        </service>

        <service uid="snapshotAdaptor" type="sight::module::viz::scene3d::SSnapshot">
            <config renderer="default" />
        </service>

        <!-- ********************************************* begin connections *********************************************** -->

        <connect>
            <signal>snapshotEditor/snapped</signal>
            <slot>snapshotAdaptor/snap</slot>
        </connect>

        <connect>
            <signal>action_open_image/clicked</signal>
            <slot>image_reader/update</slot>
        </connect>

        <!-- ************************************************* begin start ************************************************* -->

        <start uid="frame" />
        <start uid="actionOpenImage" />

        <!-- 3DScene adaptors-->
        <start uid="imageAdaptor" />
        <start uid="snapshotAdaptor" />

        <!-- ************************************************ begin update ************************************************* -->

        <update uid="actionOpenImage" />

