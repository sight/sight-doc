.. _TutorialsTuto03DataService:

***************************************************
[*tuto03_data_service*] Display an image with menu
***************************************************

The third tutorial is similar to the previous application, but we add gui services like menus.

.. figure:: ../media/tuto03DataService.png
    :scale: 25
    :align: center
    :alt: tuto03DataService

=============
Prerequisites
=============

Before reading this tutorial, you should have seen :
* :ref:`TutorialsTuto02DataServiceBasic`
* :ref:`SADGUI`

=========
Structure
=========

--------------
CMakeLists.txt
--------------

This file describes the project information and requirements :

.. code-block:: cmake

    # Create the correct CMake target for an application
    sight_add_target(tuto03_data_service TYPE APP)

    # Dependencies need to be built before, but not linked with
    add_dependencies(tuto03_data_service
                     sightrun           # Needed to build the launcher
                     module_app      # XML configurations
                     module_ui_qt       # Load qt implementation of gui
                     data               # Objects declaration
                     module_service     # Service base module
                     module_ui_base     # UI declaration/Actions
                     module_io_vtk      # Contains the reader and writer for VTK files (image and mesh).
                     module_viz_sample  # Loads basic rendering services for images and meshes.
    )

    # The ``module_param`` line defines the parameters to set for a module, here it defines the configuration
    # to launch by the module_app module, i.e. the application configuration.
    module_param(
            module_app
        PARAM_LIST
            config
        PARAM_VALUES
            Tuto03DataService_AppCfg
    )

    # Generate the profile.xml
    sight_generate_profile(tuto03_data_service)

.. _CMake: https://cmake.org

----------
plugin.xml
----------

This file is located in the ``rc/`` directory of the application. It defines the services to run.

.. code-block:: xml

    <plugin id="tuto03_data_service" >

        <requirement id="sight::module::service" />
        <requirement id="sight::module::ui::qt" />

        <extension implements="sight::app::extension::config" >
            <id>Tuto03DataService_AppCfg</id>
            <config>

                <!-- ******************************* Objects declaration ****************************** -->

                <!-- The root data object in tutoDataService is a sight::data::image. -->
                <object uid="image" type="sight::data::image" />

                <!-- ******************************* UI declaration *********************************** -->

                <!-- Frame service:
                    The frame creates a container for the rendering service and a menu bar.
                    In this tutorial, the gui services will automatically start the services they register using the
                    'start="true"' attribute.
                -->
                <service uid="mainFrame" type="sight::module::ui::frame" >
                    <gui>
                        <frame>
                            <name>tuto03_data_service</name>
                            <icon>tuto03_data_service/tuto.ico</icon>
                            <minSize width="800" height="600" />
                        </frame>
                        <menuBar />
                    </gui>
                    <registry>
                        <menuBar sid="menuBarView" start="true" />
                        <view sid="imageRenderSrv" start="true" />
                    </registry>
                </service>

                <!--
                    Menu bar service:
                    This service defines the list of the menus displayed in the menu bar.
                    Here, we have only one menu: File
                    Each <menu> declared into the <layout> tag, must have its associated <menu> into the <registry> tag.
                    The <layout> tags defines the displayed information, whereas the <registry> tags defines the
                    services information.
                -->
                <service uid="menuBarView" type="sight::module::ui::menubar" >
                    <gui>
                        <layout>
                            <menu name="File" />
                        </layout>
                    </gui>
                    <registry>
                        <menu sid="menuView" start="true" />
                    </registry>
                </service>

                <!--
                    Menu service:
                    This service defines the actions displayed in the "File" menu.
                    Here, it registers two actions: "Open file", and "Quit".
                    As in the menu bar service, each <menuItem> declared into the <layout> tag, must have its
                    associated <menuItem> into the <registry> tag.

                    It's possible to associate specific attributes for <menuItem> to configure their style, shortcut...
                    In this tutorial, the attribute 'specialAction' has the value "QUIT". On MS Windows, there's no
                    impact, but on Linux this value installs the default 'Quit' system icon in the menuItem.
                -->
                <service uid="menuView" type="sight::module::ui::menu" >
                    <gui>
                        <layout>
                            <menuItem name="Open image" shortcut="Ctrl+O" />
                            <separator />
                            <menuItem name="Quit" specialAction="QUIT" shortcut="Ctrl+Q" />
                        </layout>
                    </gui>
                    <registry>
                        <menuItem sid="openImageAct" start="true" />
                        <menuItem sid="quitAct" start="true" />
                    </registry>
                </service>

                <!-- ******************************* Actions ****************************************** -->

                <!--
                    Quit action:
                    The action service (sight::module::ui::quit) is a generic action that will close the application
                    when the user click on the menuItem "Quit".
                -->
                <service uid="quitAct" type="sight::module::ui::quit" />

                <!--
                    Open file action:
                    This service (sight::module::ui::com::starter) is a generic action, it starts and updates the
                    services given in the configuration when the user clicks on the action.
                    Here, the reader selector will be called when the action is clicked.
                -->
                <service uid="openImageAct" type="sight::module::ui::com::starter" >
                    <start uid="imageReaderSrv" />
                </service>

                <!-- ******************************* Services ***************************************** -->

                <!--
                    Reader selector dialog:
                    This is a generic service that shows a dialog to display all the readers or writers available for its
                    associated data. By default it is configured to show readers. (Note: if there is only one reading
                    service, it is directly selected without prompting.)
                    Here, the only reader available to read a sight::data::image is sight::module::io::vtk::ImageReaderService (see
                    tuto02_data_service_basic), so the selector will not be displayed.
                    When the service is chosen, it is started, updated and stopped, so the data is read.
                -->
                <service uid="imageReaderSrv" type="sight::module::ui::io::selector" >
                    <inout key="data" uid="image" />
                </service>

                <!--
                    3D visualization service of medical images:
                    Here, the service attribute 'auto_connect="true"' allows the rendering to listen the modification of
                    the data image. So, when the image is loaded, the visualization will be updated.
                -->
                <service uid="imageRenderSrv" type="sight::module::viz::sample::image" >
                    <in key="image" uid="image" auto_connect="true" />
                </service>

                <!-- ******************************* Start services ***************************************** -->

                <!--
                    Here, we only start the frame because all the others services are managed by the gui service:
                    - the frame starts the menu bar and the redering service
                    - the menu bar starts the menu services
                    - the menus starts the actions
                -->
                <start uid="mainFrame" />

            </config>
        </extension>
    </plugin>


The framework provides some gui services:

Frame (``sight::module::ui::frame``)
    This service displays a frame and creates menu bar, tool bar and container for views, rendering service, ...

View (``sight::module::ui::view``)
    This service creates sub-container and tool bar.

Menu bar (``sight::module::ui::menu``)
    A menu bar displays menus.

Tool bar (``sight::module::ui::toolbar``)
    A tool bar displays actions, menus and editors.

Menu (``sight::module::ui::menuSrv``)
    A menu displays actions and sub-menus.

Action (inherited from ``sight:ui::action`` )
    An action is a service inherited from ``sight:ui::action``. It is called when the user clicks on the associated
    tool bar or menu.

Editors (inherited from ``sight:ui::editor``)
    An editor is a service inherited from ``sight:ui::editor``. It is used to creates your own gui container.

===
Run
===

To run the application, you must call the following line into the install or build directory:

.. tabs::

   .. group-tab:: Linux

        .. code::

            bin/tuto03dataservice

   .. group-tab:: Windows

        .. code::

            bin/tuto03dataservice.bat
