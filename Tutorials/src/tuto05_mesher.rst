.. _TutorialsTuto05Mesher:

**********************************************
[*tuto05_mesher*] Create a mesh from an image
**********************************************

The fifth tutorial explains how to use several objects in an application.
This application provides an action to create a mesh from an image mask.

.. figure:: ../media/tuto05Mesher.png
    :scale: 25
    :align: center
    :alt: tuto05Mesher

=============
Prerequisites
=============

Before reading this tutorial, you should have seen:
* :ref:`TutorialsTuto04SignalSlot`

=========
Structure
=========

--------------
CMakeLists.txt
--------------

This file describes the project information and requirements :

.. code-block:: cmake

    # Create the correct CMake target for an application
    sight_add_target(tuto05_mesher TYPE APP)

    # Dependencies need to be built before, but not linked with
    add_dependencies(tuto05_mesher
                     sightrun               # Needed to build the launcher
                     module_app          # XML configurations
                     module_ui_qt           # Load qt implementation of gui
                     data                   # Objects declaration
                     module_service         # Service base module
                     module_ui_base         # UI declaration/Actions
                     module_io_vtk          # Contains the reader and writer for VTK files (image and mesh).
                     module_viz_sample      # Loads basic rendering services for images and meshes.
                     module_filter_mesh     # Provides services to generate a mesh from an image.
    )

    # The ``module_param`` line defines the parameters to set for a module, here it defines the configuration
    # to launch by the module_app module, i.e. the application configuration.
    module_param(
            module_app
        PARAM_LIST
            config
        PARAM_VALUES
            Tuto05Mesher_AppCfg
    )

    # Allow dark theme via module_ui_qt
    module_param(
            module_ui_qt
        PARAM_LIST
            resource
            stylesheet
        PARAM_VALUES
            sight::module::ui::qt/flatdark.rcc
            sight::module::ui::qt/flatdark.qss
    )

    # Generate the profile.xml
    sight_generate_profile(tuto05_mesher)

.. _CMake: https://cmake.org

----------
plugin.xml
----------

This file is in the ``rc/`` directory of the application. It defines the services to run.

.. code-block:: xml

    <plugin id="tuto05_mesher" >

        <requirement id="sight::module::service" />
        <requirement id="sight::module::ui::qt" />

        <extension implements="sight::app::extension::config" >
            <id>Tuto05Mesher_AppCfg</id>
            <config>

                <!-- ******************************* Objects declaration ****************************** -->

                <!-- Mesh object associated to the uid 'myMesh' -->
                <object uid="myMesh" type="sight::data::mesh" />

                <!-- Image object associated to the key 'myImage' -->
                <object uid="myImage" type="sight::data::image" />

                <!-- ******************************* UI declaration *********************************** -->

                <service uid="mainFrame" type="sight::module::ui::frame" >
                    <gui>
                        <frame>
                            <name>tuto05_mesher</name>
                            <icon>tuto05_mesher/tuto.ico</icon>
                            <minSize width="800" height="600" />
                        </frame>
                        <menuBar />
                    </gui>
                    <registry>
                        <menuBar sid="menuBarView" start="true" />
                        <view sid="containerView" start="true" />
                    </registry>
                </service>

                <service uid="menuBarView" type="sight::module::ui::menu_bar" >
                    <gui>
                        <layout>
                            <menu name="File" />
                            <menu name="Mesher" />
                        </layout>
                    </gui>
                    <registry>
                        <menu sid="menuFileView" start="true" />
                        <menu sid="menuMesherView" start="true" />
                    </registry>
                </service>

                <service uid="menuFileView" type="sight::module::ui::menu" >
                    <gui>
                        <layout>
                            <menuItem name="Open image" shortcut="Ctrl+O" />
                            <menuItem name="Save image" />
                            <separator />
                            <menuItem name="Open mesh" shortcut="Ctrl+M" />
                            <menuItem name="Save mesh" />
                            <separator />
                            <menuItem name="Quit" specialAction="QUIT" shortcut="Ctrl+Q" />
                        </layout>
                    </gui>
                    <registry>
                        <menuItem sid="openImageAct" start="true" />
                        <menuItem sid="saveImageAct" start="true" />
                        <menuItem sid="openMeshAct" start="true" />
                        <menuItem sid="saveMeshAct" start="true" />
                        <menuItem sid="quitAct" start="true" />
                    </registry>
                </service>

                <service uid="menuMesherView" type="sight::module::ui::menu" >
                    <gui>
                        <layout>
                            <menuItem name="Compute Mesh (VTK)" />
                        </layout>
                    </gui>
                    <registry>
                        <menuItem sid="createMeshAct" start="true" />
                    </registry>
                </service>

                <!--
                    Default view service:
                    The type 'sight::ui::layout::line' represents a layout where the views are aligned
                    horizontally or vertically (set orientation value 'horizontal' or 'vertical').
                    It is possible to add a 'proportion' attribute for the <view> to defined the proportion
                    used by the view compared to the others.
                -->
                <service uid="containerView" type="sight::module::ui::view" >
                    <gui>
                        <layout type="sight::ui::layout::line" >
                            <orientation value="horizontal" />
                            <view/>
                            <view/>
                        </layout>
                    </gui>
                    <registry>
                        <view sid="imageSrv" start="true" />
                        <view sid="meshSrv" start="true" />
                    </registry>
                </service>

                <!-- ******************************* Actions ****************************************** -->

                <service uid="quitAct" type="sight::module::ui::quit" />

                <service uid="openImageAct" type="sight::module::ui::com::starter" >
                    <start uid="imageReaderSrv" />
                </service>

                <service uid="saveImageAct" type="sight::module::ui::com::starter" >
                    <start uid="imageWriterSrv" />
                </service>

                <service uid="openMeshAct" type="sight::module::ui::com::starter" >
                    <start uid="meshReaderSrv" />
                </service>

                <service uid="saveMeshAct" type="sight::module::ui::com::starter" >
                    <start uid="meshWriterSrv" />
                </service>

                <service uid="createMeshAct" type="sight::module::ui::action"/>

                <!-- ******************************* Services ***************************************** -->

                <!-- Add a shortcut in the application (key "v") -->
                <service uid="shortcutSrv" type="sight::module::ui::qt::com::signal_shortcut" >
                    <config shortcut="v" sid="containerView" />
                </service>

                <!--
                    Services associated to the Image data :
                    Visualization, reading and writing service creation.
                -->
                <service uid="imageSrv" type="sight::module::viz::sample::image" >
                    <in key="image" uid="myImage" auto_connect="true" />
                </service>

                <service uid="imageReaderSrv" type="sight::module::ui::io::selector" >
                    <inout key="data" uid="myImage" />
                    <type mode="reader" />
                </service>

                <service uid="imageWriterSrv" type="sight::module::ui::io::selector" >
                    <inout key="data" uid="myImage" />
                    <type mode="writer" />
                </service>

                <!--
                    Services associated to the Mesh data :
                    Visualization, reading and writing service creation.
                -->
                <service uid="meshSrv" type="sight::module::viz::sample::mesh" >
                    <in key="mesh" uid="myMesh" auto_connect="true" />
                </service>

                <service uid="meshReaderSrv" type="sight::module::ui::io::selector" >
                    <inout key="data" uid="myMesh" />
                    <type mode="reader" />
                </service>

                <service uid="meshWriterSrv" type="sight::module::ui::io::selector" >
                    <inout key="data" uid="myMesh" />
                    <type mode="writer" />
                </service>

                <!-- ******************************* Connections ***************************************** -->

                <!-- Connect the shortcut "v" to the update slot of 'createMeshAct'-->
                <connect>
                    <signal>shortcutSrv/activated</signal>
                    <slot>createMeshAct/update</slot>
                </connect>

                <!-- ******************************* Start services ***************************************** -->

                <start uid="mainFrame" />
                <start uid="shortcutSrv" />

            </config>
        </extension>
    </plugin>


===
Run
===

To run the application, you must call the following line into the install or build directory:

.. tabs::

   .. group-tab:: Linux

        .. code::

            bin/tuto05mesher

   .. group-tab:: Windows

        .. code::

            bin/tuto05mesher.bat
