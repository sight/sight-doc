.. _TutorialsTuto02DataServiceBasic:

**********************************************
[*tuto02_data_service_basic*] Display an image
**********************************************

The second tutorial represents a basic application that displays a medical 3D image.

.. figure:: ../media/tuto02DataServiceBasic.png
    :scale: 25
    :align: center
    :alt: tuto02DataServiceBasic

=============
Prerequisites
=============

Before reading this tutorial, you should have seen :
 * :ref:`TutorialsTuto01Basic`

=========
Structure
=========

--------------
CMakeLists.txt
--------------

This file describes the project information and requirements :

.. code-block:: cmake

    # Create the correct CMake target for an application
    sight_add_target(tuto02_data_service_basic TYPE APP)

    # Dependencies need to be built before, but not linked with
    add_dependencies(tuto02_data_service_basic
                     sightrun           # Needed to build the launcher
                     module_app      # XML configurations
                     module_ui_qt       # Load qt implementation of gui
                     data               # Objects declaration
                     module_service     # Service base module
                     module_ui_base     # UI declaration/Actions
                     module_io_vtk      # Contains the reader and writer for VTK files (image and mesh).
                     module_viz_sample  # Loads basic rendering services for images and meshes.
    )

    # The ``module_param`` line defines the parameters to set for a module, here it defines the configuration
    # to launch by the module_app module, i.e. the application configuration.
    module_param(
            module_app
        PARAM_LIST
            config
        PARAM_VALUES
            TutoDataServiceBasic_AppCfg
    )

    # Generate the profile.xml
    sight_generate_profile(tuto02_data_service_basic)

.. _CMake: https://cmake.org

----------
plugin.xml
----------

This file is located in the ``rc/`` directory of the application. It defines the services to run.

.. code-block:: xml

    <plugin id="tuto02_data_service_basic" >

        <!-- The modules in requirements are automatically started when this
            Application is launched. -->
        <requirement id="sight::module::service" />
        <requirement id="sight::module::ui::qt" />

        <extension implements="sight::app::extension::config" >
            <id>TutoDataServiceBasic_AppCfg</id>
            <config>

                <!-- ******************************* Objects declaration ****************************** -->

                <!-- In tutoDataServiceBasic, the central data object is a sight::data::image. -->
                <object uid="imageData" type="sight::data::image" />

                <!-- ******************************* UI declaration *********************************** -->

                <!--
                    Description service of the GUI:
                    The sight::module::ui::frame service automatically positions the various
                    containers in the application main window.
                    Here, it declares a container for the 3D rendering service.
                -->
                <service uid="mainFrame" type="sight::module::ui::frame" >
                    <gui>
                        <frame>
                            <name>tuto02_data_service_basic</name>
                            <icon>tuto02_data_service_basic/tuto.ico</icon>
                            <minSize width="800" height="600" />
                        </frame>
                    </gui>
                    <registry>
                        <!-- Associate the container for the rendering service. -->
                        <view sid="imageRendereSrv" />
                    </registry>
                </service>

                <!-- ******************************* Services ***************************************** -->

                <!--
                    Reading service:
                    The <resource> tag defines the resource path (/rc) of a module or application.
                -->
                <service uid="imageReaderSrv" type="sight::module::io::vtk::image_reader" >
                    <inout key="data" uid="imageData" />
                    <resource>tuto02_data_service_basic/img-test.vtk</resource>
                </service>

                <!--
                    Visualization service of a 3D medical image:
                    This service will render the 3D image.
                -->
                <service uid="imageRendereSrv" type="sight::module::viz::sample::image" >
                    <in key="image" uid="imageData" />
                </service>

                <!-- ******************************* Start services ***************************************** -->

                <!--
                    Definition of the starting order of the different services:
                    The frame defines the 3D scene container, so it must be started first.
                    The services will be stopped the reverse order compared to the starting one.
                -->
                <start uid="mainFrame" />
                <start uid="imageReaderSrv" />
                <start uid="imageRendereSrv" />

                <!-- ******************************* Update services ***************************************** -->

                <!--
                    Definition of the service to update:
                    The reading service load the data on the update.
                    The render update must be called after the reading of the image.
                -->
                <update uid="imageReaderSrv" />
                <update uid="imageRendereSrv" />

            </config>
        </extension>

    </plugin>


For this tutorial, we have only one object ``sight::data::image`` and three services:
 * ``sight::module::ui::frame``: frame service
 * ``sight::io::vtk::image_reader``: reader for 3D VTK image
 * ``sight::module::viz::sample::image``: renderer for 3D image

The following order of the configuration elements must be respected:
  #. ``<object>``
  #. ``<service>``
  #. ``<connect>`` (see :ref:`TutorialsTuto04SignalSlot`)
  #. ``<start>``
  #. ``<update>``

.. note::
    To avoid the ``<start uid="imageRendereSrv" />``, the frame service can automatically start the rendering service: you
    just need to add the attribute ``start="true"`` in the ``<view>`` tag.

===
Run
===

To run the application, you must call the following line in the install or build directory:

.. tabs::

   .. group-tab:: Linux

        .. code::

            bin/tuto02dataservicebasic

   .. group-tab:: Windows

        .. code::

            bin/tuto02dataservicebasic.bat

