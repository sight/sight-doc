.. _TutorialsTuto07GenericScene:

***************************************
[*tuto07_generic_scene*] Generic scene
***************************************

This tutorial explains how to use the generic scene.

.. figure:: ../media/tuto07GenericScene1.png
    :scale: 25
    :align: center
    :alt: tuto07GenericScene1

    Image

.. figure:: ../media/tuto07GenericScene2.png
    :scale: 25
    :align: center
    :alt: tuto07GenericScene2


    Mesh

.. figure:: ../media/tuto07GenericScene3.png
    :scale: 25
    :align: center
    :alt: tuto07GenericScene3


    Mesh with texture

=============
Prerequisites
=============

Before reading this tutorial, you should have seen :
* :ref:`TutorialsTuto06Filter`
* :ref:`SADGenericScene`

=========
Structure
=========

--------------
CMakeLists.txt
--------------

This file describes the project information and requirements :

.. code-block:: cmake

    # Create the correct CMake target for an application
    sight_add_target(tuto07_generic_scene TYPE APP)

    # Dependencies need to be built before, but not linked with
    add_dependencies(tuto07_generic_scene
                     sightrun               # Needed to build the launcher
                     module_app          # XML configurations
                     module_ui_base         # UI declaration/Actions
                     module_ui_qt           # Load qt implementation of gui
                     module_ui_flaticons    # For "flat" icon style
                     module_viz_scene3d     # Used to render a 3D scene
                     module_viz_scene3dQt   # Enable rendering things in Qt window
                     data                   # Objects declaration
                     module_service         # Service base module
                     module_io_vtk          # Contains the reader and writer for VTK files (image and mesh).
                     module_io_matrix       # Allows reading/writing matrices from/to files
    )

    # The ``module_param`` line defines the parameters to set for a module, here it defines the configuration
    # to launch by the module_app module, i.e. the application configuration.
    module_param(
            module_app
        PARAM_LIST
            config
        PARAM_VALUES
            Tuto07GenericScene_AppCfg
    )

    # Allow dark theme via module_ui_qt
    module_param(
            module_ui_qt
        PARAM_LIST
            resource
            stylesheet
        PARAM_VALUES
            sight::module::ui::qt/flatdark.rcc
            sight::module::ui::qt/flatdark.qss
    )

    # Generate the profile.xml
    sight_generate_profile(tuto07_generic_scene)

.. _CMake: https://cmake.org

----------
plugin.xml
----------

This file is in the ``rc/`` directory of the application. It defines the services to run.

.. code-block:: xml

    <plugin id="tuto07_generic_scene" >

        <requirement id="sight::module::ui" />
        <requirement id="sight::module::viz::scene3d" />
        <requirement id="sight::module::ui::qt" />
        <requirement id="sight::module::service" />
        <requirement id="sight::module::viz::scene3dQt" />

        <extension implements="sight::app::extension::config" >
            <id>Tuto07GenericScene_AppCfg</id>
            <config>

                <!-- ******************************* Objects declaration ****************************** -->

                <object uid="image" type="sight::data::image" />
                <object uid="mesh" type="sight::data::mesh" />
                <object uid="texture" type="sight::data::image" />
                <object uid="tf" type="sight::data::transfer_function" />
                <object uid="snapshot" type="sight::data::image" />

                <!-- ******************************* UI declaration *********************************** -->

                <service uid="mainFrame" type="sight::module::ui::frame" >
                    <gui>
                        <frame>
                            <name>tuto07_generic_scene</name>
                            <icon>tuto07_generic_scene/tuto.ico</icon>
                            <minSize width="720" height="480" />
                        </frame>
                        <menuBar/>
                    </gui>
                    <registry>
                        <menuBar sid="menuBarView" start="true" />
                        <view sid="containerView" start="true" />
                    </registry>
                </service>

                <!-- Status bar used to display the progress bar for reading -->
                <service uid="progressBarView" type="sight::module::ui::job_bar" />

                <service uid="menuBarView" type="sight::module::ui::menu_bar" >
                    <gui>
                        <layout>
                            <menu name="File" />
                        </layout>
                    </gui>
                    <registry>
                        <menu sid="menuFileView" start="true" />
                    </registry>
                </service>

                <service uid="menuFileView" type="sight::module::ui::menu" >
                    <gui>
                        <layout>
                            <menuItem name="Open image" shortcut="Ctrl+I" />
                            <menuItem name="Open mesh" shortcut="Ctrl+M" />
                            <menuItem name="Open texture" shortcut="Ctrl+T" />
                            <separator/>
                            <menuItem name="Quit" specialAction="QUIT" shortcut="Ctrl+Q" />
                        </layout>
                    </gui>
                    <registry>
                        <menuItem sid="openImageAct" start="true" />
                        <menuItem sid="openMeshAct" start="true" />
                        <menuItem sid="openTextureAct" start="true" />
                        <menuItem sid="quitAct" start="true" />
                    </registry>
                </service>

                <!-- main view -->
                <service uid="containerView" type="sight::module::ui::view" >
                    <gui>
                        <layout type="sight::ui::layout::line" >
                            <orientation value="vertical" />
                            <view proportion="1" />
                            <view proportion="0" minHeight="30" resizable="false" backgroundColor="#3E4453" />
                        </layout>
                    </gui>
                    <registry>
                        <view sid="genericSceneSrv" start="true" />
                        <view sid="editorsView" start="true" />
                    </registry>
                </service>

                <!-- View for editors to update image visualization -->
                <service uid="editorsView" type="sight::module::ui::view" >
                    <gui>
                        <layout type="sight::ui::layout::line" >
                            <orientation value="horizontal" />
                            <view proportion="0" minWidth="50" />
                            <view proportion="1" />
                            <view proportion="0" minWidth="30" />
                        </layout>
                    </gui>
                    <registry>
                        <view sid="showNegatoSrv" start="true" />
                        <view sid="sliderIndexEditorSrv" start="true" />
                        <view sid="snapshotSrv" start="true" />
                    </registry>
                </service>

                <!-- *************************** Begin generic scene *************************** -->

                <!-- This scene display a 3D image and a textured mesh -->
                <service uid="genericSceneSrv" type="sight::viz::scene3d::render" >
                    <scene>
                        <background topColor="#36393E" bottomColor="#36393E" />

                        <layer id="default" order="1" />
                        <adaptor uid="trackballInteractorAdp" />
                        <adaptor uid="textureAdp" />
                        <adaptor uid="meshAdp" />
                        <adaptor uid="negatoAdp" />
                        <adaptor uid="snapshotAdp" />
                    </scene>
                </service>

                <service uid="trackballInteractorAdp" type="sight::module::viz::scene3d::adaptor::trackball_camera" >
                    <config layer="default" priority="0" />
                </service>

                <!-- Texture adaptor, used by mesh adaptor -->
                <service uid="textureAdp" type="sight::module::viz::scene3d::adaptor::texture" auto_connect="true" >
                    <in key="image" uid="texture" />
                    <config layer="default" textureName="ogreTexture" />
                </service>

                <!-- Mesh adaptor -->
                <service uid="meshAdp" type="sight::module::viz::scene3d::adaptor::mesh" auto_connect="true" >
                    <in key="mesh" uid="mesh" />
                    <config layer="default" textureName="ogreTexture" />
                </service>

                <!-- 3D image negatoscope adaptor -->
                <service uid="negatoAdp" type="sight::module::viz::scene3d::adaptor::negato3D" auto_connect="true" >
                    <in key="image" uid="image" />
                    <inout key="tf" uid="tf" />
                    <config layer="default" sliceIndex="axial" interactive="true" />
                </service>

                <service uid="snapshotAdp" type="sight::module::viz::scene3d::adaptor::fragments_info" >
                    <inout key="image" uid="snapshot" />
                    <config layer="default" flip="true" />
                </service>

                <!-- ******************************* Actions ****************************************** -->

                <!-- Actions to call readers -->
                <service uid="openImageAct" type="sight::module::ui::com::starter" >
                    <start uid="imageReaderSrv" />
                </service>

                <service uid="openMeshAct" type="sight::module::ui::com::starter" >
                    <start uid="meshReaderSrv" />
                </service>

                <service uid="openTextureAct" type="sight::module::ui::com::starter" >
                    <start uid="textureReaderSrv" />
                </service>

                <!-- Quit action -->
                <service uid="quitAct" type="sight::module::ui::quit" />

                <!-- ******************************* Services ***************************************** -->

                <!-- Image displayed in the scene -->
                <service uid="imageReaderSrv" type="sight::module::ui::io::selector" >
                    <inout key="data" uid="image" />
                    <type mode="reader" />
                </service>

                <!-- Mesh reader -->
                <service uid="meshReaderSrv" type="sight::module::ui::io::selector" >
                    <inout key="data" uid="mesh" />
                    <type mode="reader" />
                </service>

                <!-- texture reader -->
                <service uid="textureReaderSrv" type="sight::module::ui::io::selector" >
                    <inout key="data" uid="texture" />
                    <type mode="reader" />
                </service>

                <!--
                    Generic editor representing a simple button with an icon.
                    The button can be checkable. In this case it can have a second icon.
                    - It emits a signal "clicked" when it is clicked.
                    - It emits a signal "toggled" when it is checked/unchecked.

                    Here, this editor is used to show or hide the image. It is connected to the image adaptor.
                -->
                <service uid="showNegatoSrv" type="sight::module::ui::qt::com::signal_button" >
                    <config>
                        <checkable>true</checkable>
                        <icon>sight::module::ui::flaticons/RedCross.svg</icon>
                        <icon2>sight::module::ui::flaticons/Layers.svg</icon2>
                        <iconWidth>40</iconWidth>
                        <iconHeight>16</iconHeight>
                        <checked>true</checked>
                    </config>
                </service>

                <!-- Editor representing a slider to navigate into image slices -->
                <service uid="sliderIndexEditorSrv" type="sight::module::ui::qt::image::SliceIndexPositionEditor" auto_connect="true" >
                    <inout key="image" uid="image" />
                    <sliceIndex>axial</sliceIndex>
                </service>

                <service uid="snapshotSrv" type="sight::module::ui::qt::com::signalButton" >
                    <config>
                    <checkable>false</checkable>
                    <icon>sight::module::ui::flaticons/YellowPhoto.svg</icon>
                    </config>
                </service>

                <!-- Write the snapshot image -->
                <service uid="imageWriterSrv" type="sight::module::ui::io::selector" >
                    <inout key="data" uid="snapshot" />
                    <type mode="writer" />
                    <selection mode="exclude" />
                    <addSelection service="sight::module::io::session::writer" />
                </service>

                <!-- ******************************* Connections ***************************************** -->

                <!-- Connects readers to status bar service -->
                <connect>
                    <signal>meshReaderSrv/jobCreated</signal>
                    <signal>imageReaderSrv/jobCreated</signal>
                    <signal>textureReaderSrv/jobCreated</signal>
                    <slot>progressBarView/showJob</slot>
                </connect>

                <connect>
                    <signal>snapshotSrv/clicked</signal>
                    <slot>imageWriterSrv/update</slot>
                </connect>

                <!--
                    Connection for 3D image slice:
                    Connect the button (showNegatoSrv) signal "toggled" to the negato adaptor (negato3D)
                    slot "updateVisibility", this signals/slots contains a boolean.
                    The image slices will be show or hide when the button is checked/unchecked.
                -->
                <connect>
                    <signal>showNegatoSrv/toggled</signal>
                    <slot>negatoAdp/updateVisibility</slot>
                </connect>

                <!-- ******************************* Start services ***************************************** -->

                <start uid="mainFrame" />
                <start uid="progressBarView" />
                <start uid="imageWriterSrv" />
                <start uid="trackballInteractorAdp" />
                <start uid="textureAdp" />
                <start uid="meshAdp" />
                <start uid="negatoAdp" />
                <start uid="snapshotAdp" />

            </config>
        </extension>
    </plugin>


===
GUI
===

This tutorials use multiple editors to manage the image rendering:

- show/hide image slices
- navigate between the image slices
- snapshot

The two editors (``signalButton``) are generic, so we need to configure their behaviour in
the xml file.

The editor aspect is defined in the service configuration. They emit signals that must be manually connected to the
scene adaptor.

-------------
signalButton
-------------

This editor shows a simple button.

.. code-block:: xml

    <!--
        Generic editor representing a simple button with an icon.
        The button can be checkable. In this case it can have a second icon.
        - It emits a signal "clicked" when it is clicked.
        - It emits a signal "toggled" when it is checked/unchecked.

        Here, this editor is used to show or hide the image. It is connected to the image adaptor.
    -->
    <service uid="showNegatoSrv" type="sight::module::ui::qt::com::signalButton" >
        <config>
            <checkable>true</checkable>
            <icon>sight::module::ui::flaticons/RedCross.svg</icon>
            <icon2>sight::module::ui::flaticons/Layers.svg</icon2>
            <iconWidth>40</iconWidth>
            <iconHeight>16</iconHeight>
            <checked>true</checked>
        </config>
    </service>

text (optional)
    Text displayed on the button

icon (optional)
    Icon displayed on the button

checkable (optional, default: false)
    If true, the button is checkable

text2 (optional)
    Text displayed if the button is checked

icon2 (optional)
    Icon displayed if the button is checked

checked (optional, default: false)
    If true, the button is checked at start

iconWidth (optional)
    Icon width

iconHeight (optional)
    Icon height

This editor provides two signals:

clicked()
    Emitted when the user click on the button.

toggled(bool checked)
    Emitted when the button is checked or unchecked.

In our case, we want to show (or hide) the image slices when the button is checked (or unckecked). So, we need to
connect the ``toogled`` signal to the image adaptor slot ``showSlice(bool show)``.

.. code-block:: xml

    <connect>
        <signal>showNegatoSrv/toggled</signal>
        <slot>negatoAdp/updateVisibility</slot>
    </connect>

------------------------
SliceIndexPositionEditor
------------------------

This editor allows to change the slice index of an image.

.. code-block:: xml

    <!-- Editor representing a slider to navigate into image slices -->
    <service uid="sliderIndexEditorSrv" type="sight::module::ui::qt::image::SliceIndexPositionEditor" auto_connect="true" >
        <inout key="image" uid="image" />
        <sliceIndex>axial</sliceIndex>
    </service>

===
Run
===

To run the application, you must call the following line into the install or build directory:

.. tabs::

   .. group-tab:: Linux

        .. code::

            bin/tuto07genericscene

   .. group-tab:: Windows

        .. code::

            bin/tuto07genericscene.bat
