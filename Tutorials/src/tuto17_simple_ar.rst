.. _TutorialsTuto17SimpleAR:

************************************************************
[*tuto17_simple_ar*] Augmented-reality with *ArUco* markers
************************************************************

This tutorial shows a basic sample of augmented reality.
This exhibits:

- how to detect an *ArUco* marker,
- how to compute the pose of a marker,
- how to make a basic augmented-reality render (we superimpose a plane onto the *ArUco* marker),
- how to undistort a video according to the intrinsic parameters of the camera,
- how to distort a 3D render according to the intrinsic parameters of the camera,
- how to synchronize a video process pipeline with the video playback efficiently, using ``signal_gate``.

To use this application, you must open a calibration and a video. Samples are provided in the module folder
of the appplication, ``share/sight/TutoSimpleAR-0.4`` on Linux and ``share\TutoSimpleAR-0.4`` on Windows:

- ``calibration.xml``
- ``aruco_tag.m4v``

.. raw:: html

       <video width="700" controls>
          <source src="https://owncloud.ircad.fr/index.php/s/oyo3mDMObZcRpQM/download" >
          Your browser does not support the video tag.
       </video>

=============
Prerequisites
=============

Before reading this tutorial, you should have seen :
  - :ref:`TutorialsTuto07GenericScene`

=========
Structure
=========

--------------
CMakeLists.txt
--------------

This file describes the project information and requirements :

.. code-block:: cmake

    # Create the correct CMake target for an application
    sight_add_target(tuto17_simple_ar TYPE APP)


    set(MODULE_PATH "${CMAKE_BINARY_DIR}/${SIGHT_MODULE_RC_PREFIX}/tuto17_simple_ar")

    # Download additional resources: a calibration file and a related video
    file(DOWNLOAD https://owncloud.ircad.fr/index.php/s/ZGo6FTVOsQlWycG/download "${MODULE_PATH}/calibration.xml" SHOW_PROGRESS
        EXPECTED_HASH SHA256=2f983ec650e9df2329354c68d9aff3818741dda921c1857692886cf1bbd947b2)

    file(DOWNLOAD https://owncloud.ircad.fr/index.php/s/oyo3mDMObZcRpQM/download "${MODULE_PATH}/aruco_tag.m4v" SHOW_PROGRESS
        EXPECTED_HASH SHA256=92ffe4f0f89380985980a5c1c2fb7536543dee237cddd21922ead2c4e83ecbe2)

    # Dependencies need to be built before, but not linked with
    add_dependencies(tuto17_simple_ar
                     sightrun                   # Needed to build the launcher
                     module_app              # XML configurations
                     module_ui_base             # UI declaration/Actions
                     module_ui_qt               # Load qt implementation of gui
                     module_viz_scene3d         # Used to render a 3D scene
                     module_viz_scene3dQt       # Enable rendering things in Qt window
                     data                       # Objects declaration
                     module_service             # Service base module
                     module_ui_base             # UI declaration/Actions
                     module_ui_flaticons        # For "flat" icon style
                     module_io_matrix           # Contains services to read/write matrices from/to files.
                     module_io_vtk              # Contains the reader and writer for VTK files (image and mesh).
                     module_io_video            # Contains services for reading and writing video frames.
                     module_io_vision           # Contains services for reading and writing calibration files.
                     module_data                # Contains contains several interfaces for manager, updater and wrapper that work on objects.
                     module_sync                # Contains services that control execution flow to synchronize various data streams (frames, matrix, timelines...) with sight signals and slots.
                     module_navigation_optics   # Contains services performing optical tracking.
                     module_geometry_vision     # Contains services that implement some computer vision algorithms, notably to calibrate cameras and estimate object poses.
                     module_geometry_base       # Contains basic geometrical functions
    )

    # The ``module_param`` line defines the parameters to set for a module, here it defines the configuration
    # to launch by the module_app module, i.e. the application configuration.
    module_param(
            module_app
        PARAM_LIST
            config
        PARAM_VALUES
            Tuto17SimpleAR_AppCfg
    )

    # Allow dark theme via module_ui_qt
    module_param(
            module_ui_qt
        PARAM_LIST
            resource
            stylesheet
        PARAM_VALUES
            sight::module::ui::qt/flatdark.rcc
            sight::module::ui::qt/flatdark.qss
    )

    # Generate the profile.xml
    sight_generate_profile(tuto17_simple_ar)

.. _CMake: https://cmake.org

----------
plugin.xml
----------

This file is in the ``rc/`` directory of the application. It defines the services to run.

.. code-block:: xml

    <!--
        This tutorial shows a basic sample of augmented reality.
        This exhibits:
        - how to detect an ArUco marker,
        - how to compute the pose of a marker,
        - how to make a basic augmented-reality render (we superimpose a cube onto the ArUco marker)
        - how to undistort a video according to the camera intrinsic parameters
        - how to distort a 3D render according to the camera intrinsic parameters
        - how to synchronize efficiently a video process pipeline with the video playback using signal_gate

        To use this application, you must open a calibration and a video. Samples are provided in the module folder
        of the application, `share/sight/tuto17_simple_ar` on Linux and `share\tuto17_simple_ar` on Windows:
        - calibration.xml
        - aruco_tag.m4v
    -->
    <plugin id="tuto17_simple_ar" >

        <requirement id="sight::module::ui" />
        <requirement id="sight::module::viz::scene3d" />
        <requirement id="sight::module::ui::qt" />
        <requirement id="sight::module::service" />
        <requirement id="sight::module::ui" />

        <extension implements="sight::app::extension::config" >
            <id>Tuto17SimpleAR_AppCfg</id>
            <config>

                <!-- ******************************* Objects declaration ****************************** -->

                <!-- Series of camera calibration -->
                <object uid="cameraSet" type="sight::data::camera_set" />
                <!-- Camera calibration, deferred because it is extracted from the series -->
                <object uid="camera" type="sight::data::camera" src="deferred"/>
                <!-- Frame timeline used to buffer the output of the video grabber -->
                <object uid="frameTL" type="sight::data::frame_tl" />
                <!-- Video frame that is used for the current iteration -->
                <object uid="sourceFrame" type="sight::data::image" />
                <!-- Map containing all detected markers in the current frame -->
                <object uid="markerMap" type="sight::data::marker_map" />
                <!-- Marker matrix in the camera world -->
                <object uid="markerToCamera" type="sight::data::matrix4" />
                <!-- Camera matrix in the marker world -->
                <object uid="cameraToMarker" type="sight::data::matrix4" />
                <!-- Cube superimposed on the video at the marker location -->
                <object uid="cubeMesh" type="sight::data::mesh" />
                <object uid="undistortMap" type="sight::data::image" />
                <object uid="distortMap" type="sight::data::image" />
                <object uid="cubeMaterial" type="sight::data::material"/>

                <!-- ******************************* UI declaration *********************************** -->

                <!-- declaration of the views, menu and toolbar -->
                <service uid="mainFrame" type="sight::module::ui::frame" >
                    <gui>
                        <frame>
                            <name>tuto17_simple_ar</name>
                            <icon>tuto17_simple_ar/app.ico</icon>
                        </frame>
                        <toolBar/>
                    </gui>
                    <registry>
                        <toolBar sid="toolbarView" start="true" />
                        <view sid="cameraView" start="true" />
                    </registry>
                </service>

                <service uid="toolbarView" type="sight::module::ui::tool_bar" >
                    <gui>
                        <layout>
                            <menuItem name="Load Calibration" icon="sight::module::ui::flaticons/YellowCamera.svg" />
                            <editor/>
                            <separator/>
                            <menuItem name="Start" icon="sight::module::ui::flaticons/GreenStart.svg" shortcut="Space" />
                            <menuItem name="Pause" icon="sight::module::ui::flaticons/OrangePause.svg" shortcut="Space" />
                            <menuItem name="Play" icon="sight::module::ui::flaticons/GreenStart.svg" shortcut="Space" />
                            <menuItem name="Stop" icon="sight::module::ui::flaticons/RedStop.svg" />
                            <menuItem name="Loop" icon="sight::module::ui::flaticons/OrangeLoop.svg" style="check" />
                            <separator />
                            <menuItem name="Enable video image undistortion" icon="sight::module::ui::flaticons/YellowUndistortion.svg" />
                            <menuItem name="Disable video image undistortion" icon="sight::module::ui::flaticons/YellowDistortion.svg" />
                            <menuItem name="Enable 3D rendering distortion" icon="sight::module::ui::flaticons/YellowDistortion.svg" />
                            <menuItem name="Disable 3D rendering distortion" icon="sight::module::ui::flaticons/YellowUndistortion.svg" />
                            <separator/>
                            <menuItem name="Show Mesh on tag" icon="sight::module::ui::flaticons/Mask.svg" style="check" />
                            <separator/>
                            <menuItem name="Settings" icon="sight::module::ui::flaticons/BlueParameters.svg" style="check" />
                        </layout>
                    </gui>
                    <registry>
                        <menuItem sid="loadCalibAct" start="true" />
                        <editor sid="videoSelectorSrv" />
                        <menuItem sid="startVideoAct" start="true" />
                        <menuItem sid="pauseVideoAct" start="true" />
                        <menuItem sid="resumeVideoAct" start="true" />
                        <menuItem sid="stopVideoAct" start="true" />
                        <menuItem sid="loopVideoAct" start="true" />
                        <menuItem sid="startUndistortAct" start="true" />
                        <menuItem sid="stopUndistortAct" start="true" />
                        <menuItem sid="startDistortAct" start="true" />
                        <menuItem sid="stopDistortAct" start="true" />
                        <menuItem sid="showMeshAct" start="true" />
                        <menuItem sid="showParametersAct" start="true" />
                    </registry>
                </service>

                <service uid="cameraView" type="sight::module::ui::view" >
                    <gui>
                        <layout type="sight::ui::layout::line" >
                            <orientation value="horizontal" />
                            <view proportion="1" />
                            <view proportion="0" backgroundColor="#36393E" />
                        </layout>
                    </gui>
                    <registry>
                        <view sid="videoView" start="true" />
                        <view sid="parametersView" start="true" />
                    </registry>
                </service>

                <service uid="videoView" type="sight::module::ui::view" >
                    <gui>
                        <layout type="sight::ui::layout::line" >
                            <orientation value="vertical" />
                            <view proportion="3" />
                            <view proportion="0" backgroundColor="#36393E" />
                        </layout>
                    </gui>
                    <registry>
                        <view sid="genericSceneSrv" />
                        <view sid="errorLabelSrv" start="true" />
                    </registry>
                </service>

                <service uid="parametersView" type="sight::module::ui::view" >
                    <gui>
                        <layout type="sight::ui::layout::line" >
                            <orientation value="vertical" />
                            <view proportion="3" />
                            <view proportion="2" />
                            <view proportion="1" />
                            <spacer/>
                        </layout>
                    </gui>
                    <registry>
                        <view sid="arucoParamametersSrv" start="true" />
                        <view sid="reprojectionParamametersSrv" start="true" />
                        <view sid="materialOpacityEditor" start="true" />
                    </registry>
                </service>

                <!-- ************************************* Action ************************************ -->

                <!-- declaration of actions/slot callers -->
                <service uid="showParametersAct" type="sight::module::ui::modify_layout" >
                    <config>
                        <show_or_hide sid="parametersView" />
                    </config>
                </service>

                <service uid="loadCalibAct" type="sight::module::ui::action" />

                <!-- Start the frame grabber -->
                <service uid="startVideoAct" type="sight::module::ui::action" >
                    <state enabled="false" />
                </service>

                <!-- Pause the frame grabber -->
                <service uid="pauseVideoAct" type="sight::module::ui::action" >
                    <state visible="false" />
                </service>

                <!-- Resume the frame grabber -->
                <service uid="resumeVideoAct" type="sight::module::ui::action" >
                    <state visible="false" />
                </service>

                <!-- Stop the frame grabber -->
                <service uid="stopVideoAct" type="sight::module::ui::action" >
                    <state enabled="false" />
                </service>

                <!-- Loop the frame grabber -->
                <service uid="loopVideoAct" type="sight::module::ui::action" >
                    <state enabled="false" />
                </service>

                <service uid="startUndistortAct" type="sight::module::ui::action" />

                <service uid="stopUndistortAct" type="sight::module::ui::action" >
                    <state visible="false" />
                </service>

                <service uid="startDistortAct" type="sight::module::ui::action" />

                <service uid="stopDistortAct" type="sight::module::ui::action" >
                    <state visible="false" />
                </service>

                <service uid="showMeshAct" type="sight::module::ui::action" >
                    <state checked="true" />
                </service>

                <!-- ******************************* Begin Generic Scene ******************************* -->

                <!-- Scene in which the video and the 3D will be rendered -->
                <!-- In this tutorial, we move the camera and the marker mesh is fixed -->
                <service uid="genericSceneSrv" type="sight::viz::scene3d::render" >
                    <!-- It is essential to use the 'sync' mode when doing AR -->
                    <!-- In this mode, the renderer will wait for a signal to trigger the rendering -->
                    <scene renderMode="sync" >
                        <background topColor="#36393E" bottomColor="#36393E" />

                        <layer id="video" order="1" compositors="Remap" transparency=""/>
                        <adaptor uid="videoAdp" />
                        <adaptor uid="undistortAdp" />

                        <layer id="default" order="3" compositors="Remap" transparency=""/>
                        <adaptor uid="axisAdp" />
                        <adaptor uid="cameraAdp" />
                        <adaptor uid="materialAdp" />
                        <adaptor uid="cubeAdp" />
                        <adaptor uid="distortAdp" />
                    </scene>
                </service>

                <service uid="videoAdp" type="sight::module::viz::scene3d::adaptor::video" auto_connect="true" >
                    <in key="image" uid="sourceFrame" />
                    <config layer="video" />
                </service>

                <service uid="undistortAdp" type="sight::module::viz::scene3d::adaptor::compositor_parameter" auto_connect="true" >
                    <inout key="parameter" uid="undistortMap" />
                    <config layer="video" compositorName="Remap" parameter="u_map" shaderType="fragment" visible="false" />
                </service>

                <service uid="axisAdp" type="sight::module::viz::scene3d::adaptor::axis" >
                    <config layer="default" length="30" origin="true" label="false" />
                </service>

                <!-- Camera for the 3D layer -->
                <service uid="cameraAdp" type="sight::module::viz::scene3d::adaptor::camera" auto_connect="true" >
                    <inout key="transform" uid="cameraToMarker" />
                    <in key="calibration" uid="camera" />
                    <config layer="default" />
                </service>

                <service uid="materialAdp" type="sight::module::viz::scene3d::adaptor::material" auto_connect="true">
                    <inout key="material" uid="cubeMaterial" />
                    <config layer="default" materialName="cube" />
                </service>

                <!-- Cube displayed on top of the marker plane -->
                <service uid="cubeAdp" type="sight::module::viz::scene3d::adaptor::mesh" auto_connect="true" >
                    <in key="mesh" uid="cubeMesh" />
                    <config layer="default" autoresetcamera="false" materialName="cube" />
                </service>

                <service uid="distortAdp" type="sight::module::viz::scene3d::adaptor::compositor_parameter" auto_connect="true" >
                    <inout key="parameter" uid="distortMap" />
                    <config layer="default" compositorName="Remap" parameter="u_map" shaderType="fragment" visible="false" />
                </service>

                <!-- ************************************* Services ************************************ -->

                <service uid="loadMeshSrv" type="sight::module::io::vtk::meshReader" >
                    <inout key="data" uid="cubeMesh" />
                    <resource>tuto17_simple_ar/cube_60.vtk</resource>
                </service>

                <!-- hide axis adaptor until a marker is found -->
                <service uid="hideAxisSrv" type="sight::module::ui::action" />

                <!-- Calibration reader (here OpenCV's XML/YAML) -->
                <service uid="calibrationReaderSrv" type="sight::module::io::vision::open_cv_reader" >
                    <inout key="data" uid="cameraSet" />
                </service>

                <!-- extract the first sight::data::camera from the sight::data::camera_set -->
                <service uid="extractCameraSrv" type="sight::module::data::get_camera" >
                    <inout key="cameraSet" uid="cameraSet" />
                    <out group="camera" >
                        <key index="0" uid="camera" />
                    </out>
                </service>

                <!-- GUI to handle aruco tracking parameters -->
                <service uid="arucoParamametersSrv" type="sight::module::ui::qt::parameters" >
                    <parameters>
                        <!-- show marker or not -->
                        <param type="bool" name="Show Marker" key="debugMode" defaultValue="true" />
                        <!--  do corner refinement or not. -->
                        <param type="bool" name="Corner refinement." key="corner" defaultValue="true" />
                        <!-- minimum window size for adaptive thresholding before finding contours -->
                        <param type="int" name="adpt. Threshold win size min" key="adaptiveThreshWinSizeMin" defaultValue="3" min="3" max="100" />
                        <!-- maximum window size for adaptive thresholding before finding contours -->
                        <param type="int" name="adpt. Threshold win size max" key="adaptiveThreshWinSizeMax" defaultValue="23" min="4" max="100" />
                        <!-- increments from adaptiveThreshWinSizeMin to adaptiveThreshWinSizeMax during the thresholding -->
                        <param type="int" name="adpt. Threshold win size step" key="adaptiveThreshWinSizeStep" defaultValue="10" min="1" max="100" />
                        <!-- constant for adaptive thresholding before finding contours -->
                        <param type="double" name="adpt. threshold constant" key="adaptiveThreshConstant" defaultValue="7." min="0." max="30." />
                        <!-- determine minimum perimeter for marker contour to be detected.
                            This is defined as a rate respect to the maximum dimension of the input image -->
                        <param type="double" name="Min. Marker Perimeter Rate" key="minMarkerPerimeterRate" defaultValue="0.03" min="0.01" max="1.0" />
                        <!-- determine maximum perimeter for marker contour to be detected.
                            This is defined as a rate respect to the maximum dimension of the input image -->
                        <param type="double" name="Max. Marker Perimeter Rate" key="maxMarkerPerimeterRate" defaultValue="4.0" min="1." max="10." />
                        <!-- minimum accuracy during the polygonal approximation process to determine which contours are squares -->
                        <param type="double" name="Polygonal Approx. Accuracy Rate" key="polygonalApproxAccuracyRate" defaultValue="0.03" min="0.01" max="1." />
                        <!-- minimum distance between corners for detected markers relative to its perimeter -->
                        <param type="double" name="Min. Corner Distance Rate" key="minCornerDistanceRate" defaultValue="0.01" min="0." max="1." />
                        <!-- minimum distance of any corner to the image border for detected markers (in pixels) -->
                        <param type="int" name="Min. Distance to Border" key="minDistanceToBorder" defaultValue="1" min="0" max="10" />
                        <!-- minimum mean distance beetween two marker corners to be considered similar,
                        so that the smaller one is removed. The rate is relative to the smaller perimeter of the two markers -->
                        <param type="double" name="Min. Marker Distance Rate" key="minMarkerDistanceRate" defaultValue="0.01" min="0." max="1." />
                        <!-- window size for the corner refinement process (in pixels) -->
                        <param type="int" name="Corner Refinement Win. Size" key="cornerRefinementWinSize" defaultValue="5" min="1" max="100" />
                        <!-- maximum number of iterations for stop criteria of the corner refinement process -->
                        <param type="int" name="Corner Refinement Max Iterations" key="cornerRefinementMaxIterations" defaultValue="30" min="1" max="10" />
                        <!-- minimum error for the stop cristeria of the corner refinement process -->
                        <param type="double" name="Corner Refinement Min. Accuracy" key="cornerRefinementMinAccuracy" defaultValue="0.1" min="0.01" max="10." />
                        <!-- number of bits of the marker border, i.e. marker border width -->
                        <param type="int" name="Marker Border Bits" key="markerBorderBits" defaultValue="1" min="1" max="100" />
                        <!-- number of bits (per dimension) for each cell of the marker when removing the perspective -->
                        <param type="int" name="Perspective Remove Pixel per Cell" key="perspectiveRemovePixelPerCell" defaultValue="8" min="1" max="32" />
                        <!-- width of the margin of pixels on each cell not considered for the determination of the cell bit.
                            Represents the rate respect to the total size of the cell,
                            i.e. perpectiveRemovePixelPerCel -->
                        <param type="double" name="Perspective Remove Ignored Margin Per Cell" key="perspectiveRemoveIgnoredMarginPerCell" defaultValue="0.13" min="0." max="1." />
                        <!-- maximum number of accepted erroneous bits in the border (i.e. number of allowed white bits in the border).
                            Represented as a rate respect to the total number of bits per marker -->
                        <param type="double" name="Max. Erroneous Bits In Border Rate" key="maxErroneousBitsInBorderRate" defaultValue="0.35" min="0." max="1." />
                        <!-- minimun standard deviation in pixels values during the decodification step to apply Otsu thresholding
                            (otherwise, all the bits are set to 0 or 1 depending on mean higher than 128 or not) -->
                        <param type="double" name="Min. Otsu Std. Dev." key="minOtsuStdDev" defaultValue="5.0" min="0." max="100." />
                        <!-- error correction rate respect to the maximun error correction capability for each dictionary -->
                        <param type="double" name="Error Correction Rate" key="errorCorrectionRate" defaultValue="0.6" min="0." max="1." />
                    </parameters>
                </service>

                <service uid="reprojectionParamametersSrv" type="sight::module::ui::qt::parameters" >
                    <parameters>
                        <param type="bool" name="Show reprojection" key="display" defaultValue="true" />
                        <param type="color" name="Circle color" key="color" defaultValue="#ffffff" />
                    </parameters>
                </service>

                <!-- Gui Service to display a value in a QLabel -->
                <service uid="errorLabelSrv" type="sight::module::ui::qt::textStatus" >
                    <label>Reprojection error (RMSE)</label>
                    <color>#D25252</color>
                </service>

                <!-- GUI to select camera (device, file, or stream) -->
                <service uid="videoSelectorSrv" type="sight::module::ui::qt::video::camera" >
                    <inout key="camera" uid="camera" />
                    <videoSupport>true</videoSupport>
                </service>

                <!-- Grab image from camera device and fill a frame timeline -->
                <service uid="videoGrabberSrv" type="sight::module::io::video::frame_grabber" >
                    <in key="camera" uid="camera" />
                    <inout key="frameTL" uid="frameTL" />
                </service>

                <!-- Consumes a frame in the timeline, picks the latest one to be processed -->
                <!-- It is overkill in this sample, but mandatory when we use multiple grabbers to synchronize them. -->
                <service uid="frameUpdaterSrv" type="sight::module::sync::frameMatrixSynchronizer" >
                    <in group="frameTL" >
                        <key uid="frameTL" auto_connect="true" />
                    </in>
                    <inout group="image" >
                        <key uid="sourceFrame" />
                    </inout>
                    <tolerance>100</tolerance>
                    <framerate>0</framerate>
                </service>

                <!-- Aruco tracker service -->
                <service uid="trackerSrv" type="sight::module::navigation::optics::aruco_tracker" worker="tracking" >
                    <in key="camera" uid="camera" />
                    <inout key="frame" uid="sourceFrame" auto_connect="true" />
                    <inout group="markerMap" >
                        <key uid="markerMap" /> <!-- timeline of detected tag(s) -->
                    </inout>
                    <track>
                        <!-- list of tag's id -->
                        <marker id="101" />
                    </track>
                    <debugMarkers>true</debugMarkers>
                </service>

                <!-- Computes the pose of the camera with tag(s) detected by aruco -->
                <service uid="registrationSrv" type="sight::module::geometry::vision::pose_from2d" worker="tracking" >
                    <in group="markerMap" auto_connect="true" >
                        <key uid="markerMap" />
                    </in>
                    <in group="camera" >
                        <key uid="camera" />
                    </in>
                    <inout group="matrix" >
                        <key uid="markerToCamera" id="101" />
                    </inout>
                    <patternWidth>60</patternWidth>
                </service>

                <!-- Computes the reprojection error -->
                <service uid="errorSrv" type="sight::module::geometry::vision::reprojection_error" worker="error" >
                    <in group="matrix" auto_connect="true" >
                        <key uid="markerToCamera" id="101" />
                    </in>
                    <in key="markerMap" uid="markerMap" />
                    <in key="camera" uid="camera" />
                    <inout key="frame" uid="sourceFrame" />
                    <patternWidth>60</patternWidth>
                </service>

                <!-- Multiply matrices (here only used to inverse "markerToCamera") -->
                <service uid="matrixReverserSrv" type="sight::module::geometry::base::concatenate_matrices" >
                    <in group="matrix" >
                        <key uid="markerToCamera" auto_connect="true" inverse="true" />
                    </in>
                    <inout key="output" uid="cameraToMarker" />
                </service>

                <service uid="undistorterSrv" type="sight::module::geometry::vision::distortion" >
                    <in key="camera" uid="camera" auto_connect="true" />
                    <inout key="map" uid="undistortMap" />
                    <mode>undistort</mode>
                </service>

                <service uid="distorterSrv" type="sight::module::geometry::vision::distortion" >
                    <in key="camera" uid="camera" auto_connect="true" />
                    <inout key="map" uid="distortMap" />
                    <mode>distort</mode>
                </service>

                <!-- Wait for the undistortion and the matrix inversion to be finished -->
                <service uid="syncGenericSceneSrv" type="sight::module::sync::signal_gate" >
                    <signal>sourceFrame/bufferModified</signal>
                    <signal>cameraToMarker/modified</signal>
                </service>

                <service uid="materialOpacityEditor" type="sight::module::ui::qt::materialOpacityEditor">
                    <inout key="material" uid="cubeMaterial"/>
                    <label>Cube opacity</label>
                </service>

                <!-- ******************************* Connections ***************************************** -->

                <connect>
                    <signal>showMeshAct/isChecked</signal>
                    <slot>cubeAdp/updateVisibility</slot>
                </connect>

                <connect>
                    <signal>hideAxisSrv/isChecked</signal>
                    <slot>axisAdp/updateVisibility</slot>
                </connect>

                <connect>
                    <signal>videoSelectorSrv/configuredFile</signal>
                    <signal>videoSelectorSrv/configuredStream</signal>
                    <signal>videoSelectorSrv/configuredDevice</signal>
                    <slot>startVideoAct/update</slot>
                </connect>

                <connect>
                    <signal>videoGrabberSrv/cameraStarted</signal>
                    <slot>pauseVideoAct/show</slot>
                    <slot>startVideoAct/hide</slot>
                    <slot>stopVideoAct/enable</slot>
                    <slot>loopVideoAct/enable</slot>
                </connect>

                <connect>
                    <signal>camera/idModified</signal>
                    <slot>videoGrabberSrv/stopCamera</slot>
                </connect>

                <connect>
                    <signal>camera/modified</signal>
                    <slot>startVideoAct/enable</slot>
                    <slot>stopVideoAct/update</slot>
                </connect>

                <!-- signal/slot connection -->
                <connect>
                    <signal>reprojectionParamametersSrv/colorChanged</signal>
                    <slot>errorSrv/setColorParameter</slot>
                </connect>

                <connect>
                    <signal>reprojectionParamametersSrv/boolChanged</signal>
                    <slot>errorSrv/setBoolParameter</slot>
                </connect>

                <connect>
                    <signal>errorSrv/errorComputed</signal>
                    <slot>errorLabelSrv/setDoubleParameter</slot>
                </connect>

                <connect>
                    <signal>arucoParamametersSrv/boolChanged</signal>
                    <slot>trackerSrv/setBoolParameter</slot>
                </connect>

                <connect>
                    <signal>arucoParamametersSrv/intChanged</signal>
                    <slot>trackerSrv/setIntParameter</slot>
                </connect>

                <connect>
                    <signal>arucoParamametersSrv/doubleChanged</signal>
                    <slot>trackerSrv/setDoubleParameter</slot>
                </connect>

                <connect>
                    <signal>cameraSet/modified</signal>
                    <slot>extractCameraSrv/update</slot>
                    <slot>errorSrv/update</slot>
                </connect>

                <connect>
                    <signal>trackerSrv/markerDetected</signal>
                    <slot>axisAdp/updateVisibility</slot>
                </connect>

                <!-- When the undistortion and the matrix inversion are done, trigger the rendering -->
                <!-- then process a new frame -->
                <connect>
                    <signal>syncGenericSceneSrv/allReceived</signal>
                    <slot>genericSceneSrv/requestRender</slot>
                    <slot>frameUpdaterSrv/synchronize</slot>
                </connect>

                <connect>
                    <signal>loadCalibAct/updated</signal>
                    <slot>calibrationReaderSrv/update</slot>
                </connect>

                <connect>
                    <signal>startVideoAct/updated</signal>
                    <slot>videoGrabberSrv/startCamera</slot>
                </connect>

                <connect>
                    <signal>pauseVideoAct/updated</signal>
                    <slot>videoGrabberSrv/pauseCamera</slot>
                    <slot>resumeVideoAct/show</slot>
                    <slot>pauseVideoAct/hide</slot>
                </connect>

                <connect>
                    <signal>resumeVideoAct/updated</signal>
                    <slot>videoGrabberSrv/pauseCamera</slot>
                    <slot>resumeVideoAct/hide</slot>
                    <slot>pauseVideoAct/show</slot>
                </connect>

                <connect>
                    <signal>stopVideoAct/updated</signal>
                    <slot>videoGrabberSrv/stopCamera</slot>
                    <slot>startVideoAct/show</slot>
                    <slot>resumeVideoAct/hide</slot>
                    <slot>pauseVideoAct/hide</slot>
                    <slot>stopVideoAct/disable</slot>
                    <slot>loopVideoAct/disable</slot>
                    <slot>loopVideoAct/uncheck</slot>
                </connect>

                <connect>
                    <signal>loopVideoAct/updated</signal>
                    <slot>videoGrabberSrv/loopVideo</slot>
                </connect>

                <connect>
                    <signal>startUndistortAct/updated</signal>
                    <slot>undistortAdp/show</slot>
                    <slot>startDistortAct/disable</slot>
                    <slot>startUndistortAct/hide</slot>
                    <slot>stopUndistortAct/show</slot>
                </connect>

                <connect>
                    <signal>stopUndistortAct/updated</signal>
                    <slot>undistortAdp/hide</slot>
                    <slot>startDistortAct/enable</slot>
                    <slot>startUndistortAct/show</slot>
                    <slot>stopUndistortAct/hide</slot>
                </connect>

                <connect>
                    <signal>startDistortAct/updated</signal>
                    <slot>distortAdp/show</slot>
                    <slot>startUndistortAct/disable</slot>
                    <slot>startDistortAct/hide</slot>
                    <slot>stopDistortAct/show</slot>
                </connect>

                <connect>
                    <signal>stopDistortAct/updated</signal>
                    <slot>distortAdp/hide</slot>
                    <slot>startUndistortAct/enable</slot>
                    <slot>startDistortAct/show</slot>
                    <slot>stopDistortAct/hide</slot>
                </connect>

                <!-- ******************************* Start services ***************************************** -->

                <start uid="mainFrame" />
                <start uid="videoGrabberSrv" />
                <start uid="frameUpdaterSrv" />
                <start uid="registrationSrv" />
                <start uid="trackerSrv" />
                <start uid="calibrationReaderSrv" />
                <start uid="videoSelectorSrv" />
                <start uid="extractCameraSrv" />
                <start uid="matrixReverserSrv" />
                <start uid="errorSrv" />
                <start uid="hideAxisSrv" />
                <start uid="undistorterSrv" />
                <start uid="distorterSrv" />
                <start uid="syncGenericSceneSrv" />
                <start uid="loadMeshSrv" />
                <start uid="genericSceneSrv" />
                <start uid="materialAdp" />
                <start uid="videoAdp" />
                <start uid="undistortAdp" />
                <start uid="axisAdp" />
                <start uid="cameraAdp" />
                <start uid="cubeAdp" />
                <start uid="distortAdp" />

                <!-- ******************************* Update services ***************************************** -->

                <!-- At launch, enable the synchronization with the non-offscreen rendering -->
                <update uid="showParametersAct" />
                <update uid="hideAxisSrv" />
                <update uid="loadMeshSrv" />
                <update uid="arucoParamametersSrv" />
                <update uid="reprojectionParamametersSrv" />

            </config>
        </extension>
    </plugin>


========
Tracking
========

Tag detection is done with the ``sight::module::navigation::optics::aruco_tracker`` service, which takes a video frame
as input, and fills in a map of identified *ArUco* markers. You only have to specify which marker identifier you
want to retrieve, here we choose the tag **101** because it is the one seen in the video sample.

.. code-block:: xml

    <service uid="trackerSrv" type="sight::module::navigation::optics::aruco_tracker" worker="tracking" >
        <in key="camera" uid="camera" />
        <inout key="frame" uid="sourceFrame" auto_connect="true" />
        <inout group="markerMap" >
            <key uid="markerMap" /> <!-- timeline of detected tag(s) -->
        </inout>
        <track>
            <!-- list of tag's id -->
            <marker id="101" />
        </track>
        <debugMarkers>true</debugMarkers>
    </service>

The map of markers is a ``sight::data::marker_map``, which stores, for each tag identifier, a list of 2D coordinates
corresponding to the shape of the marker. In the *ArUco* case, the markers are squared so you get four 2D coordinates
per marker.

Once you get the markers, you want to get the 3D pose of each marker in the camera space. The
``sight::module::geometry::vision::pose_from2d`` service takes the previous markers map as input, along with the calibration
of the camera of type ``sight::data::camera``. Each time the map is updated, it fills in a matrix for each identifier,
so here for the tag **101**.

.. code-block:: xml

    <service uid="registrationSrv" type="sight::module::geometry::vision::pose_from2d" worker="tracking" >
        <in group="markerMap" auto_connect="true" >
            <key uid="markerMap" />
        </in>
        <in group="camera" >
            <key uid="camera" />
        </in>
        <inout group="matrix" >
            <key uid="markerToCamera" id="101" />
        </inout>
        <patternWidth>60</patternWidth>
    </service>

===============
Augmented view
===============

Now that we get the 3D pose of the marker, it is pretty straightforward to display an object at this location on
top of the video. In the sample, a ``sight::data::mesh`` is preloaded and contains a cube whose sides have the same
dimensions as the *ArUco* marker.

In the generic scene, a first adaptor is used to display the video on the background layer:

.. code-block:: xml

    <service uid="videoAdp" type="sight::module::viz::scene3d::adaptor::video" auto_connect="true" >
        <in key="image" uid="sourceFrame" />
        <config layer="video" />
    </service>

Then, to display the cube in 3D, we setup a scene where the cube does not move but the camera receives the inverse
transform of the pose of the marker.

.. code-block:: xml

    <!-- Camera for the 3D layer -->
    <service uid="cameraAdp" type="sight::module::viz::scene3d::adaptor::camera" auto_connect="true" >
        <inout key="transform" uid="cameraToMarker" />
        <in key="calibration" uid="camera" />
        <config layer="default" />
    </service>

    <!-- Cube displayed on top of the marker plane -->
    <service uid="cubeAdp" type="sight::module::viz::scene3d::adaptor::mesh" auto_connect="true" >
        <inout key="mesh" uid="cubeMesh" />
        <config layer="default" autoresetcamera="false" materialName="cube" />
    </service>

To compute the inverse matrix we use the ``concatenate_matrices`` service that can be used to multiply transform
matrices and also to invert them at the same time :

.. code-block:: xml

    <!-- Multiply matrices (here only used to inverse "markerToCamera") -->
    <service uid="matrixReverserSrv" type="sight::module::geometry::base::concatenate_matrices" >
        <in group="matrix" >
            <key uid="markerToCamera" auto_connect="true" inverse="true" />
        </in>
        <inout key="output" uid="cameraToMarker" />
    </service>

================
Lens distortion
================

We offer the possiblity to apply the lens distortion correction either to the video or to the 3D rendering. In the
first case we undistort the video, and in the second case we distort the 3D rendering. Undistorting the video is
more common and easier, but in the field of surgery with laparoscopic or endoscopic videos, it may be preferable or
even mandatory to not alter the video image. This is why we give both options.

We can use the same ``sight::module::calibration::video::distortion`` service for both cases. Here is the configuration used in the
tutorial to undistort the video:

.. code-block:: xml

    <service uid="undistorterSrv" type="sight::module::geometry::vision::distortion" >
        <in key="camera" uid="camera" auto_connect="true" />
        <inout key="map" uid="undistortMap" />
        <mode>undistort</mode>
    </service>

The service takes the calibration camera and it outputs an undistortion map.

A second instance of the service can be used to compute the distortion map:

.. code-block:: xml

    <service uid="distorterSrv" type="sight::module::geometry::vision::distortion" >
        <in key="camera" uid="camera" auto_connect="true" />
        <inout key="map" uid="distortMap" />
        <mode>distort</mode>
    </service>

These maps are used to undistort the video or distort the scene directly in the rendering pipeline. To achive this, we
apply a compositor on the video layer and the scene layer, this compositor allows to remap the rendering with
a given map.

.. code-block:: xml

    <!-- Scene in which the video and the 3D will be rendered -->
    <!-- In this tutorial, we move the camera and the marker mesh is fixed -->
    <service uid="genericSceneSrv" type="sight::viz::scene3d::render" >
        <!-- It is essential to use the 'sync' mode when doing AR -->
        <!-- In this mode, the renderer will wait for a signal to trigger the rendering -->
        <scene renderMode="sync" >
            <background topColor="#36393E" bottomColor="#36393E" />

            <layer id="video" order="1" compositors="Remap" transparency=""/>
            <adaptor uid="videoAdp" />
            <adaptor uid="undistortAdp" />

            <layer id="default" order="3" compositors="Remap" transparency=""/>
            <adaptor uid="axisAdp" />
            <adaptor uid="cameraAdp" />
            <adaptor uid="materialAdp" />
            <adaptor uid="cubeAdp" />
            <adaptor uid="distortAdp" />
        </scene>
    </service>

==========
Compositor
==========

To give the map to each compositor, we used a new adaptor ``sight::module::viz::scene3d::adaptor::compositor_parameter``,
it allows to bind a sight data to a compositor.
The first one gives the undistortion map to the video layer.

.. code-block:: xml

    <service uid="undistortAdp" type="sight::module::viz::scene3d::adaptor::compositor_parameter" auto_connect="true" >
        <inout key="parameter" uid="undistortMap" />
        <config layer="video" compositorName="Remap" parameter="u_map" shaderType="fragment" visible="false" />
    </service>

And the second one gives the distortion map to the scene layer.

.. code-block:: xml

    <service uid="distortAdp" type="sight::module::viz::scene3d::adaptor::compositor_parameter" auto_connect="true" >
        <inout key="parameter" uid="distortMap" />
        <config layer="default" compositorName="Remap" parameter="u_map" shaderType="fragment" visible="false" />
    </service>

By default, these two compositor are disabled (``visible="false"``), and only one of them can be enabled at the same
time, this is perform by to services ``sight::module::ui::com::SSlotCaller``.

.. code-block:: xml

    <service uid="startUndistortAct" type="sight::module::ui::com::SSlotCaller" >
        <slots>
            <slot>undistortAdp/show</slot>
            <slot>startDistortAct/setInexecutable</slot>
            <slot>startUndistortAct/hide</slot>
            <slot>stopUndistortAct/show</slot>
        </slots>
    </service>

    <service uid="stopUndistortAct" type="sight::module::ui::com::SSlotCaller" >
        <state visible="false" />
        <slots>
            <slot>undistortAdp/hide</slot>
            <slot>startDistortAct/setExecutable</slot>
            <slot>startUndistortAct/show</slot>
            <slot>stopUndistortAct/hide</slot>
        </slots>
    </service>

================
Synchronization
================

The last important part of the tutorial is the synchronization of all these services to get the cube always perfectly
aligned with the *ArUco* marker of the video. What we want to obtain is simple:

- decode a video frame sight::module::io::qt::frame_grabber
- detect the tag
- extract the pose
- compute the inverse matrix
- render the scene

However it is not that easy to achieve because the services work independently on different worker threads. So the
execution process rather looks like this :

.. figure:: ../media/TutorialsTuto17SimpleARSync1.png
    :scale: 100
    :align: center
    :alt: TutorialsTuto17SimpleARSync1

To pipeline all of those services together we use signals and slots.
We first retrieve a frame from the frame timeline filled by the ``frame_grabber`` with the help of
the ``sight::module::io::video::frameMatrixSynchronizer`` service:

Thanks to the auto connections, the modification of ``sourceFrame`` triggers the video adaptor in
``sight::module::viz::scene3d::adaptor::video`` and in parallel the detection in ``sight::module::navigation::optics::aruco_tracker``, which then
modifies the marker map. The modification of the marker map then triggers the computation of the pose in
``sight::module::geometry::vision::pose_from2d``. Last the modification of the ``markerToCamera`` matrix triggers the computation
of the inverse matrix ``cameraToMarker``.

Now to trigger the rendering of the scene, we simply use the ``signal_gate`` service which waits on several signals to
be triggered before sending a signal. It is configured by simply giving it the list of signals :

.. code-block:: xml

    <service uid="syncGenericSceneSrv" type="sight::module::sync::signal_gate" >
        <signal>sourceFrame/bufferModified</signal>
        <signal>cameraToMarker/modified</signal>
    </service>

    <connect>
        <signal>syncGenericSceneSrv/allReceived</signal>
        <slot>genericSceneSrv/requestRender</slot>
        <slot>frameUpdaterSrv/synchronize</slot>
    </connect>

Note that in addition with launching the scene rendering, we also request the frame synchronizer to consume
a new frame, which triggers a new iteration of the pipeline process.

At the end, the execution process looks like this:

.. figure:: ../media/TutorialsTuto17SimpleARSync2.png
    :scale: 100
    :align: center
    :alt: TutorialsTuto17SimpleARSync2

Please also note that by default, the generic scene renders each time the input of any of there adaptors is modified.
To disable this behavior and synchronize only when requested, we set the ``renderMode`` attribute to ``sync``:

.. code-block:: xml

    <service uid="genericSceneSrv" type="sight::viz::scene3d::render" >
        <scene renderMode="sync" >
            {...}
        </scene>
    </service>

===
Run
===

To run the application, you must call the following line into the install or build directory:

.. tabs::

   .. group-tab:: Linux

        .. code::

            bin/tutosimplear

   .. group-tab:: Windows

        .. code::

            bin/tutosimplear.bat

