.. _TutorialsTuto01basic:

***************************************
[*tuto01_basic*] Create an application
***************************************

The first tutorial represents a basic application that launches a simple empty frame. It introduces the concept of XML
application configuration and CMake generation.

.. figure:: ../media/tuto01Basic.png
    :scale: 25
    :align: center
    :alt: tuto01_basic

=========
Structure
=========

Sight is organized around four elements: the ``application``, the ``module``, the ``library`` and the ``utility``.

The ``applications`` contain the configuration of the ``modules`` (and its services) to launch. The ``modules`` contain
the cpp implementation of the services, it may also contain some application sub-configuration. The ``libraries``
contain the data implementation and the code shared with several modules. The ``utilities`` are simple executables using
the ``libraries``.

In this example, we will only explain how to create a basic application with the existing modules. Further Tutorials
will explain how to use and create services and modules.

A sight application is organized around two main files :
 * CMakeLists.txt
 * plugin.xml

--------------
CMakeLists.txt
--------------

The CMakeLists.txt is parsed by CMake_. For an application, it should contain the following lines :

.. code-block:: cmake

    # Create the correct CMake target for an application
    sight_add_target(tuto01_basic TYPE APP)

    # Dependencies need to be built before, but not linked with
    add_dependencies(tuto01_basic
                     sightrun           # Needed to build the launcher
                     module_app      # XML configurations
                     module_ui_base     # UI declaration/Actions
                     module_ui_qt       # Load qt implementation of gui
                     module_service     # Service base module
    )

    # The ``module_param`` line defines the parameters to set for a module, here it defines the configuration
    # to launch by the module_app module, i.e. the application configuration.
    module_param(
            module_app
        PARAM_LIST
            config
        PARAM_VALUES
            Tuto01Basic_AppCfg
    )

    # Generate the profile.xml
    sight_generate_profile(tuto01_basic)

.. _CMake: https://cmake.org

.. note::

    The ``sight_generate_profile()`` generate the ``profile.xml``, used to launch the application
    (see :ref:`profile.xml`).

----------
plugin.xml
----------

This file is located in the ``rc/`` directory of the application. It contains the application configuration.

.. code-block:: xml

    <!-- Application name and version (the version is automatically replaced by CMake
        using the version defined in the Properties.cmake) -->
    <plugin id="tuto01_basic" >

        <!-- The modules in requirements are automatically started when this
            Application is launched. -->
        <requirement id="sight::module::service" />
        <requirement id="sight::module::ui::qt" />

        <!-- Defines the App-config -->
        <extension implements="sight::app::extension::config" >
            <id>Tuto01Basic_AppCfg</id><!-- identifier of the configuration -->
            <config>

                <!-- ******************************* UI declaration *********************************** -->

                <service uid="myFrame" type="sight::module::ui::frame" >
                    <gui>
                        <frame>
                            <name>tuto01_basic</name>
                            <icon>tuto01_basic/tuto.ico</icon>
                            <minSize width="800" height="600" />
                        </frame>
                    </gui>
                </service>

                <!-- ******************************* Start services ***************************************** -->

                <start uid="myFrame" />

            </config>
        </extension>
    </plugin>



``<requirement>`` lists the modules that should be loaded before launching the application:
the module to register data or i/o services (see Requirements_).

The ``sight::app::extension::config`` extension defines the configuration of an application:

**id**:
    The configuration identifier.
**config**:
    Contains the list of objects and services used by the application.
    For this tutorial, we have no object and only one service ``sight::module::ui::frame``.
    There are few others tags that will be described in the next tutorials.

.. _Requirements: https://sight.pages.ircad.fr/sight/group__requirement.html

===
Run
===

To run the application, you must call the following line into the install or build directory:

.. tabs::

   .. group-tab:: Linux

        .. code::

            bin/tuto01basic

   .. group-tab:: Windows

        .. code::

            bin/tuto01basic.bat
