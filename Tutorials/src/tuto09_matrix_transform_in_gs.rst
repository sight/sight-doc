.. _TutorialsTuto09MatrixTransformInGS:

************************************************
[*tuto09_matrix_transform_in_gs*] Generic scene
************************************************

This tutorial explains how to use matrices and concatenation of matrices.

.. figure:: ../media/tuto09_matrix_transform_in_gs.png
    :scale: 25
    :align: center
    :alt: tuto09MatrixTransformInGS

=============
Prerequisites
=============

Before reading this tutorial, you should have seen :
 * :ref:`TutorialsTuto07GenericScene`
 * :ref:`SADGenericScene`

=========
Structure
=========

--------------
CMakeLists.txt
--------------

This file describes the project information and requirements :

.. code-block:: cmake

    # Create the correct CMake target for an application
    sight_add_target(tuto09_matrix_transform_in_gs TYPE APP)

    # Dependencies need to be built before, but not linked with
    add_dependencies(tuto09_matrix_transform_in_gs
                     sightrun               # Needed to build the launcher
                     module_app          # XML configurations
                     module_ui_base         # UI declaration/Actions
                     module_ui_qt           # Load qt implementation of gui
                     module_ui_flaticons    # For "flat" icon style
                     module_viz_scene3d     # Used to render a 3D scene
                     module_viz_scene3dQt   # Enable rendering things in Qt window
                     data                   # Objects declaration
                     module_service         # Service base module
                     module_io_vtk          # Contains the reader and writer for VTK files (image and mesh).
                     module_io_matrix       # Allows reading/writing matrices from/to files
                     module_geometry_base   # Contains basic geometrical functions
    )

    # The ``module_param`` line defines the parameters to set for a module, here it defines the configuration
    # to launch by the module_app module, i.e. the application configuration.
    module_param(
            module_app
        PARAM_LIST
            config
        PARAM_VALUES
            Tuto09MatrixTransformInGS_AppCfg
    )

    # Allow dark theme via module_ui_qt
    module_param(
            module_ui_qt
        PARAM_LIST
            resource
            stylesheet
        PARAM_VALUES
            sight::module::ui::qt/flatdark.rcc
            sight::module::ui::qt/flatdark.qss
    )

    # Generate the profile.xml
    sight_generate_profile(tuto09_matrix_transform_in_gs)

.. _CMake: https://cmake.org

----------
plugin.xml
----------

This file is in the ``rc/`` directory of the application. It defines the services to run.

.. code-block:: xml

    <!--
        This tutorial explains how to perform matrix transformation using the generic scene.

        To use this application, you need to load a mesh.
    -->
        <!--
            This tutorial explains how to perform matrix transformation using the generic scene.

            To use this application, you need to load a mesh.
    -->
    <plugin id="tuto09_matrix_transform_in_gs" >

        <requirement id="sight::module::ui" />
        <requirement id="sight::module::viz::scene3d" />
        <requirement id="sight::module::ui::qt" />
        <requirement id="sight::module::service" />

        <extension implements="sight::app::extension::config" >
            <id>Tuto09MatrixTransformInGS_AppCfg</id>
            <config>

                <!-- ******************************* Objects declaration ****************************** -->

                <object uid="matrixA" type="sight::data::matrix4" >
                    <matrix>
                        <![CDATA[
                            1 0 0 2
                            0 1 0 0
                            0 0 1 0
                            0 0 0 1
                        ]]>
                    </matrix>
                </object>

                <object uid="matrixB" type="sight::data::matrix4" >
                    <matrix>
                        <![CDATA[
                            1 0 0 4
                            0 1 0 0
                            0 0 1 0
                            0 0 0 1
                        ]]>
                    </matrix>
                </object>

                <object uid="matrixC" type="sight::data::matrix4" >
                    <matrix>
                        <![CDATA[
                            1 0 0 0
                            0 1 0 0
                            0 0 1 2
                            0 0 0 1
                        ]]>
                    </matrix>
                </object>

                <object uid="matrixD" type="sight::data::matrix4" >
                    <matrix>
                        <![CDATA[
                            0.75 0        0        0
                            0        0.75 0        0
                            0        0        0.75 0
                            0        0        0    1
                        ]]>
                    </matrix>
                </object>

                <object uid="matrixE" type="sight::data::matrix4" />

                <object uid="rotation1" type="sight::data::matrix4" />

                <object uid="rotation2" type="sight::data::matrix4" />

                <object uid="rotation3" type="sight::data::matrix4" />

                <object uid="mesh" type="sight::data::mesh" />

                <!-- ******************************* UI declaration *********************************** -->

                <service uid="mainFrame" type="sight::module::ui::frame" >
                    <gui>
                        <frame>
                            <name>tuto09_matrix_transform_in_gs</name>
                            <icon>tuto09_matrix_transform_in_gs/tuto.ico</icon>
                            <minSize width="800" height="600" />
                        </frame>
                        <menuBar/>
                    </gui>
                    <registry>
                        <menuBar sid="menuBarView" start="true" />
                        <view sid="mainView" start="true" />
                    </registry>
                </service>

                <service uid="menuBarView" type="sight::module::ui::menu_bar" >
                    <gui>
                        <layout>
                            <menu name="File" />
                        </layout>
                    </gui>
                    <registry>
                        <menu sid="menuFileView" start="true" />
                    </registry>
                </service>

                <service uid="menuFileView" type="sight::module::ui::menu" >
                    <gui>
                        <layout>
                            <menuItem name="Open mesh" shortcut="Ctrl+O" />
                            <separator/>
                            <menuItem name="Quit" shortcut="Ctrl+Q" specialAction="QUIT" />
                        </layout>
                    </gui>
                    <registry>
                        <menuItem sid="openMeshAct" start="true" />
                        <menuItem sid="quitAct" start="true" />
                    </registry>
                </service>

                <service uid="mainView" type="sight::module::ui::view" >
                    <gui>
                        <layout type="sight::ui::layout::line" >
                            <orientation value="vertical" />
                            <view proportion="1" />
                            <view proportion="0" minHeight="30" resizable="false" backgroundColor="#3E4453" />
                        </layout>
                    </gui>
                    <registry>
                        <view sid="genericSceneSrv" start="true" />
                        <view sid="matrixEditorSrv" start="true" />
                    </registry>
                </service>

                <!-- *************************** Begin generic scene *************************** -->

                <!--
                    Generic scene:
                    This scene shows four times the same mesh but with a different matrix. It manages multiples transformation matrices.
                -->
                <service uid="genericSceneSrv" type="sight::viz::scene3d::render" >
                    <scene>
                        <background topColor="#36393E" bottomColor="#36393E" />

                        <layer id="default" order="1" />
                        <adaptor uid="trackballInteractorAdp" />
                        <adaptor uid="mesh0Adp" />
                        <adaptor uid="transform1Adp" />
                        <adaptor uid="mesh1Adp" />
                        <adaptor uid="transform2Adp" />
                        <adaptor uid="mesh2Adp" />
                        <adaptor uid="transform3Adp" />
                        <adaptor uid="mesh3Adp" />
                    </scene>
                </service>

                <service uid="trackballInteractorAdp" type="sight::module::viz::scene3d::adaptor::trackball_camera" >
                    <config layer="default" priority="0" />
                </service>

                <service uid="mesh0Adp" type="sight::module::viz::scene3d::adaptor::mesh" auto_connect="true" >
                    <in key="mesh" uid="mesh" />
                    <config layer="default" />
                </service>

                <service uid="transform1Adp" type="sight::module::viz::scene3d::adaptor::transform" auto_connect="true" >
                    <inout key="transform" uid="rotation1" />
                    <config layer="default" transform="rotationTransform1" />
                </service>

                <service uid="mesh1Adp" type="sight::module::viz::scene3d::adaptor::mesh" auto_connect="true" >
                    <in key="mesh" uid="mesh" />
                    <config layer="default" transform="rotationTransform1" />
                </service>

                <service uid="transform2Adp" type="sight::module::viz::scene3d::adaptor::transform" auto_connect="true" >
                    <inout key="transform" uid="rotation2" />
                    <config layer="default" transform="rotationTransform2" />
                </service>

                <service uid="mesh2Adp" type="sight::module::viz::scene3d::adaptor::mesh" auto_connect="true" >
                    <in key="mesh" uid="mesh" />
                    <config layer="default" transform="rotationTransform2" />
                </service>

                <service uid="transform3Adp" type="sight::module::viz::scene3d::adaptor::transform" auto_connect="true" >
                    <inout key="transform" uid="rotation3" />
                    <config layer="default" transform="rotationTransform3" />
                </service>

                <service uid="mesh3Adp" type="sight::module::viz::scene3d::adaptor::mesh" auto_connect="true" >
                    <in key="mesh" uid="mesh" />
                    <config layer="default" transform="rotationTransform3" />
                </service>

                <!-- ******************************* Actions ****************************************** -->

                <service uid="openMeshAct" type="sight::module::ui::com::starter" >
                    <start uid="meshReaderSrv" />
                </service>

                <service uid="quitAct" type="sight::module::ui::quit" />

                <!-- ******************************* Services ***************************************** -->

                <service uid="matrixEditorSrv" type="sight::module::ui::qt::viz::transform_editor" >
                    <inout key="matrix" uid="matrixE" />
                    <translation enabled="false" />
                    <rotation enabled="y" min="0" max="360" />
                </service>

                <service uid="meshReaderSrv" type="sight::module::ui::io::selector" >
                    <inout key="data" uid="mesh" />
                    <type mode="reader" />
                </service>

                <service uid="rotation1Srv" type="sight::module::geometry::base::concatenate_matrices" >
                    <in group="matrix" >
                        <key uid="matrixE" auto_connect="true" />
                        <key uid="matrixA" />
                        <key uid="matrixD" />
                    </in>
                    <inout key="output" uid="rotation1" />
                </service>

                <service uid="rotation2Srv" type="sight::module::geometry::base::concatenate_matrices" >
                    <in group="matrix" >
                        <key uid="matrixB" />
                        <key uid="matrixE" auto_connect="true" />
                        <key uid="matrixE" />
                        <key uid="matrixE" />
                        <key uid="matrixD" />
                        <key uid="matrixD" />
                    </in>
                    <inout key="output" uid="rotation2" />
                </service>

                <service uid="rotation3Srv" type="sight::module::geometry::base::concatenate_matrices" >
                    <in group="matrix" >
                        <key uid="matrixC" />
                        <key uid="matrixE" auto_connect="true" />
                        <key uid="matrixD" />
                        <key uid="matrixD" />
                        <key uid="matrixD" />
                    </in>
                    <inout key="output" uid="rotation3" />
                </service>

                <!-- ******************************* Start services ***************************************** -->

                <start uid="mainFrame" />
                <start uid="rotation1Srv" />
                <start uid="rotation2Srv" />
                <start uid="rotation3Srv" />
                <start uid="trackballInteractorAdp" />
                <start uid="mesh0Adp" />
                <start uid="transform1Adp" />
                <start uid="mesh1Adp" />
                <start uid="transform2Adp" />
                <start uid="mesh2Adp" />
                <start uid="transform3Adp" />
                <start uid="mesh3Adp" />

                <update uid="rotation1Srv" />
                <update uid="rotation2Srv" />
                <update uid="rotation3Srv" />

            </config>
        </extension>
    </plugin>


===
GUI
===

This tutorial uses new services to manage matrices:

----------------
transform_editor
----------------

This editors regulates the position and rotation defined in a transformation matrix

.. code-block:: xml

    <service uid="matrixEditorSrv" type="sight::module::ui::qt::viz::transform_editor" >
        <inout key="matrix" uid="matrixE" />
        <translation enabled="false" />
        <rotation enabled="y" min="0" max="360" />
    </service>

translation
    Used to updates the translation of the matrix

rotation
    Used to updates the rotation of the matrix

enable (optional, default "true")
    Enables/disables rotation/translation edition. Can be 'yes', 'no' or a combination of [xyz]

min (optional, default "translation=-300, rotation=-180")
    Sets the minimum value for translation/rotation

max (optional, default "translation=300, rotation=180")
    Sets the maximum value for translation/rotation

When the user move the slider, the matrix given in the ``inout`` section is updated.

=============
Generic scene
=============

This tutorial presents new services and objects that allow to apply a transformation matrix to an adaptor.

-------
Matrix4
-------

This data represents a 4*4 3D transformation matrix.

.. code-block:: xml

    <object uid="matrixA" type="sight::data::matrix4" >
        <matrix>
            <![CDATA[
                1 0 0 2
                0 1 0 0
                0 0 1 0
                0 0 0 1
            ]]>
        </matrix>
    </object>

----------
transform
----------

Adaptor binding a Matrix4 to a generic scene.

.. code-block:: xml

    <service uid="transform1Adp" type="sight::module::viz::scene3d::adaptor::transform" auto_connect="true" >
        <inout key="transform" uid="rotation1" />
        <config layer="default" transform="rotationTransform1" />
    </service>

With this service, the `Matrix4` is binded to the corresponding given name (`rotationTransform`).

-----
mesh
-----

This adaptor displays a `Mesh` in a generic scene

.. code-block:: xml

    <service uid="mesh0Adp" type="sight::module::viz::scene3d::adaptor::mesh" auto_connect="true" >
        <inout key="mesh" uid="mesh" />
        <config layer="default" />
    </service>

transform
    Transform visually applied on the mesh, the name must match the name given
    in the transform adaptor (``rotationTransform``).

===
Run
===

To run the application, you must call the following line into the install or build directory:

.. tabs::

   .. group-tab:: Linux

        .. code::

            bin/tuto09matrixtransformings

   .. group-tab:: Windows

        .. code::

            bin/tuto09matrixtransformings.bat
