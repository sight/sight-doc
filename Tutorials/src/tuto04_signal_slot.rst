.. _TutorialsTuto04SignalSlot:

*************************************************
[*tuto04_signal_slot*] Signal-slot communication
*************************************************

The fourth tutorial explains the communication mechanism with signals and slots.

.. figure:: ../media/tuto04SignalSlot.png
    :scale: 25
    :align: center
    :alt: tuto04SignalSlot

=============
Prerequisites
=============

Before reading this tutorial, you should have seen :
* :ref:`TutorialsTuto03DataService`
* :ref:`SADSigSlot`

=========
Structure
=========

--------------
CMakeLists.txt
--------------

This file describes the project information and requirements :

.. code-block:: cmake

    # Create the correct CMake target for an application
    sight_add_target(tuto04_signal_slot TYPE APP)

    # Dependencies need to be built before, but not linked with
    add_dependencies(tuto04_signal_slot
                     sightrun           # Needed to build the launcher
                     module_app      # XML configurations
                     module_ui_qt       # Load qt implementation of gui
                     data               # Objects declaration
                     module_service     # Service base module
                     module_ui_base     # UI declaration/Actions
                     module_io_vtk      # Contains the reader and writer for VTK files (image and mesh).
                     module_viz_sample  # Loads basic rendering services for images and meshes.
    )

    # The ``module_param`` line defines the parameters to set for a module, here it defines the configuration
    # to launch by the module_app module, i.e. the application configuration.
    module_param(
            module_app
        PARAM_LIST
            config
        PARAM_VALUES
            Tuto04SignalSlot_AppCfg
    )

    # Allow dark theme via module_ui_qt
    module_param(
            module_ui_qt
        PARAM_LIST
            resource
            stylesheet
        PARAM_VALUES
            sight::module::ui::qt/flatdark.rcc
            sight::module::ui::qt/flatdark.qss
    )

    # Generate the profile.xml
    sight_generate_profile(tuto04_signal_slot)

.. _CMake: https://cmake.org

----------
plugin.xml
----------

This file is in the ``rc/`` directory of the application. It defines the services to run.

.. code-block:: xml

    <plugin id="tuto04_signal_slot" >

        <requirement id="sight::module::service" />
        <requirement id="sight::module::ui::qt" />

        <extension implements="sight::app::extension::config" >
            <id>Tuto04SignalSlot_AppCfg</id>
            <config>

                <!-- ******************************* Objects declaration ****************************** -->

                <!-- The main data object is sight::data::mesh. -->
                <object uid="mesh" type="sight::data::mesh" />

                <!-- ******************************* UI declaration *********************************** -->

                <service uid="mainFrame" type="sight::module::ui::frame" >
                    <gui>
                        <frame>
                            <name>tuto04_signal_slot</name>
                            <icon>tuto04_signal_slot/tuto.ico</icon>
                            <minSize width="1280" height="720" />
                        </frame>
                        <menuBar />
                    </gui>
                    <registry>
                        <menuBar sid="menuBarView" start="true" />
                        <view sid="containerView" start="true" />
                    </registry>
                </service>

                <service uid="menuBarView" type="sight::module::ui::menu_bar" >
                    <gui>
                        <layout>
                            <menu name="File" />
                        </layout>
                    </gui>
                    <registry>
                        <menu sid="menuFileView" start="true" />
                    </registry>
                </service>

                <service uid="menuFileView" type="sight::module::ui::menu" >
                    <gui>
                        <layout>
                            <menuItem name="Open mesh" shortcut="Ctrl+O" />
                            <separator />
                            <menuItem name="Quit" specialAction="QUIT" shortcut="Ctrl+Q" />
                        </layout>
                    </gui>
                    <registry>
                        <menuItem sid="openMeshAct" start="true" />
                        <menuItem sid="quitAct" start="true" />
                    </registry>
                </service>

                <!--
                    Default view service:
                    This service defines the view layout. The type 'sight::ui::layout::cardinal' represents a main
                    central view and other views at the 'right', 'left', 'bottom' or 'top'.
                    Here the application contains a central view at the right.

                    Each <view> declared into the <layout> tag, must have its associated <view> into the <registry> tag.
                    A minimum window height and a width are given to the two non-central views.
                -->
                <service uid="containerView" type="sight::module::ui::view" >
                    <gui>
                        <layout type="sight::ui::layout::cardinal" >
                            <view align="center" />
                            <view caption="Move cameras 1,2" align="right" minWidth="600" minHeight="100" />
                            <view caption="Move camera 3" align="right" minWidth="600" minHeight="100" />
                        </layout>
                    </gui>
                    <registry>
                        <view sid="rendering1Srv" start="true" />
                        <view sid="rendering2Srv" start="true" />
                        <view sid="rendering3Srv" start="true" />
                    </registry>
                </service>

                <!-- ******************************* Actions ****************************************** -->

                <service uid="openMeshAct" type="sight::module::ui::com::starter" >
                    <start uid="meshReaderSrv" />
                </service>

                <service uid="quitAct" type="sight::module::ui::quit" />

                <!-- ******************************* Services ***************************************** -->

                <service uid="meshReaderSrv" type="sight::module::ui::io::selector" >
                    <inout key="data" uid="mesh" />
                    <type mode="reader" /><!-- mode is optional (by default it is "reader") -->
                </service>

                <!--
                    Visualization services:
                    We have three rendering service representing a 3D scene displaying the loaded mesh. The scene are
                    shown in the windows defines in 'view' service.
                -->
                <service uid="rendering1Srv" type="sight::module::viz::sample::mesh" >
                    <in key="mesh" uid="mesh" auto_connect="true" />
                </service>

                <service uid="rendering2Srv" type="sight::module::viz::sample::mesh" >
                    <in key="mesh" uid="mesh" auto_connect="true" />
                </service>

                <service uid="rendering3Srv" type="sight::module::viz::sample::mesh" >
                    <in key="mesh" uid="mesh" auto_connect="true" />
                </service>

                <!-- ******************************* Connections ***************************************** -->

                <!--
                    Each 3D scene owns a 3D camera that can be moved by clicking in the scene.
                    - When the camera move, a signal 'camUpdated' is emitted with the new camera information (position,
                    focal, view up).
                    - To update the camera without clicking, you could call the slot 'updateCamPosition'

                    Here, we connect some rendering services signal 'camUpdated' to the others service slot
                    'updateCamPosition', so the cameras are synchronized between scenes.
                -->
                <connect>
                    <signal>rendering1Srv/camUpdated</signal>
                    <slot>rendering2Srv/updateCamPosition</slot>
                    <slot>rendering3Srv/updateCamPosition</slot>
                </connect>

                <connect>
                    <signal>rendering2Srv/camUpdated</signal>
                    <slot>rendering1Srv/updateCamPosition</slot>
                </connect>

                <!-- ******************************* Start services ***************************************** -->

                <start uid="mainFrame" />

            </config>
        </extension>

    </plugin>


You can also group the signals and all the slots together.

.. code-block:: xml

    <connect>
        <signal>rendering1Srv/camUpdated</signal>
        <signal>rendering2Srv/camUpdated</signal>
        <signal>rendering3Srv/camUpdated</signal>

        <slot>rendering1Srv/updateCamPosition</slot>
        <slot>rendering2Srv/updateCamPosition</slot>
        <slot>rendering3Srv/updateCamPosition</slot>
    </connect>

.. tip::
    You can remove a connection to see that a camera in the scene is no longer synchronized.

=========================
Signal and slot creation
=========================

---------------
*mesh.hpp*
---------------

.. code-block:: cpp

    // {...}

    class mesh : public sight::ui::service
    {

    public:

        // {...}
        static const core::com::slots::key_t UPDATE_CAM_POSITION_SLOT;

        static const core::com::signals::key_t CAM_UPDATED_SIG;

        using cam_updated_signal_t = core::com::signal<void (data::matrix4::sptr)>;

        // {...}

    private:

        /// SLOT: receives new camera transform and update the camera.
        void update_cam_position(data::matrix4::sptr _transform);

        /// SLOT: receives new camera transform from the camera service and trigger the signal.
        void update_cam_transform();

        // {...}

        /// Contains the signal emitted when camera position is updated.
        cam_updated_signal_t::sptr m_sig_cam_updated;

        // {...}

    };

---------------
*mesh.cpp*
---------------

.. code-block:: cpp

    const core::com::slots::key_t mesh::UPDATE_CAM_POSITION_SLOT   = "update_cam_position";
    static const core::com::slots::key_t UPDATE_CAM_TRANSFORM_SLOT = "update_cam_transform";

    const core::com::signals::key_t mesh::CAM_UPDATED_SIG = "cam_updated";

    // {...}

    //------------------------------------------------------------------------------

    mesh::mesh() noexcept
    {
        new_slot(UPDATE_CAM_POSITION_SLOT, &mesh::update_cam_position, this);
        new_slot(UPDATE_CAM_TRANSFORM_SLOT, &mesh::update_cam_transform, this);

        m_sig_cam_updated = new_signal<cam_updated_signal_t>(CAM_UPDATED_SIG);
    }

    // {...}

    //------------------------------------------------------------------------------

    void mesh::update_cam_position(data::matrix4::sptr _transform)
    {
        m_camera_transform->shallow_copy(_transform);
        m_camera_srv->update().wait();
    }

    //------------------------------------------------------------------------------

    void mesh::update_cam_transform()
    {
        {
            core::com::connection::blocker block(m_sig_cam_updated->get_connection(this->slot(UPDATE_CAM_TRANSFORM_SLOT)));
            m_sig_cam_updated->async_emit(m_camera_transform);
        }
    }

===
Run
===

To run the application, you must call the following line into the install or build directory:

.. tabs::

   .. group-tab:: Linux

        .. code::

            bin/tuto04signalslot

   .. group-tab:: Windows

        .. code::

            bin/tuto04signalslot.bat
