.. _TutorialsTuto06Filter:

********************************************
[*tuto06_filter*] Apply a filter on an image
********************************************

This tutorial explains how to apply a filter on an image. Here, the filter applied on the image is a threshold.
The tutorial also implements a flip filter, that won't be described in this tutorial.
The code for this second filter is available in the repository.

.. figure:: ../media/tuto06Filter.png
    :scale: 25
    :align: center
    :alt: tuto06Filter

=============
Prerequisites
=============

Before reading this tutorial, you should have seen:

* :ref:`TutorialsTuto05Mesher`
* :ref:`SADBufferObjects`

=========
Structure
=========

--------------
CMakeLists.txt
--------------

This file describes the project information and requirements :

.. code-block:: cmake

    # Create the correct CMake target for an application
    sight_add_target(tuto06_filter TYPE APP)

    # Dependencies need to be built before, but not linked with
    add_dependencies(tuto06_filter
                     sightrun               # Needed to build the launcher
                     module_app          # XML configurations
                     module_ui_qt           # Load qt implementation of gui
                     data                   # Objects declaration
                     module_service         # Service base module
                     module_ui_base         # UI declaration/Actions
                     module_io_vtk          # Contains the reader and writer for VTK files (image and mesh).
                     module_viz_sample      # Loads basic rendering services for images and meshes.
                     module_filter_image    # Contains the threshold functionality
    )

    # The ``module_param`` line defines the parameters to set for a module, here it defines the configuration
    # to launch by the module_app module, i.e. the application configuration.
    module_param(
            module_app
        PARAM_LIST
            config
        PARAM_VALUES
            Tuto06Filter_AppCfg
    )

    # Allow dark theme via module_ui_qt
    module_param(
            module_ui_qt
        PARAM_LIST
            resource
            stylesheet
        PARAM_VALUES
            sight::module::ui::qt/flatdark.rcc
            sight::module::ui::qt/flatdark.qss
    )

    # Generate the profile.xml
    sight_generate_profile(tuto06_filter)

.. _CMake: https://cmake.org

----------
plugin.xml
----------

This file is in the ``rc/`` directory of the application. It defines the services to run.

.. code-block:: xml

        <plugin id="tuto06_filter" >

        <requirement id="sight::module::service" />
        <requirement id="sight::module::ui::qt" />

        <extension implements="sight::app::extension::config" >
            <id>Tuto06Filter_AppCfg</id>
            <config>

                <!-- ******************************* Objects declaration ****************************** -->

                <!-- This is the source image for the filtering. -->
                <object uid="myImage1" type="sight::data::image" />
                <!-- This is the output image for the filtering. "deferred" defines that the image is not created at the
                    beginning, but will be created by a service. -->
                <object uid="myImage2" type="sight::data::image" src="deferred" />

                <!-- ******************************* UI declaration *********************************** -->

                <!-- Windows & Main Menu -->
                <service uid="mainFrame" type="sight::module::ui::frame" >
                    <gui>
                        <frame>
                            <name>tuto06_filter</name>
                            <icon>tuto06_filter/tuto.ico</icon>
                            <minSize width="1280" height="720" />
                        </frame>
                        <menuBar />
                    </gui>
                    <registry>
                        <menuBar sid="menuBarView" start="true" />
                        <view sid="containerView" start="true" />
                    </registry>
                </service>

                <service uid="menuBarView" type="sight::module::ui::menu_bar" >
                    <gui>
                        <layout>
                            <menu name="File" />
                            <menu name="Filter" />
                        </layout>
                    </gui>
                    <registry>
                        <menu sid="menuFileView" start="true" />
                        <menu sid="menuFilterView" start="true" />
                    </registry>
                </service>

                <!-- Menus -->
                <service uid="menuFileView" type="sight::module::ui::menu" >
                    <gui>
                        <layout>
                            <menuItem name="Open image" shortcut="Ctrl+O" />
                            <separator />
                            <menuItem name="Quit" specialAction="QUIT" shortcut="Ctrl+Q" />
                        </layout>
                    </gui>
                    <registry>
                        <menuItem sid="openImageAct" start="true" />
                        <menuItem sid="quitAct" start="true" />
                    </registry>
                </service>

                <service uid="menuFilterView" type="sight::module::ui::menu" >
                    <gui>
                        <layout>
                            <menuItem name="Compute image filter" />
                            <menuItem name="Toggle vertical image flipping" />
                        </layout>
                    </gui>
                    <registry>
                        <menuItem sid="imageFilterAct" start="true" />
                        <menuItem sid="imageFlipperAct" start="true" />
                    </registry>
                </service>

                <service uid="containerView" type="sight::module::ui::view" >
                    <gui>
                        <layout type="sight::ui::layout::line" >
                            <orientation value="horizontal" />
                            <view proportion="1" />
                            <view proportion="1" />
                        </layout>
                    </gui>
                    <registry>
                        <view sid="image1Srv" start="true" />
                        <!-- As the output image is deferred, the service cannot be started at the beginning. -->
                        <view sid="image2Srv" start="false" />
                    </registry>
                </service>

                <!-- ******************************* Actions ****************************************** -->

                <!-- Action to quit the application -->
                <service uid="quitAct" type="sight::module::ui::quit" />

                <!-- Action to open image file: call update on image reader service -->
                <service uid="openImageAct" type="sight::module::ui::action" />

                <!-- Action apply threshold filter: call update on threshold filter service -->
                <service uid="imageFilterAct" type="sight::module::ui::action" />

                <!-- Action apply flip filter: call 'flipAxisY' slot on flip service -->
                <service uid="imageFlipperAct" type="sight::module::ui::action" />

                <!-- ******************************* Services ***************************************** -->

                <!-- Reader of the input image -->
                <service uid="imageReaderSrv" type="sight::module::ui::io::selector" >
                    <inout key="data" uid="myImage1" />
                    <type mode="reader" />
                </service>

                <!--
                    Threshold filter:
                    Applies a threshold filter. The source image is 'myImage1' and the
                    output image is 'myImage2'.
                    The two images are declared above.
                -->
                <service uid="imageFilterSrv" type="sight::module::filter::image::threshold" >
                    <in key="source" uid="myImage1" />
                    <out key="target" uid="myImage2" />
                    <config>
                        <threshold>50</threshold>
                    </config>
                </service>

                <!--
                    Flip filter:
                    Applies a flip filter. The source image is 'myImage1' and the
                    output image is 'myImage2'.
                    The two images are declared above.
                -->
                <service uid="imageFlipperSrv" type="sight::module::filter::image::flip" >
                    <in key="source" uid="myImage1" />
                    <out key="target" uid="myImage2" />
                </service>

                <!--
                    Renderer of the 1st Image:
                    This is the source image for the filtering.
                -->
                <service uid="image1Srv" type="sight::module::viz::sample::image" >
                    <in key="image" uid="myImage1" auto_connect="true" />
                </service>

                <!--
                    Rendered for the 2nd Image:
                    This is the output image for the filtering.
                -->
                <service uid="image2Srv" type="sight::module::viz::sample::image" >
                    <in key="image" uid="myImage2" auto_connect="true" />
                </service>

                <!-- ******************************* Start services ***************************************** -->

                <connect>
                    <signal>openImageAct/updated</signal>
                    <slot>imageReaderSrv/update</slot>
                </connect>

                <connect>
                    <signal>imageFilterAct/updated</signal>
                    <slot>imageFilterSrv/update</slot>
                </connect>

                <connect>
                    <signal>imageFlipperAct/updated</signal>
                    <slot>imageFlipperSrv/flipAxisY</slot>
                </connect>

                <start uid="mainFrame" />
                <start uid="imageReaderSrv" />
                <start uid="imageFilterSrv" />
                <start uid="imageFlipperSrv" />

                <!-- start the service using a deferred image -->
                <start uid="image2Srv" />

            </config>
        </extension>
    </plugin>



---------------
Filter service
---------------

Here, the filter service is inherited from a ``sight::service::operator``

This  ``updating()`` function retrieves the two images and applies the threshold algorithm.

The ``sight::data::image`` contains a buffer for pixel values, it is stored as a ``void *`` to allows several types of
pixels (uint8, int8, uint16, int16, double, float ...). To use the image buffer, we need to cast it to the image pixel
type. For that, we use the ``sight::core::tools::Dispatcher`` class which it allows to invoke a template functor according to the
image type. This is particularly useful when using template based libraries like `ITK <https://itk.org/>`_.

The image type is defined by the ``sight::core::tools::Type``, this class allows to serialize the image type as a string and to
retrieve the type information as sizeof, signed or not, ...
The Dispatcher allows to associate each ``Type`` to a real type as std::uint8_t, std::int8_t, std::uint16_t,... float,
double.

.. code-block:: cpp

    void threshold::updating()
    {
        ThresholdFilter::Parameter param; // filter parameters: threshold value, image source, image target

        // retrieve the input object
        auto input = m_source.lock();

        // try to dynamic cast to an Image and an ImageSeries to know which type of data we use
        data::Imageseries::csptr imageSeriesSrc = data::image_series::dynamicConstCast(input.get_shared());
        data::image::csptr imageSrc             = data::image::dynamicConstCast(input.get_shared());
        data::object::sptr output;

        // Get source/target image
        if(imageSeriesSrc)
        {
            param.imageIn = imageSeriesSrc->getImage();
            data::Imageseries::sptr imageSeriesDest = data::image_series::copy(imageSeriesSrc);
            // define the input image series as the reference
            imageSeriesDest->setDicomReference(imageSeriesSrc->getDicomReference());

            // create the output image
            data::image::sptr imageOut = std::make_shared<sight::data::image>();
            imageSeriesDest->setImage(imageOut);
            param.imageOut = imageOut;
            output         = imageSeriesDest;
        }
        else if(imageSrc)
        {
            param.imageIn = imageSrc;
            // create the output image
            data::image::sptr imageOut = std::make_shared<sight::data::image>();
            param.imageOut = imageOut;
            output         = imageOut;
        }
        else
        {
            SIGHT_THROW("Wrong type: source type must be an ImageSeries or an Image");
        }

        param.thresholdValue = m_threshold;

        // get image type
        core::tools::Type type = param.imageIn->getType();

        /* The dispatcher allows to apply the filter on any type of image.
        * It invokes the template functor ThresholdFilter using the image type.
        * - template parameters:
        *   - core::tools::SupportedDispatcherTypes defined all the supported type of the functor, here all the type
        *     supported by core::tools::Type(std::int8_t, std::uint8_t, std::int16_t, std::uint16_t, std::int32_t,
        *     std::uint32_t, std::int64_t, std::uint64_t, float, double)
        *   - ThresholdFilter: functor struct or class
        * - parameters:
        *   - type: core::tools::Type of the image
        *   - param: struct containing the functor parameters (here the input and output images and the threshold value)
        */
        core::tools::Dispatcher<core::tools::SupportedDispatcherTypes, ThresholdFilter>::invoke(type, param);

        // register the output image to be accesible by the other service from the XML configuration
        this->set_output(s_IMAGE_OUT, output);
    }


The functor is a *structure* containing a *sub-structure* for the parameters (inputs and outputs) and a template
method ``operator(parameters)``.

.. code-block:: cpp

    /**
    * Functor to apply a threshold filter.
    *
    * The pixel with the value less than the threshold value will be set to 0, else the value is set to the maximum
    * value of the image pixel type.
    *
    * The functor provides a template method operator(param) to apply the filter
    */
    struct ThresholdFilter
    {
        struct Parameter
        {
            double thresholdValue;      ///< threshold value.
            data::image::csptr imageIn; ///< image source
            data::image::sptr imageOut; ///< image target: contains the result of the filter
        };

        /**
        * @brief Applies the filter
        * @tparam PIXELTYPE image pixel type (uint8, uint16, int8, int16, float, double, ....)
        */
        template<class PIXELTYPE>
        void operator()(Parameter& param)
        {
            const PIXELTYPE thresholdValue = static_cast<PIXELTYPE>(param.thresholdValue);
            data::image::csptr imageIn     = param.imageIn;
            data::image::sptr imageOut     = param.imageOut;
            SIGHT_ASSERT("Sorry, image must be 3D", imageIn->getNumberOfDimensions() == 3);

            imageOut->copyInformation(imageIn); // Copy image size, type... without copying the buffer
            imageOut->resize();                 // Allocate the image buffer

            // Get iterators on image buffers
            auto it1          = imageIn->begin<PIXELTYPE>();
            const auto it1End = imageIn->end<PIXELTYPE>();
            auto it2          = imageOut->begin<PIXELTYPE>();
            const auto it2End = imageOut->end<PIXELTYPE>();

            const PIXELTYPE maxValue = std::numeric_limits<PIXELTYPE>::max();

            // Fill the target buffer considering the threshold
            for( ; it1 != it1End && it2 != it2End ; ++it1, ++it2)
            {
                *it2 = (*it1 < thresholdValue) ? 0 : maxValue;
            }
        }
    };

.. note::

    The flip service uses the same principle as the threshold service. The code makes further use of templates to
    enable the filter to work 1, 2 and 3 dimension images. Furthermore, the main code is implemented in the
    imageFilterOp library, which is then called from the flip service.

===
Run
===

To run the application, launch the following command in the install or build directory:

.. tabs::

   .. group-tab:: Linux

        .. code::

            bin/tuto06filter

   .. group-tab:: Windows

        .. code::

            bin/tuto06filter.bat
