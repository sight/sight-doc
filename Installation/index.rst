.. _Installation:

Installation
============

Introduction
------------

The installation should be straightforward as almost all requirements are provided by distribution packages on Linux and VCPKG on Microsoft Windows.

Prerequisites
-------------

.. tabs::

   .. group-tab:: Linux

        Until now, Ubuntu 22.04 and 24.04 are supported, other distributions may work but have not been tested.

   .. group-tab:: Windows

        Only `Microsoft Windows 11` with `Visual Studio 2022` are supported, other versions may work but have not been
        tested.


If not already installed (please, note the recommended version):

#. Install `git [>= 2.27.0] <https://git-scm.com/>`_
#. Install `Python [>= 3.8.6] <https://www.python.org/downloads/>`_
#. Install `CMake [>= 3.19.6] <http://www.cmake.org/download/>`_
#. Install `Ninja [>= 1.10.0] <https://github.com/ninja-build/ninja/releases>`_
#. Install a C++ compiler and other development libraries.

.. tabs::

   .. group-tab:: Linux

        These install steps depend on the linux distribution you use. Here, instructions are given for Ubuntu.

        First Git, Python, Ninja and C++ compiler can be installed with the following commands:

        .. code:: bash

            $ sudo apt install build-essential ninja-build git git-lfs

        For an up-to-date CMake:

        .. code:: bash

            $ sudo snap install cmake --classic

        And dependency development libraries:

        .. code:: bash

            $ sudo apt install dcmtk libarchive-dev libboost-all-dev libceres-dev libcppunit-dev \
            libexpat1-dev libfftw3-dev libgdcm-dev libglm-dev libinsighttoolkit5-dev \
            libogre-1.12-dev libopencv-dev libopenigtlink-dev libqt5opengl5-dev \
            libqt5x11extras5-dev libvtk9-dev libvtkgdcm-dev libvtk9-qt-dev \
            qt3d5-dev qtmultimedia5-dev qtquickcontrols2-5-dev qttools5-dev zlib1g-dev zstd \
            libpng-dev libtiff5-dev libhdf5-dev libann-dev

        For Ubuntu 24.04, you will also need:

        .. code:: bash

            $ sudo apt install libnsl-dev

        The last step is optional, but required to build integrated documentation:

        .. code:: bash

            $ sudo apt install doxygen graphviz gnuplot

   .. group-tab:: Windows

    These installation step has been fully tested for windows version 10 only.

    You first need to install `Visual Studio 2019 Community <https://visualstudio.microsoft.com/>`_ adapted to your computer.

    On the contrary to the Ubuntu installation of the sight framework, which relies only on standard package provided through the standard distribution, some additional packages are required for the windows installation.
    To provide these packages, we make use of a custom [Vcpkg](https://github.com/IRCAD/vcpkg) setup.

    Run the following command in your build folder to download the binary packages:

        .. code:: bash

            cmake -P <SOURCE_PATH>\cmake\build\download_deps.cmake

    This will create a folder in the parent directory named sight-vcpkg-<sha1>, where <sha1> is the commit used to create the export from the sight-vcpkg repository.



Source tree layout
~~~~~~~~~~~~~~~~~~~

Good practices in Sight recommend to separate source files, build and install folders.
So to prepare the development environment:

* Create a development folder (Dev)

* Create a build folder (Dev/Build).
    .. tip:: The build folder is where sources will be compiled.

    * Add a sub folder for Debug and Release.

* Create a source folder (Dev/Src)
    .. tip:: The source folder is where sources will be cloned.

* Create an install folder (Dev/Install).
    .. tip:: The install folder is where the binaries will be installed, independently from sources or intermediate object files.

    * Add a sub folder for Debug and Release.

|directories|

Of course you can name the folders as you wish, or choose a different layout, but keep in mind to not build inside the
source directory. This is strongly discouraged by *CMake* authors.

.. |directories| image:: media/DirectoriesNoDeps.png


.. _settingUpEnv:

Setting up your environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. tabs::

   .. group-tab:: Linux

        Make sure all of your Prerequisites_ are loaded into your path correctly, for all installation made through
        `apt` or `snap` this is done automatically. If you use custom versions, be sure to add them manually with a command similar to :

        .. code:: bash

            > export PATH=<PATH_TO_ADD>:${PATH}

        .. tip::

            Writing a ``.sh`` script that loads all these previous locations to your path can save you time and effort!

   .. group-tab:: Windows

        Load into your active PATH environment variable the needed locations in-order to be able to build.

        * Add Visual studio compilers.

        You can use the 'VS2019 x64 Native Tools Command Prompt'  or launch the `vcvarsall.bat` script with the parameter
        `amd64` on your current console.
        The location of that script will look something like this
        ``C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat``

        * Add the Prerequisites_

        If installed with default parameters ``git``, ``CMake`` and ``Python`` will be automatically loaded into your PATH
        variable.

        For static binaries like ``Ninja`` you will need to add them manually with a command similar to :

        .. code:: bash

            > PATH=C:\Bin;%PATH%

        .. tip::

            Writing a ``.cmd`` script that loads all these previous locations to your path can save you time and effort!



Building your sources
----------------------

* `Clone <http://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository#Cloning-an-Existing-Repository>`_  sight `repository <https://git.ircad.fr/Sight/sight.git>`_ in the (Dev/Src) source folder:

.. code:: bash

    $ cd Dev/Src
    $ git clone https://git.ircad.fr/Sight/sight.git

* Go into your Build directory (Debug or Release). Here is an example if you want to compile in debug:

.. code:: bash

    $ cd Dev/Build/Debug

.. warning:: Make sure your environment is properly set : :ref:`settingUpEnv` .

* Call cmake-gui.

.. code:: bash

    $ cmake-gui .

Configuration
~~~~~~~~~~~~~~~~


.. tabs::

    .. group-tab:: Linux

      * Set the desired Build directory (e.g. Dev/Build/Debug or Release)

      * Set the desired Source directory (e.g. Dev/Src/sight)

      * Click on "configure".

      * During configure step, choose the generator 'Ninja' to compile Sight sources.

      .. tip:: An error will be raised, which is perfectly normal if you don't have set ``CMAKE_BUILD_TYPE``
          to either ``Debug``, ``Release`` or ``RelWithDebInfo``. You can set it now and click again on "configure".

      .. tip:: If you compile an other project than sight, you will need to set the  ``sight_DIR``
          to the following path ``<SIGHT_INSTALL_PATH>/lib/cmake/sight``.

    .. group-tab:: Windows

      * Set the desired Build directory (e.g. Dev/Build/Debug or Release)

      * Set the desired Source directory (e.g. Dev/Src/sight)

      * Click on 'Add Entry' and add the entry ``CMAKE_TOOLCHAIN_FILE`` with the value ``..\sight-vcpkg-<sha1>\scripts\buildsystems\vcpkg.cmake``

      .. warning::
          This must be done on the very first configuration, otherwise, CMake will fail to find the Vcpkg binary packages.
          If you miss this step, please erase your build and start again.

      * Click on "configure".

      * During the configure step, choose the generator 'Ninja' to compile Sight sources.

      .. tip:: An error will be raised, which is perfectly normal if you don't have set ``CMAKE_BUILD_TYPE``
          to either ``Debug``, ``Release`` or ``RelWithDebInfo``. You can set it now and click again on "configure".

      .. tip:: If you compile an other project than sight, you will need to set the  ``sight_DIR``
          to the following path [path to sight install dir]/lib/cmake/sight.


Generation
~~~~~~~~~~~~~~

.. tabs::

    .. group-tab:: Linux

        Set the following arguments:

        ``CMAKE_BUILD_TYPE``:
            Set to Debug or Release or RelWithDebInfo.
        ``CMAKE_INSTALL_PREFIX``:
            Set the install location (e.g. ``Dev/Install/Debug``).

            .. tip:: If you are generating an a sight-dependant project that you expect to integrate as a dependance in a third project,
                it is recommended to set the same ``CMAKE_INSTALL_PREFIX`` as for sight.

        ``sight_DIR``:
            If you generate a _Sight_ dependent project, set to ``[path to sight install dir]/lib/cmake/sight``.

    .. group-tab:: Windows

        Set the following arguments:

        ``CMAKE_BUILD_TYPE``:
            Set to Debug or Release or RelWithDebInfo.
        ``CMAKE_TOOLCHAIN_FILE``:
            Set to ``..\sight-vcpkg-<sha1>\scripts\buildsystems\vcpkg.cmake``
        ``CMAKE_INSTALL_PREFIX``:
            Set the install location (e.g. ``Dev\Install\Debug``).

            .. tip:: If you are generating an a sight-dependant project that you expect to integrate as a dependance in a third project,
                it is recommanded to set the same ``CMAKE_INSTALL_PREFIX`` as for sight.

        ``sight_DIR``:
            If you generate a sight dependent project, set to ``[path to sight install dir]\lib\cmake\sight``.

* Click on "generate".

If you want to launch the ``cmake`` through the command line with the appropriate parameters:

.. tabs::

    .. group-tab:: Linux

        .. code:: bash

            $ cd Dev/Build/Debug
            $ cmake ../../Src/sight -G "Ninja" -DCMAKE_INSTALL_PREFIX=../../Install/Debug -DCMAKE_BUILD_TYPE=Debug -Dsight_DIR=[sight install dir]/lib/cmake/sight

    .. group-tab:: Windows

        .. code:: bash

            $ cd Dev\Build\Debug
            $ cmake ..\..\Src\sight -G "Ninja" -DCMAKE_TOOLCHAIN_FILE ..\sight-vcpkg-<sha1>\scripts\buildsystems\vcpkg.cmake -DCMAKE_INSTALL_PREFIX=..\..\Install\Debug -DCMAKE_BUILD_TYPE=Debug -Dsight_DIR=[sight install dir]\lib\cmake\sight

Build
~~~~~~~

Compile Sight source code using ninja in the console:

- Go to the build directory (e.g. ``Dev/Build/Debug`` or ``Dev/Build/Release``)
- Use ``ninja`` if you want to compile everything.
- Use ``ninja name_of_application`` to compile only one application.

.. code:: bash

    $ cd Dev/Build/Debug
    $ ninja

Launch an application
---------------------

After a successful compilation any previously built application can be launched with the appropriate script from Sight.

.. tabs::

   .. group-tab:: Linux

        You will find in the ``Build/bin`` directory an automatically generated script with the same name (on lowercase)
        as the application you built.

        .. tip:: If you are running a _Sight_ application which depends on a secondary project which depends on sight, you should add ``-B [secondary  project install dir]/share/[secondary project]/``

        .. tip:: If you have issues with some file dialogs which don't open properly, you should add XDG_CURRENT_DESKTOP=qt before your application run.

        .. code:: bash

            $ cd Dev/Build/Debug
            $ XDG_CURRENT_DESKTOP=qt ./bin/myapplication  -B [secondary  project install dir]/share/[secondary project]/


   .. group-tab:: Windows

        You will find in the ``Build\bin`` directory an automatically generated ``.bat`` with the same name (on
        lowercase) as the application you built.


        .. tip:: If you are running a _Sight_ application which depends on a secondary project which depends on sight, you should add ``-B [secondary  project install dir]/share/[secondary project]/``

        .. code:: bash

            $ cd Dev\Build\Debug
            $ .\bin\myapplication.bat -B [secondary  project install dir]\share\[secondary project]\



.. important::
    This automatically generated script loads all the needed packages locations and adds them temporarily to your
    PATH variable. Feel free to take a look inside.

Generate an installer
---------------------

Follow these two steps:

* Run *ninja install application_to_install* in the Build directory
* Run *ninja package* in the Build directory

The installer will be generated in the Build directory.


Recommended software
--------------------

The following programs may be helpful for your developments:

.. tabs::

   .. group-tab:: Linux

        * `Qt Creator <https://download.qt.io/official_releases/qtcreator/>`_:
            Qt Creator is a multi-OS Integrated Development Environment (IDE) for computer programming.
        * `Visual Studio Code <https://code.visualstudio.com//>`_:
            Code editing redefined. Free. Built on open source. Runs everywhere.
            Install also Microsoft C++ extension pack `ms-vscode.cpptools-extension-pack`.

   .. group-tab:: Windows

        * `Qt Creator <https://download.qt.io/official_releases/qtcreator/>`_:
            Qt Creator is a multi-OS Integrated Development Environment (IDE) for computer programming.
            You can find a setup tutorial here :ref:`HowTosQtCreatorSetup`.
        * `Visual Studio Code <https://code.visualstudio.com//>`_:
            Code editing redefined. Free. Built on open source. Runs everywhere.
            Install also Microsoft C++ extension pack `ms-vscode.cpptools-extension-pack`.
        * `ConsoleZ <https://github.com/cbucher/console/wiki/Downloads>`_:
            ConsoleZ is an alternative command prompt for Windows, adding more capabilities to the default Windows command
            prompt. To compile Sight with the console the windows command prompt has to be set in the tab settings.


Known issues
-------------------------------

We use some fine tuned compiler flags (like `/arch:AVX2`) to optimize and generate code specifically for CPUs that
were released around 2013 and later. It means, if your CPU is too old, **Sight** will crash at runtime with
an error like `Illegal instruction` because some CPU instructions are not implemented. In such situation, you can
modify hidden cmake variable `SIGHT_ARCH` at configuring time or modify the default compiler flag directly in **Sight**
CMake code.

Need some help? Keep in touch!
-------------------------------

As any active community, we *sighters* are happy to help each other or beginners however we can. Feel free to join us
and share with us your questions or comments at our `Gitter <https://gitter.im/IRCAD/sight-support>`_ .
We provide support in French or English.
