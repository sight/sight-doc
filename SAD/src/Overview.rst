Overview
========

Sight (Surgical Image Guidance and Health-care Toolkit) is an open-source
library, developed by IRCAD (research institute against cancer and disease).
The purpose of Sight is to ease the creation of applications in the medical imaging field.
Therefore it provides features like digital image processing in 2D and 3D, visualization or
simulation of medical interactions.

Sight is based on a component architecture composed of C++ libraries.
The three main concepts of the architecture, explained in the following sections, are:

-  object-service concept
-  component approach
-  signal-slot communication

The toolkit supports Windows and Linux. Building an application with Sight only requires to write one or several XML
files describing the application. Its functionalities can be also extended by writing new components in C++,
which is the coding language of the framework.

This document introduces the general architecture of Sight.
