.. _SADMultithreading:

Multithreading
=======================

Overview
--------

The multithreading paradigm has become more popular as efforts to further
exploit instruction level parallelism have stalled since the late 1990s. This
allowed the concept of throughput computing to re-emerge to prominence from the
more specialized field of transaction processing:

- Even though it is very difficult to further speed up a single thread or
  single program, most computer systems are actually multi-tasking among
  multiple threads or programs.
- Techniques that would allow speedup of the overall system throughput of all
  tasks would be a meaningful performance gain.

Some advantages include:

- If a thread gets a lot of cache misses, the other thread(s) can continue,
  taking advantage of the unused computing resources, which thus can lead to
  faster overall execution, as these resources would have been idle if only a
  single thread was executed.
- If a thread cannot use all the computing resources of the CPU (because
  instructions depend on each other's result), running another thread can avoid
  leaving these idle.
- If several threads work on the same set of data, they can actually share
  their cache, leading to better cache usage or synchronization of its values.

Some criticisms of multithreading include:

- Multiple threads can interfere with each other when sharing hardware
  resources such as caches or translation look aside buffers (TLBs).
- Execution times of a single thread are not improved but can be degraded, even
  when only one thread is executing. This is due to slower frequencies and/or
  additional pipeline stages that are necessary to accommodate thread-switching
  hardware.
- Hardware support for multithreading is more visible to software, thus
  requiring more changes to both application programs and operating systems
  than multiprocessing.
- Thread scheduling is also a major problem in multithreading.

Michael K. Gschwind, et al. [#]_

.. [#]  Michael K. Gschwind, Valentina Salapura. 2011. Using Register Last Use Infomation to Perform Decode-Time Computer Instruction Optimization US 20130086368 A1 [Patent]. http://www.google.com/patents/US20130086368



Worker and Timer
----------------

In the Sight architecture, the namespace ``thread`` of the ``core`` library provides few tools to execute
asynchronous tasks on different threads.

In this library, the class ``Worker`` creates and manages a task loop. The default
implementation creates a loop in a new thread. Some tasks can be posted on the
worker and will be executed on the managed thread. When the worker is stopped,
it waits for the last task to be processed and stops the loop.

.. code-block:: cpp

    sight::core::thread::worker::sptr worker = sight::core::thread::worker::make();

    ::std::packaged_task<void> task( ::std::bind( &myFunction ) );
    ::std::future< void > future = task.get_future();
    ::std::function< void () > f = moveTaskIntoFunction(task);

    worker->post(f);

    future.wait();
    worker->stop();

The Timer class provides single-shot or repetitive timers. A Timer triggers a
function once after a delay, or periodically, inside the worker loop. The delay
or the period is defined by the duration attribute.

.. code-block:: cpp

    auto worker = sight::core::thread::worker::make();

    auto timer = worker->createTimer();

    timer->set_function(std::bind(&myFunction));

    std::chrono::milliseconds duration = std::chrono::milliseconds(100) ;
    timer->set_duration(duration);

    timer->start();
    //...
    timer->stop();

    worker->stop();

Mutex
-----

The namespace ``sight::core::mt`` provides common foundations for multithreading in
Sight, especially tools to manage mutual exclusions. In computer science,
mutual exclusion refers to the requirement of ensuring that two concurrent
threads are not in a critical section at the same time, it is a basic
requirement in concurrency control, to prevent race conditions. Here, a
critical section refers to a period when the process accesses a shared
resource, such as shared memory. A lock system is designed to enforce a mutual
exclusion concurrency control policy.

Currently, Sight uses Boost Thread library which allows the use of multiple
execution threads with shared data, keeping the C++ code portable.
``sight::core::mt`` defines a few typedef over Boost:

.. code-block:: cpp

  namespace sight::core::mt
  {

  using mutex       = boost::mutex;
  using scoped_lock = boost::unique_lock<mutex>;

  /// Defines a single writer, multiple readers mutex.
  using read_write_mutex = boost::shared_mutex;
  /**
  * @brief Defines a lock of read type for read/write mutex.
  * @note Multiple read lock can be done.
  */
  using read_lock = boost::shared_lock<read_write_mutex>;
  /**
  * @brief Defines a lock of write type for read/write mutex.
  * @note Only one write lock can be done at once.
  */
  using write_lock = boost::unique_lock<read_write_mutex>;
  /**
  * @brief Defines an upgradable lock type for read/write mutex.
  * @note Only one upgradable lock can be done at once but there may be multiple read lock.
  */
  using read_to_write_lock = boost::upgrade_lock<read_write_mutex>;
  /**
  * @brief Defines a write lock upgraded from read_to_write_lock.
  * @note Only one upgradable lock can be done at once but there may be multiple read lock.
  */
  using upgrade_to_write_lock = boost::upgrade_to_unique_lock<read_write_mutex>;

  } // namespace sight::core::mt



Multithreading and communication
---------------------------------

Asynchronous call
~~~~~~~~~~~~~~~~~

Slots are able to work with ``sight::core::thread::worker``. If a Slot has a Worker, each
asynchronous execution request will be run in its worker, otherwise
asynchronous requests can not be satisfied without specifying a worker.

Setting worker example:

.. code-block:: cpp

    sight::core::com::slot< int (int, int) >::sptr slotSum
            = sight::core::com::new_slot( &sum );
    sight::core::com::slot< void () >::sptr slotStart
            = sight::core::com::new_slot( &A::start, &a );

    sight::core::thread::worker::sptr w = sight::core::thread::worker::make();
    slotSum->set_worker(w);
    slotStart->set_worker(w);


``async_run`` method returns a std::shared_future< void >, that makes it possible
to wait for end-of-execution.

.. code-block:: cpp

    std::future< void > future = slotStart->async_run();
    // do something else ...
    future.wait(); //ensures slotStart is finished before continuing

``async_call`` method returns a ``std::shared_future< R >`` where R is the return
type. This allows facilitates waiting for end-of-execution and retrieval of the computed value.

.. code-block:: cpp

    std::future< int > future = slotSum->async_call();
    // do something else ...
    future.wait(); //ensures slotStart is finished before continuing
    int result = future.get();

In this case, the slots asynchronous execution has been *weakened*. For an async call/run
pending in a worker queue, it means that :

- if the slot is detroyed before the execution of this call, it will be
  canceled.
- if slot's worker is changed before the execution of this call, it will also
  be canceled.

Asynchronous emit
~~~~~~~~~~~~~~~~~

As slots can work asynchronously, triggering a Signal with async_emit results in
the execution of connected slots in their worker :

.. code-block:: cpp

    sig2->async_emit(21, 42);

The instruction above has the consequence of running each connected slot in its
own worker.

Note: Each connected slot must have a worker set to use async_emit.


Object-Service and Multithreading
----------------------------------

Object
~~~~~~

The architecture allows the writing of thread safe functions which manipulate objects
easily. Objects have their own mutex (inherited from ``sight::data::object``) to
control concurrent access from different threads. This mutex is available using the following method:

.. code-block:: cpp

    sight::core::mt::ReadWriteMutex & get_mutex();

The namespace ``sight::data::mt`` contains several helpers to lock objects for
multithreading:

- ``read_lock``: locks an object mutex on read mode.
- ``read_to_write_lock``:  locks an object mutex on upgradable mode.
- ``write_lock``: locks an object mutex on exclusive mode.

The following example illustrates how to use these helpers:

.. code-block:: cpp

    auto m_data = std::make_shared<sight::data::string>();
    {
        // lock data to write
        sight::data::mt::read_lock read_lock(m_data);
    } // helper destruction, data is no longer locked


    {
        // lock data to write
        sight::data::mt::write_lock write_lock(m_data);

        // unlock data
        write_lock.unlock();

        // lock data to read
        sight::data::mt::read_to_write_lock upgrade_lock(m_data);

        // unlock data
        upgrade_lock.unlock();

        // lock again data to read
        upgrade_lock.lock();

        // lock data to write
        upgrade_lock.upgrade();

        // lock data to read
        upgrade_lock.downgrade();

    } // helper destruction, data is no longer locked



Services
~~~~~~~~

The service architecture allows the writing of a thread-safe service by
avoiding the requirement of explicit synchronization. Each service has an associated
worker in which service methods are intended to be executed.

Specifically, all inherited ``sight::service::base`` methods (``start``, ``stop``,
``update``, ``receive``, ``swap``) are slots. Thus, the whole service life
cycle can be managed in a separate thread.

Since services are designed to be managed in an associated worker, the worker
can be set/updated by using the inherited method :

.. code-block:: cpp

    // Initializes m_associatedWorker and associates
    // this worker to all service slots
    void set_worker( sight::core::thread::worker::sptr worker );

    // Returns associate worker
    sight::core::thread::worker::sptr getWorker() const;

Since the signal-slot communication is thread-safe and
``sight::service::base::receive(msg)`` method is a slot, it is possible to attach a service
to a thread and send notifications to execute parallel tasks.

.. note::
    Some services use or require GUI backend elements. Thus, they can't be used
    in a separate thread. All GUI elements must be created and managed in the
    application main thread/worker.

