.. _SADGenericScene:

Generic Scene
==============

Overview
------------------------

A generic scene in Sight is a feature to visualize elements like meshes or images in a scene.
It uses the Ogre3d library as a rendering engine. The generic scene is universal and therefore applicable for diverse
visualization tasks.

Main rendering service
------------------------

The service ``sight::viz::scene3d::render`` is the manager of the 3D scene. Its main task is to instantiate an OpenGL context.
In addition, it configures the rendering properties and describes a list of *adaptors*,
which are dedicated services that render Sight data into this rendering context.

.. code-block:: xml

    <service uid="generiSceneUID" type="sight::viz::scene3d::render" >
        <scene renderMode="auto|sync" offScreen="imageKey" width="1920" height="1080">
            <renderer id="myRenderer" layer="0" background="0.0" />
            <layer id="..." order="1" />
            <layer id="..." order="2" />

            <adaptor uid="..."/>
            <adaptor uid="..."/>

        </scene>
    </service>


renderMode (optional, "auto" by default)
    This attribute is forwarded to all adaptors. In the "auto" mode, each adaptor may then trigger a rendering request
    when it updates, then the system will render a new frame as soon as possible. The mode "sync" is rather designed to
    allow the application to request a rendering, when its update loop is done. Most of the time, this is done when
    all adaptors are first updated. The tutorial :ref:`TutorialsTuto17SimpleAR` shows how to achieve this.

offScreen (optional):
    Key of the image used for off screen rendering

width (optional, "1280" by default):
    Width for off screen render

height (optional, "720" by default):
    Height for off screen render

renderer
    Defines a renderer. At least one renderer is mandatory, but there can be multiple renderer on different layers.

    - **id** (mandatory): the identifier of the renderer
    - **layer** (optional): defines the layer of the renderer.
    - **background** (optional): the background color of the rendering screen.

    The color value can be defined as a grey level value (ex . 1.0 for white)
    or as a hexadecimal value (ex : \#ffffff for white).

adaptor
    Defines the adaptors to display in the scene.

    - **uid** (mandatory): the uid of the adaptor service

Adaptor
-------------

An adaptor (inherited from ``sight::viz::scene3d::adaptor``) is a service to manipulate or display a sight data.
Services representing an adaptor are managed by a generic scene (``sight::viz::scene3d::render``).
The adaptors are the gateway between Sight objects and Ogre objects.
To respect the principles of the framework, adaptors are kept as generic as possible.
Therefore they are reusable in other applications or even adaptors as sub-services.

As usual, an adaptor needs to implement the methods ``configuring``, ``starting``, ``stopping``, and ``updating``.


.. code-block:: cpp

    class my_adaptor : public sight::viz::scene3d::adaptor
    {

    public:

        SIGHT_DECLARE_SERVICE(my_adaptor, sight::viz::scene3d::adaptor);

    protected:

        /// Parse the adaptor "config" tag
        void configuring(const config_t& _config) override;

        /// Initialize the pipeline (scene nodes, material, , ...)
        void starting() override;

        /// Clear the pipeline
        void stopping() override;

        /// Update the pipeline from the current object
        void updating() override;
    };

To ease the configuration and the link with the ``sight::viz::scene3d::render``, the ``configuring`` and ``starting``
should contain this minimal code:

.. code-block:: cpp

    void mesh::configuring(const config_t& _config)
    {
        this->configureParams();
        ...
    }

    void mesh::starting()
    {
        this->initialize();

        ...

        // Request sight::viz::scene3d::render to trigger a rendering when it is ready
        this->requestRender();
    }


Adaptors are configured and started like other services in the xml since **Sight 12.0.0**.

.. code-block:: xml

    <service uid="meshAdaptor" type="sight::module::viz::scene3d::adaptor::mesh" >
        <in key="mesh" uid="..." />
        <config layer="..." transform="..." visible="true" materialName="..." shadingMode="phong" />
    </service>

    ...

    <start uid="meshAdaptor" />
