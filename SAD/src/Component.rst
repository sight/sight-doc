Modules
===============

*Sight* bases its architecture on components. Component-based software engineering (CBSE) (also known as
component-based development (CBD)) is a
branch of software engineering that emphasizes the separation of concerns in
respect of the wide-ranging functionality available throughout a given software
system. It is a reuse-based approach to define, implement and compose
loosely coupled independent components into systems. This practice aims to
bring about an equally wide-ranging degree of benefits in both the short-term
and the long-term for the software itself and for organizations that sponsor
such software. (Excerpt from the Wikipedia page "Component-based software engineering" [#]_ )

In *Sight* we rather use the terminology **module** instead of component, like many other software.

Definition
------------

A module is a software package that encapsulates a set of code and resources. Basically,
it proposes extension points and/or implements those extensions. The content of a module
is semantically related. Developers should design and implement modules in such
a way that different programs can reuse them.

In *Sight*, the modules are used to define libraries of services and application configurations.

Implementation
-------------------------

At runtime (in the build or install directory), a module is a folder on the filesystem that contains :

- [required] the module description file ``plugin.xml`` that describes the content of the
  dynamic library,
- the dynamic library (.so or .dll depending on the operating system) containing the code,
- file resources (icons, XSD file, media files, ...)

The content of the ``plugin.xml`` is parsed when launching the application, which then allows
*Sight* to automatically load the necessary modules when necessary.

The source directory of a module contains the following elements:

- [required] ``CMakeLists.txt`` to generate the *CMake* target (see :ref:`HowTosCMake`).
- [optional] C++ source and header files
- [required] *rc* folder to contain resources
  - [required] the module description file ``plugin.xml``
  - [optional] other XML configuration files, included from the main ``plugin.xml``
  - [optional] file resources
- [optional] a *test* folder to contain the unit tests

plugin.xml
+++++++++++++

The module description file (plugin.xml) is used to describe which extension and extension implementations are proposed
by the component.

A typical case of an extension point is represented by an abstract class, where the implementation is a inherited
class. The extension can be defined in a module and the implementation in an another one. This
is for example how services are implemented. The module description file
of ``sight::module::service`` introduces the service extension and brings the interface ``sight::service::base``:

.. code-block:: xml

 <plugin id="sight::module::service" library="true">

   <extension-point id="sight::service::extension::factory" />

 </plugin>

The attribute ``id`` of the ``<plugin>`` tag usually follows the location of the module on the filesystem.
It contains the main namespace, here ``sight``, then the ``module`` keyword, then a optional ``category`` and
last the name of the module itself. It is highly recommend to follow this naming convention to avoid name conflicts
when working with multiple repositories.

The attribute ``library`` indicates if the module contains code. If ``false``, this means the module only contains
XML configuration files or file resources.

When creating a new service in another component, the description file implements the extension with
something like this.

.. code-block:: xml

  <plugin id="sight::module::foo" library="true">

      <! -- my_module requires the module service to run -->
      <requirement id="sight::module::service" />

      <! -- Need code related to sight::io::reader -->
      <requirement id="sight::io" />

      <extension implements =" sight::service::extension::factory ">
          <! -- service type -->
          <type>sight::io::reader</type>
          <! -- the service name available in this component library -->
          <service>sight::module::foo::my_reader</service>
          <! -- the object type associated to the service -->
          <object>sight::data::myData</object>
          <desc>Description of my reader</desc>
      </extension>

  </plugin>

Extensions are not limited to implements class interfaces. Other extensions can be defined : service configurations,
operator parameters, activities, etc...

.. [#] Component-based software engineering http://en.wikipedia.org/wiki/Component-based_software_engineering
