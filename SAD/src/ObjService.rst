Object-Service concept
======================

Introduction
------------

In the *Object Oriented Programming* (OOP) paradigm, an object is an instance of a class
which contains the data and the algorithms. For instance, a class Image contains the image buffer,
the size and format attributes, along with the methods to process the image,
such as reading, writing, visualizing, analyzing, etc.

This design works well, but has some drawbacks.
First the code of the class implementation can become very big
if you put everything in it, making collaborative work harder.
Then if you use different third-party libraries in your methods
(for instance DCMTK for I/O, VTK for visualization, OpenCV or ITK for the filtering methods),
your class becomes dependent of all of these libraries even if you only need one or two functionalities.
This is not compatible with a modular approach.
Last, because of the two previous points, the maintenance of source code becomes tedious.

Instead, Sight proposes an *Object-Service* paradigm where data and algorithms are separated into different code units.

Object
-------

Objects represent data.
They can be simple (boolean, integer, string, etc.) or advanced structures (image, mesh, video, patient, etc.).
They are generic, which means they do not depend on the original input format or the future output format.
This way, objects can be used with different third-party libraries.
We provide helper methods to convert them into the internal format of these libraries.

These object classes contain only data features and their corresponding getter/setter methods.

For instance, the ``image`` object:

- contains a buffer pointer, a buffer size, the image's dimension and origin,
- has public setter/getter methods to access these members,
- does not have methods such as reading or writing a buffer

The ``sight::data`` is the main data library of Sight. Other data exist in other libraries or modules and this can
be extended anywhere. A data list with a brief description is available in the appendices.

Creation
~~~~~~~~~~

New data must be created as described below.

In the header file (``my_data.hpp``):

.. code-block:: cpp

    class my_data : public sight::data::object
    {
    public :
        SIGHT_DECLARE_CLASS(my_data, sight::data::object) ;

        // Constructor, may be used to initialize data and signals
        my_data();

        /// Destructor, required for all data
        ~my_data() override;

        /// Defines shallow copy (pointer addresses are copied), required for all data
        void shallow_copy( const sight::data::object::csptr& _source ) override;

        /// Defines deep copy (pointer objects are copied), required for all data
        void deep_copy(const sight::data::object::csptr& _source, DeepCopyCacheType &cache) override;
    };

In the source file (``my_data.cpp``), this line must also be added to register ``my_data`` in the data factory registry.

.. code-block:: cpp

    SIGHT_REGISTER_DATA( my_data );

With this declaration and registration, the data object can then be instantiated in two ways:

.. code-block:: cpp

    // Direct creation, which requires to link at compile time with the implementation code
    my_data::sptr obj = my_data::make();

    // Factory creation, which does not requires to link at compile time with the implementation code,
    // but implies that the library should be loaded at runtime
    sight::data::object::sptr obj = sight::data::factory::make("my_data");
    // If you link with the implementation, you may cast to the data type
    auto my_data = std::dynamic_pointer_cast<my_data>(obj);

Service
-------

A service represents a functionality which uses or modifies data. **It
is associated with one or several objects**. For example, a service working on a
single image could be a reader, a writer, or a visualization service. A service working on two images could be a
filtering service, or a service working on a image and a mesh, a mesher.

Interfaces
~~~~~~~~~~~~

Several service categories exist in Sight. These categories are called *service
types* and are represented by an abstract class. Here are few samples of basic service types:

- ``sight::io::reader``: base interface for reader services.
- ``sight::io::writer``: base interface for writer services.
- ``sight::ui::action``: base interface to manage action from a button or a menu in the GUI.
- ``sight::ui::editor``:  base interface to create new widgets in the GUI.
- ``sight::viz::render``: base interface to create new visualization widgets in the GUI.
- ``sight::service::controller``: does nothing in particular but can be considered as
  a default service type to be implemented by unclassified services.

All services require a type association and must inherit from an abstract type service.

Life cycle
~~~~~~~~~~~~~

A service uses the *State* pattern.

.. graphviz::
   :align: center

   digraph foo {
      "configure" -> "start" -> "update" -> "stop";
      "update" -> "update";
   }


- ``configure``: parses the service parameters and analyzes its
  configuration. For example, this is used to configure an image file
  path on the file system for an image reader service.
- ``start``: initializes and launches the service. For
  example, for a GUI service, this instantiates all GUI widgets.
- ``stop``: stops the service. For example, for a GUI
  service, this detaches and destroys all GUI widgets previously
  instantiated earlier in the ``start`` method.
- ``update``: performs an action on the data associated with the service. For example, for an image reader service,
  the service reads the image, converts it and loads it into the associated data.

These methods must be called with respect to the state sequence. For example, it is not possible to
stop a service before starting it.

Here is a C++ sample of the whole life cycle of a service:

.. code-block:: cpp

    #include <sight/service/base.hpp>

    my_data::sptr input_data = my_data::make();
    my_data::sptr modified_data = my_data::make();
    my_service::sptr srv = sight::service::add("my_service"); // creates the service
    srv->set_input(input_data, "input");  // registers the input data
    srv->set_inout(modified_data, "modified"); // registers the input/output data

    srv->set_config( ... ); // set parameters
    srv->configure(); // check parameters
    srv->start(); // start the service
    srv->update(); // update the service, can be called many times
    srv->stop(); // stop the service
    sight::service::remove(srv); // destroy the service

.. note::
    Sight extensively uses `std::shared_ptr <http://en.cppreference.com/w/cpp/memory/shared_ptr>`_ to handle objects
    and services. The basic declaration macros of data and services define a typedef ``sptr`` as an alias to
    ``std::shared_ptr<this_class>`` and a typedef ``csptr`` as an alias to ``std::shared_ptr<const this_class>``.

Creation
~~~~~~~~~~~~~~~~

A new service class can be created as described below (see also :ref:`HowTosServiceCreation`). First in the header
file (``my_service.hpp``), some functions from ``sight::service::base`` must be overridden:

.. code-block:: cpp

    class my_service final : public sight::service::base
    {
    public:

        // Macro to define few important parameters/functions used by the architecture
        SIGHT_DECLARE_SERVICE(my_service, sight::service::base);

        // Service constructor
        my_service() noexcept;

        // Service destructor.
        ~my_service() noexcept final;

    protected:

        // To configure the service
        void configuring(const config_t& _config) final;

        // To start the service
        void starting() final;

        // To stop the service
        void stopping() final;

        // To update the service
        void updating() final;
    };

In the source file (``my_service.cpp``), the following lines must also be added to register ``my_service`` :

.. code-block:: cpp

    SIGHT_REGISTER_SERVICE( sight::service::base, my_service );
    SIGHT_REGISTER_SERVICE_OBJECT( my_service, my_data )

However almost everywhere, services are described by a ``plugin.xml`` file which makes this registration step redundant
and useless. Indeed, our build system generates, from the ``plugin.xml``, a file called ``registerServices.cpp``
where these macros are automatically added.

With this declaration and registration, the service can then be instantiated in different ways:

.. code-block:: cpp

    // Returns a generic service
    sight::service::base::sptr srv = sight::service::add("my_service");

    // Returns the service type, which requires to link at compile time with the implementation code
    my_service::sptr srv = sight::service::add<my_service>("my_service");


Object/Service registration
------------------------------

Each object associated with a service is identified with a **key** and accessed with a given **type**:

- **in**: read only, when the data is not modified. That implies you will only get a const pointer on the data.
- **inout**: read/write, when the data **exists** before the creation of the service and can be modified.
- **out**: read/write, when the data **does not exist** before the creation of the service. This happens when
  the service itself creates the data, during ``start()``, ``update()`` or anywhere else where the service is started.

The key and access of parameters are static, they can't change at runtime. To declare a data in ``sight::service``, just
add a ``sight::data::ptr`` member in the service class header:

.. code-block:: cpp

    sight::data::ptr<sight::data::image, sight::data::access::in> m_image {this, "image"};
    sight::data::ptr<sight::data::mesh, sight::data::access::inout> m_mesh {this, "mesh"};
    sight::data::ptr<sight::data::image, sight::data::access::out> m_result {this, "result"};

Then, to actually set a data instance to a service, just call ``set_input()``, ``set_inout()`` or ``set_output()``
accordingly to the access type:

.. code-block:: cpp

    sight::service::base::sptr srv = sight::service::add("sight::my_module::my_service");
    sight::data::image::sptr image = std::make_shared<sight::data::image>();
    srv->set_input("image", image);
    sight::data::image::sptr mesh = std::make_shared<sight::data::mesh>();
    srv->set_inout("mesh", mesh);
    sight::data::image::sptr result = std::make_shared<sight::data::image>();
    srv->set_output("result", result);


Note that with :ref:`AppConfig<SADAppConfig>`, most of the time, the association of *input* and *inout* data will be
done automatically by the *Sight* runtime, unless you create services in the C++ code. For *output* data, this is
slightly more complicated and this will be explained right after the next section.

Object retrieval
~~~~~~~~~~~~~~~~~

Getting a data from a service is fairly simple, since the interface of ``sight::data_ptr`` is very close to a
``weak_ptr``. This means to get a data, you need first to lock the ``sight::data::ptr`` member:

.. code-block:: cpp

    auto input = m_input.lock();
    sight::data::image& image = *input;
    sight::data::image::sptr pointer_to_image = input.get_shared();

There is actually some magic behind this. ``lock()`` actually returns a ``sight::data::mt::locked_ptr<T>``.
This special pointer class is a RAII (Resource Acquisition Is Initialization) object that ensures that the data object
is protected from concurrent access and from dumping on disk (explained later). This is very important because services
can run asynchronously on different threads. The RAII mechanism will ensure that everything is unlocked once the
``locked_ptr`` is destroyed. Once destroyed, you must not access the data objects as there is no more concurrent access
nor dumping protection.

You may also access the objects bound to a service with getter methods. This can be useful when you are outside of the
class code itself. Similarly, the returned pointer object needs to be locked:

.. code-block:: cpp

    auto input = srv->getInput().lock();
    sight::data::image& image = *input;
    sight::data::image::sptr pointer_to_image = input.get_shared();


Last, it is worth mentioning that the access type of the object implies different mutex types:

- **in**: *read* (*shared*), when locked, other pointer of this type can also access the data,
- **inout**: *write* (*exclusive*), when locked, no other pointer can access the data,
- **out**: write* (*exclusive*), when locked, no other pointer can access the data.

.. note::

    ``locked_ptr`` is not "recursive", which means getting two ``locked_ptr`` in the same thread will
    lead to a dead lock.


Output objects
*****************

With input and inout objects, services do not handle the lifetime of objects. For output objects, this is a bit
trickier. As explained above, when a service generates a new object, it can share it with other services by
calling ``set_output("<key>", <object>)``. But it is more convenient to directly use the ``sight::data::ptr`` member to
this end by simply using the ``operator=``:

.. code-block:: cpp

    // From the service code
    sight::data::image::sptr result = std::make_shared<sight::data::image>();
    m_result = result;

    // Reset the data
    m_result.reset();
    // Or alternatively
    m_result = nullptr;

When the data is created in the service, the service becomes responsible of the lifetime of the output object.
Thus it is crucial to reset the data when the service is destroyed. It could be potentially dangerous if other services
work on this object, so this must be handled with care.

When the data is not created, but extracted for instance from an input object, the service is not responsible
of the lifetime of the object. When it resets the pointer, the data will no longer be available in this service but
the data object will still be allocated in memory.


.. _Object-Service_example:

Object-Service concept example
*********************************

To conclude, the generic object-service concept is illustrated with this example:

.. code-block:: cpp

    // Create an object
    sight::data::object::sptr obj = sight::data::factory::make("sight::data::image");

    // Create a reader and a view for this object
    sight::service::base::sptr reader = sight::service::add("my_custom_image_reader");
    reader->set_inout(obj, "data");
    sight::service::base::sptr view = sight::service::add("my_custom_image_reader");
    view->set_input(obj, "object");

    // Configure and start services
    reader->set_config ( /* ... */ );
    reader->configure();
    reader->start();

    view->set_config ( /* ... */ );
    view->configure();
    view->start();

    // Execute services
    reader->update(); // Read image on filesystem
    view->update(); // Refresh visualization with the new image buffer

    // Stop services
    reader->stop();
    view->stop();

    // Destroy services
    sight::service::remove(reader);
    sight::service::remove(view);

This example shows the code to create a small application to read an image
and visualize it. You can easily transform this code to build an application
which reads and displays a 3D mesh by changing object and services
implementation strings only.

However, most applications made with Sight are not built this way. Instead, we use :ref:`AppConfig<SADAppConfig>`,
which allows to simplify the code above by a declarative approach based on XML files.

Group of Objects
***************************

For the sake of simplicity, we only described single data objects in services so far. However, it is also possible to
define a vector of objects of the same type as input, inout or output.

To declare such a group, the ``sight::data::ptr_vector`` class must be used instead of the ``sight::data::ptr`` class:

.. code-block:: cpp

    data::ptr_vector<data::mesh, data::Access::inout> m_meshes {this, "meshes"};

Setting data only requires to pass a third argument to specify the index of the object:

.. code-block:: cpp

    sight::service::base::sptr srv = sight::service::add("sight::my_module::my_service");
    sight::data::mesh::sptr mesh0 = std::make_shared<sight::data::mesh>();
    sight::data::mesh::sptr mesh1 = std::make_shared<sight::data::mesh>();
    srv->set_input("meshes", mesh0, 0);
    srv->set_input("meshes", mesh1, 1);


Then, accessing data is then quite as straightforward as using a ``std::vector<>``:

.. code-block:: cpp

    // Access data in a group using indexes
    for(int i = 0; i < m_meshes.size(); ++i)
    {
       auto data = m_meshes[i].lock();
       ...
    }

    // Access data in a group using for range loop - this gives access to the underlying map,
    // that's why '.second' should be specified, while '.first' gives the index
    for(const auto& data : m_meshes)
    {
        auto data = data.second.lock();
    }

    // Preferred alternative using C++ 17 structured binding
    for(const auto&[index, data] : m_meshes)
    {
       auto lockedData = data.lock();
    }

Last, inside the service, it is also possible to set the output data in a similar way:

.. code-block:: cpp

    for(int i = 0; i < m_meshes.size(); ++i)
    {
       sight::data::mesh::sptr mesh = std::make_shared<sight::data::mesh>();
       m_meshes[i] = mesh;
    }

    for(const auto&[_, data] : m_meshes)
    {
       sight::data::mesh::sptr mesh = std::make_shared<sight::data::mesh>();
       data = mesh;
    }

    // Reset the data
    for(const auto&[_, data] : m_meshes)
    {
       data.reset();
    }

    // Reset the data, alternative
    for(const auto&[_, data] : m_meshes)
    {
       data = nullptr;
    }
