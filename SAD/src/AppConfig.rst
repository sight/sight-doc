.. _SADAppConfig:

Application
===============

As shown in the :ref:`Object-Service concept example<Object-Service_example>`, *Sight* allows to create an application
by instantiating objects and services. Unfortunately, the C++ API does not scale very well, and it can becomes very
tedious to apply to bigger applications containing many services and objects.

To overcome this, *Sight* provides the ``sight::app`` library that proposes a descriptive API to define applications
with objects, services and communication. It mainly consists in writing configuration XML plugin files.
The XML configuration for an application is actually defined by declaring a new extension
of type ``sight::app::extension::config``. We often abbreviate it as **app config**.


Configuration
----------------------

Here is a simple example corresponding to the configuration XML file of the previous :ref:`Object-Service_example`,
that reads an image and visualizes it, decoupling each task in a dedicated service.

.. code-block:: xml

    <!-- 1. Declaration of objects -->
    <object uid="image" type ="sight::data::data" />

    <!-- 2. Declaration of services -->
    <service uid="frame" type="sight::ui::frame">
        <!-- service configuration -->
    </service>

    <service uid="view" type="sight::ui::view">
        <in key="object" uid="image" />
        <!-- service configuration -->
    </service>

    <service uid="reader" type="sight::io::reader">
        <in key="object" uid="image" />
        <!-- service configuration -->
    </service>

    <!-- 3. Declaration of communications between objects and services -->
    <connect>
        <signal>image/modified</signal>
        <slot>view/render</slot>
    </connect>

    <!-- 4. Start and update services, defines the creation order and the start order -->
    <start uid="frame" />
    <start uid="view"/>
    <start uid="reader"/>

    <update uid ="reader"/> <!-- Read the image on filesystem and notify the view to refresh is content -->

Instead of a pointer in C++, objects and services are identified by an ``uid``, which allows to reference them after
their declaration. They are also assigned a ``type`` that refer to their concrete implementation in C++.

After services declaration, a section allows to connect signals and slots together, as soon as objects and services are
created. In this example, the view triggers a new rendering when the image is modified.

Services are created in the order they are actually started with the ``start`` tags, not their order of declaration.
Thus the ``start`` tag of the ``reader`` service actually hides the following C++ code:

.. code-block:: c++

    auto reader = sight::service::add("sight::io::reader"); // creates the service
    reader->set_input(image, "image");  // registers the input data
    reader->set_config( ... ); // set configuration
    reader->configure(); // check parameters
    reader->start(); // start the service

Last the ``update`` tag just allows to call this slot once after the start sequence.

In-depth explanations
-------------------------

The previous example was actually a bit simplified, notably concerning the type and the configuration of the services.
As discussed in the introduction of this chapter, to create an application, it is required to declare an extension point
of type ``sight::app::extension::config``. Such an extension requires different XML tags:

    * **id**: the configuration identifier, and is thus unique to each configuration.
    * **desc** (optional): the description of the configuration.
    * **parameters** (optional): defines the list of parameters required to launch the app config.

       * **param.name**: parameter name, used as ``${app_name}`` in the configuration. It will be replaced by the
         string passed by the configuration launcher.
       * **param.default** (optional): default value for the parameter, it is used if the value is not provided by the
         the configuration launcher.
    * **config**: the actual description of the application, with objects and services.

The following ``plugin.xml`` file demonstrates a proper description of the configuration of the previous example.

.. code-block:: xml

    <extension implements="sight::app::extension::config">
        <id>app_config_id</id>
        <parameters>
            <param name="app_name" default="my Application" />
            <param name="app_icon_path" />
        </parameters>
        <desc>Image Viewer</desc>
        <config>

            <object uid="image" type="sight::data::image" />

            <!--
                Description service of the GUI:
                The sight::module::ui::frame service automatically positions the various
                containers in the application main window.
                Here, it declares a container for the 3D rendering service.
            -->
            <service uid="frame" type="sight::module::ui::frame">
                <gui>
                    <frame>
                        <name>${app_name}</name>
                        <icon>${appIconPath}</icon>
                        <minSize width="800" height="600" />
                    </frame>
                </gui>
                <registry>
                    <!-- Associate the container for the rendering service. -->
                    <view sid="renderer" />
                </registry>
            </service>

            <!--
                Reading service:
                The <file> tag defines the path of the image to load. Here, it is a relative
                path from the repository in which you launch the application.
            -->
            <service uid="reader_path_file" type="sight::module::io::vtk::image_reader">
               <inout key="target" uid="image" />
               <file>./TutoData/patient1.vtk</file>
            </service>

            <!--
                Visualization service of a 3D medical image:
                This service will render the 3D image.
            -->
            <service uid="renderer" type="sight::module::viz::sample::image">
               <in key="image" uid="image" />
            </service>

            <!--
                Definition of the starting order of the different services:
                The frame defines the 3D scene container, so it must be started first.
                The services will be stopped the reverse order compared to the starting one.
            -->
            <start uid="frame" />
            <start uid="reader_path_file" />
            <start uid="renderer" />

            <!--
                Definition of the service to update:
                The reading service load the data on the update.
                The render update must be called after the reading of the image.
            -->
            <update uid="reader_path_file" />
            <update uid="renderer" />

        </config>
    </extension>

Here two parameters are required, the application name and the path of the icon. For the main configuration, this is
done by declaring an extension point:

.. code-block:: xml

    <extension implements="sight::app::extension::parameters">
        <id>app_config_params</id>
        <parameters>
            <param name="app_name" value="app_config" />
            <param name="app_icon_path" value="app_config/app.ico" />
        </parameters>
    </extension>

Then these parameters can then be passed to the ``sight::module::app`` responsible of the parsing and the start of
XML-based applications. This is done when declaring its use in the ``CMakeLists.txt`` of the application module.
Indeed, this build file defines the application target, lists the required modules, and can pass parameters to each
depending module with the ``module_param`` CMake macro (see :ref:`HowTosCMakeLists` for more details on the build
system). In this case, we simply set the parameters ``parameters``:

.. code-block:: cmake

    sight_add_target(sample_app TYPE APP) # Notice the type APP is used

    add_dependencies(sample_app sightrun module_app module_ui_qt module_viz_sample module_io_vtk)

    module_param(module_app PARAM_LIST config parameters PARAM_VALUES app_config_id app_config_params)

As explained later in this chapter, an application can be made of multiple XML files. Launching these configurations
and passing parameters between them will be detailed at this time.

Objects
*************

Inside the ``<config>`` tag, the first elements must the ``<object>`` tags, that define the objects to be used.
These have the following attributes:

- **uid** (optional): identifier of the object ``sight::core::id``. If it is not defined, it will be
  automatically generated. This is not important at this stage, but *Sight* always makes this identifier unique alone.
  This is helpful when the app config is launched multiple times.
- **type**: object type (ex: ``sight::data::image``, ``sight::data::map``)
- **src** (optional, **new** by default) possible values:

  - **new** : defines that the object should be created.
  - **ref** : defines that the object already exists in the application. The uid must be the same as the first
    declaration of this object (with "new").
  - **deferred** : defines that the object will be created later (by a service).

Services
*************

The ``<service>`` tag represents a service. It enumerates the data it uses and how it accesses them.
A service needs a specific configuration, which is usually described in the Doxygen.

.. code-block:: xml

    <service uid="mesher" type="sight::module::filter::mesher" worker="myWorker">
        <in key="image" uid="imageId" auto_connect="true" />
        <out key="mesh" uid="meshId" />
        <inout group="matrix">
            <key uid="matrix1" />
            <key uid="matrix2" auto_connect="true" />
        </inout>
    </service>


- **uid** (optional): unique identifier of the service. If it is not defined, it is automatically generated.
- **type**: Service implementation type (ex: ``sight::io::vtk::image_reader``)
- **auto_connect** (optional): defines if the service receives the signals of all objects
- **worker** (optional): allows to run the service in a specific worker (see :ref:`SADMultithreading`)

The data objects members declared with ``sight::data::ptr`` and ``sight::data::ptr_vector`` in a service class are
bound in the service configuration by specifying their access type:

- **in**: input object, it is const and cannot be modified
- **inout**: input object that can be modified
- **out**: output object, it must be created by the service.
  The bound object must be declared as *deferred* in the \<object\> declaration.

Whatever the access type, each member has the following attributes:

- **key** : object key used to retrieve the object into the service.
- **uid** : unique identifier of the object declared in the ``<object>`` tag.
- **group**: define a group of objects (declared with ``sight::data::ptr_vector``), each data in the group must then
  be defined by a key tag.
- **optional** : (optional, ``<true|false>``, default ``false``) uf ``true``, the service can be started
  even if the object is not present. By definition, the output objects are always optional.
- **auto_connect** (optional, ``<true|false>``, default ``true``): defines if the service connects to the signals of
  the object declared in the ``auto_connections()`` virtual function.


Connections
*************

After the services declaration, the ``<connect>`` tags allow to connect objects with services or directly services
together. The signals and slots must be compatible otherwise an error is raised: the type and the number of arguments
must be identical, or the slot can ignore parameters starting from the right. In other terms, a signal of type
``void (bool, int, float)``, can be connected to a slot of type ``void (bool, int, float)`` or ``void (bool, int)`` or
``void (bool)`` or even ``void ()``.

Apart from the ``signal`` and ``slot`` tags, a channel name can optionally be provided, which will be explained in the
following section.

.. code-block:: xml

    <connect channel="channel">
        <signal>object_uid/signal_name</signal>
        <slot>service_uid/slot_name</slot>
    </connect>

Start/update
**************
- **start**: defines the service to start when the app config is launched. The services will be automatically stopped in
  the reverse order when the app config is stopped.

.. code-block:: xml

    <start uid="service_uid" />

**The services using "deferred" object as input or inout will be automatically started when the object is created.**


- **update**: defines the service to update when the app config is launched.

.. code-block:: xml

    <update uid="service_uid" />


Profile
------------

To launch an XML-based application, we use a launcher called ``sightrun`` and we pass it the path to the application
profile:

.. code::

    bin/sightrun share/<myApplication>_<version>/profile.xml

This ``profile.xml`` file, used as an input of the sightrun command, holds a list of all modules
necessary to run an application. We describe the content of this file here for reference,
but hopefully you do **not** have to write it yourself. When you declare an target of type APP in ``sight_add_target``,
a ``profile.xml`` is generated according to the dependencies set in the ``CMakeLists.txt`` with ``add_dependencies``.
Not that the dependencies are generated recursively.

Here is for example the ``profile.xml`` generated for our example:

.. code-block:: xml

    <!-- WARNING, this file is GENERATED by Sight CMake-based build system from CMake/build/profile.xml.in -->
    <!-- DO NOT EDIT MANUALLY !!! -->

    <profile name="sample_app" version="0.1" check-single-instance="false">

        <activate id="sight::module::viz::sample" />
        <activate id="sight::module::viz::scene3d_qt" />
        <activate id="sight::module::viz::scene3d" />
        <activate id="sight::module::ui::icons" />
        <activate id="sight::module::service" />
        <activate id="sight::module::ui::qt" />
        <activate id="sight::module::ui" />
        <activate id="sight::module::io::session" />
        <activate id="sight::module::data" />
        <activate id="sight::module::io::vtk" />
        <activate id="sight::module::app" >
            <param id="config" value="app_config_id" />
            <param id="parameters" value="app_config_params" />
        </activate>
        <activate id="sample_app" />

        <start id="sight::module::viz::sample" />
        <start id="sight::module::viz::scene3d_qt" />
        <start id="sight::module::viz::scene3d" />
        <start id="sight::module::ui::qt" />
        <start id="sight::module::app" />

    </profile>

activate:
    List of modules used in this application. We see the parameter given to *app* module that
    we wrote in the *CMakeLists.txt*.

start:
    List of modules to start when the application is launched. Basically,
    there are a few modules to start at the beginning:

    - *module_app*: to launch the configuration
    - *module_ui_qt*: to launch the qt event loop for applications with a GUI
    - *module_memory*: to manage image and mesh buffers

    The other modules will be started according to the XML <requirement> tags of the modules,
    or when a service is used in an XML configuration and its module is not started.
    That way we only have the minimum number of shared libraries loaded.

Launch
----------

We showed earlier how an app config is launched. However, it is not reasonable to design a real-life application
with a single configuration.

.. code-block:: xml

    <service uid="..." type="sight::app::config_controller">
        <appConfig id="app_config_id" />
        <inout group="data">
            <key name="image" uid="image" />
            <key name="currentTF" uid="volumeTF" />
        </inout>
    </service>

    <extension implements="sight::app::extension::config">
        <id>sight::config::viz::scene3d::negato2d</id>
        <parameters>
            <object uid="image" type="sight::data::image_series" optional="true" />
            <object uid="transferFunction" type="sight::data::transfer_function" />

            <param name="WID_PARENT" />
            <param name="sliceIndex" default="axial" />
            <param name="enableSliceType" default="true" />
            <param name="WORLD_COORDS_LANDMARKS_CHANNEL" default="WORLD_COORDS_LANDMARKS_CHANNEL" />
            <param name="TOGGLE_VIEW_VISIBILITY_SENDER_CHANNEL" default="TOGGLE_VIEW_VISIBILITY_SENDER_CHANNEL" />
            <param name="RULER_ACTIVATION_CHANNEL" default="RULER_ACTIVATION_CHANNEL" />
            <param name="RULER_DEACTIVATION_CHANNEL" default="RULER_DEACTIVATION_CHANNEL" />
            <param name="VISIBILITY_RULERS_RECEIVER_CHANNEL" />
            <param name="LANDMARKS_EDIT_MODE_RECEIVER_CHANNEL" default="LANDMARKS_EDIT_MODE_RECEIVER_CHANNEL" />
            <param name="LANDMARKS_GROUP_SELECTED_RECEIVER_CHANNEL" default="LANDMARKS_GROUP_SELECTED_RECEIVER_CHANNEL" />
            <param name="LANDMARKS_EDIT_MODE_CHANGED_SENDER_CHANNEL" default="LANDMARKS_EDIT_MODE_CHANGED_SENDER_CHANNEL" />
        </parameters>
        ...
    </extension>

Specific object configuration
-------------------------------

Depending on their type, objects can also be configured in XML to initialize their content.

- **value** (optional): the simplest example is for all types holding scalar values, such as ``sight::data::boolean``,
  ``sight::data::integer``, ``sight::data::real`` or ``sight::data::string``. Their value can be defined with the
  ``value`` attribute.

.. code-block:: xml

    <object type="sight::data::integer">
        <value>42</value>
    </object>

    <!-- Short syntax -->
    <object type="sight::data::integer" value="42">


- **matrix** (optional): specific to ``sight::data::matrix4`` objects. It defines the values of the matrix.

.. code-block:: xml

    <object uid="matrix" type="sight::data::matrix4">
        <matrix>
        <![CDATA[
            1  0  0  0
            0  1  0  0
            0  0  1  0
            0  0  0  1
        ]]>
        </matrix>
    </object>


- **colors** (optional): only ``sight::data::transfer_function`` contains this tag. It allows to fill the transfer
  function values.

.. code-block:: xml

    <object type="sight::data::transfer_function">
        <colors>
            <step color="#ff0000ff" value="1" />
            <step color="#ffff00ff" value="500" />
            <step color="#00ff00ff" value="1000" />
            <step color="#00ffffff" value="1500" />
            <step color="#0000ffff" value="2000" />
            <step color="#000000ff" value="4000" />
        </colors>
    </object>

- **item** (optional): it defines a sub-object of a composite or a field of any other object.

  - **key**: key of the object
  - **object**: the 'item' tag can only contain 'object' tags that represents the sub-object

.. code-block:: xml

    <item key="image">
        <object uid="imageUid" type="sight::data::image" />
    </item>


How is this done?
********************

If you wonder, to define a parser for your own data object, you only need to define a service implementing the interface
``sight::service::object_parser`` and associate it with your data type in the extension:

.. code-block:: xml

    <extension implements="sight::service::extension::factory">
        <type>sight::service::object_parser</type>
        <service>sight::app::parser::image</service>
        <object>sight::data::image</object>
    </extension>

