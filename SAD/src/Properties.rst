Properties
=======================

As shown in :ref:`Object-Service concept example<Object-Service_example>`, services in *Sight* rely on data objects.
Objects are often complex data such as images, models, that do not require much interaction in the XML configuration.

However, services also rely a lot on much simpler data such as algorithm parameters, using simple types such as
numbers and strings. Manipulating these parameters always ends up requiring the same features:

- initialization of the parameter in the XML configuration of the service,
- dynamic update of the parameter through a slot,
- persistence of the parameter during the runtime.

To simplify the coding of these three features, *Sight* proposes the concept of service *properties*.

Definition
------------

A property is defined in the service header in a similar way to a data pointer:

.. code-block:: c++

    // Initializes the property with its name and default value
    sight::data::property<sight::data::boolean> m_enabled {this, "enabled", true};
    sight::data::property<sight::data::real> m_value {this, "value", 0.};

This initialization automates the parsing of this property using a dedicated ``<properties>`` tag. If the property is
not initialized in the configuration, the default value used in the declaration is used.

.. code-block:: xml

    <service uid="..." type="sight::module::sample::service">
        <properties enabled="false" value="12." />
    </service>

In addition, the argument given to each property can be a reference to an existing supported data object:

.. code-block:: xml

    <object uid="real_value" type="sight::data::real" value="6." />

    <service uid="..." type="sight::module::sample::service" >
        <properties enabled="false" value="${real_value}" />
    </service>

The data objects that can be passed inherits from the ``sight::data::string_serializable`` interface, which means
they can simply be interpreted to and from a string. Such data objects include:

- ``sight::data::boolean``
- ``sight::data::integer``
- ``sight::data::real``
- ``sight::data::string``
- ``sight::data::color``
- ``sight::data::ivec2``
- ``sight::data::ivec3``
- ``sight::data::ivec4``
- ``sight::data::dvec2``
- ``sight::data::dvec3``
- ``sight::data::dvec4``

Using properties thus gives the flexibility to pass either directly the value of the parameter or an object if its
content need to be shared with another services. This also brings the persistence of the parameter, by allowing you to
serialize the object or pass it to another XML configurations.

Last, to access the content of a property in the C++ code, one can use the classical accessors:

.. code-block:: c++

    double value = *m_value;
    double value = m_value.value();

This is actually a shortcut to the usual *lock data/get value* idiom of the ``sight::data::ptr``. If you ever need to protect
the access to the value or you want to set the value, you can still rely on it.

.. code-block:: c++

    {
        auto value = m_value.const_lock();
        function1(*value);
        function2(*value);
        ...
        functionN(*value);
    }
    {
        auto value = m_value.lock();
        *value = 42;
    }


Connections
---------------

When passing an object to a property, its content is always up-to-date when accessed as shown before. However,
you may need to know when the value change to call some specific code. Since a property is actually a data pointer,
it can be connected like any other data.

.. code-block:: c++

    service::base::connections_t sample::auto_connections() const
    {
        return { {m_value, data::object::MODIFIED_SIG, service::slots::UPDATE} };
    }

Working with many properties
--------------------------------

Things can become complicated when a service requires dozens of parameters shared across different configurations.

First it becomes tedious to pass all required objects to the configuration launchers. To simplify this, it is
possible to pass a map of objects to any service supporting properties, without additional
code. Simply declare a map of objects and pass it to the service as follows:

.. code-block:: xml

    <object uid="service_properties" type="sight::data::map">
        <item key="enabled" type="sight::data::boolean" value="false" />
        <item key="value" type="sight::data::real" value="0.6" />
    </object>

    <service uid="..." type="sight::module::sample::service">
        <properties from="service_properties" />
    </service>

Second, if the update of many parameters requires specific code, it can also be cumbersome to write a slot for each
one. To simplify this, a virtual function ``service::base::on_property_set(std::string_view)`` can be overridden in the
service:

.. code-block:: cpp

    void sample::on_property_set(std::string_view _key)
    {
        if(_key == "enabled")
        {
            this->impl->set_enable(m_enabled)
        }
        else if(_key == "value")
        {
            this->impl->set_value(value * 1000);
        }
        else
        {
            SIGHT_ASSERT("Unreachable code", false);
        }
    }

It should be noted that this mechanism is exclusive with the slot approach. The virtual function is only called for a
given key if there is no auto connection provided for this key.

Also, this virtual function is called whether you use individual properties or a map of properties in the configuration
of the service.

User interface
-----------------

To tweak services properties with the user interface, it is recommended to use the service
``sight::module::ui::qt::settings``. It supports either the use of individual objects or a map of objects:

.. code-block:: xml

    <service uid="..." type="sight::module::ui::qt::settings">
        <inout group="keys">
            <key uid="${enabled_value}" />
            <key uid="${real_value}" />
        </inout>
        <ui>
            <item name="Enabled" />
            <item name="Value" widget="slider" min="0." max="100." />
        </ui>
    </service>

.. code-block:: xml

    <service uid="..." type="sight::module::ui::qt::settings">
        <inout key="map" uid="${service_properties}" />
        <ui>
            <item name="Enabled" key="enabled" />
            <item name="Value" key="value" widget="slider" min="0." max="100." />
        </ui>
    </service>

In both cases, the service is connected with the objects signals and updates itself when values change from the outside.
The widgets also provide an optional reset button, enabled by default. When clicked, it restores the value to the one
defined in the declaration of the object.

To make the changes to the data persistent across different sessions, a preferences mechanism is present. To use it,
just add the ``src="preference"`` attribute to the declaration of the object.

.. code-block:: xml

    <object uid="real_value" type="sight::data::real" value="6." src="preference" />

    <object uid="service_properties" type="sight::data::map" src="preference">
        <item key="enabled" type="sight::data::boolean" value="false" />
        <item key="value" type="sight::data::real" value="0.6" />
    </object>

Please note that this feature is not supported for individual objects inside a map object, only the whole map can be
defined as a preference.
