Activities
==========

In *Sight*, an activity represents the user interface and the data needed for a piece of an application. For instance
in a navigation software, it corresponds to a step in the clinical workflow. It relies on a
:ref:`AppConfig<SADAppConfig>`, that will be launched with the appropriate data, and it implies the creation of a
``sight::data::ActivitySeries`` that holds all necessary data.

An activity is defined by implementing the extension ``sight::activity::extension::Activity``.

.. code-block:: xml

    <extension implements="sight::activity::extension::Activity">
       <id>myActivityId</id>
       <title>3D Visu</title>
       <desc>Activity description ...</desc>
       <icon>media_0-1/icons/icon-3D.png</icon>
       <requirements>
            <requirement name="image" type="sight::data::image" /> <!-- defaults : minOccurs = 1, maxOccurs = 1-->
            <requirement name="mesh1" type="sight::data::mesh" maxOccurs="3" >
                <key>Item1</key>
                <key>Item2</key>
                <key>Item3</key>
            </requirement>
            <requirement name="mesh2" type="sight::data::mesh" maxOccurs="*" container="vector" />
            <requirement name="imageSeries" type="sight::data::image_series" minOccurs="0" maxOccurs="2" />
            <requirement name="modelSeries" type="sight::data::model_series" minOccurs="1" maxOccurs="1">
                 <desc>Description of the required data....</desc>
                 <validator>sight::activity::validator::ImageProperties</validator>
            </requirement>
            <requirement name="transformationMatrix" type="sight::data::matrix4" minOccurs="0"
                         maxOccurs="1" create="true" />
           <!-- ...-->
       </requirements>
       <builder>sight::activity::builder::ActivitySeries</builder>
       <validator>sight::activity::validator::ImageProperties</validator>
       <appConfig id="myAppConfigId" />
    </extension>


The activity parameters are (in the following order):

- **id**: the activity unique identifier.
- **title**: the activity title that will be displayed on the tab.
- **desc**: the description of the activity. It is displayed by the service ``launcher``.
- **icon**: the path to the activity icon. It is displayed by the service ``launcher``.
- **requirements**: the list of the required data to launch an activity.

  - **requirement**: a required data.

    - name:
        Name of the parameter, as defined in the AppConfig.

    - type:
        The data type (ex: ``sight::data::image_series``).

    - minOccurs (optional, "1" by default):
        The minimum number of occurrences of this type of object in the vector.

    - maxOccurs (optional, "1" by default):
        The maximum number of occurrences of this type of object in the vector.

    - container (optional, "vector" or "composite", default: composite):
        Container used to contain the data if minOccurs or maxOccurs are not "1".
        If the container is "composite", you need to specify the "key" of each object in the composite.

    - create (optional, default "false"):
        If true and (minOccurs == 0 && maxOccurs == 1), the data will be automatically created if it is not present.

    - desc (optional):
        description of the parameter

    - validator (optional):
        validator to check if the associated data is well formed (inherited from ``sight::activity::IObjectValidator``)


- **builder**:
    **The builder is only used when the activity series is created from a selection of Series**. The default builder is
    ``sight::activity::builder::ActivitySeries`` : it creates the ``sight::data::ActivitySeries`` and adds the required
    data in its composite with the defined keys. The builder ``sight::activity::builder::ActivitySeriesInitData``
    allows, in addition, to create data when minOccurs == 0 and maxOccurs == 0.

- **validators** (optional):
    defines the list of validators. If you need only one validator, you don't need the "validators" tag (only "validator").

- **validator** (optional):
    allows to validate if the selected required objects are correct for the activity. For example, the validator
    ``sight::activity::validator::ImageProperties`` checks that all the selected images have the same size, spacing
    and origin.

- **appConfig**:

  - id:
    defines the AppConfig to launch and its parameters. For Qml activities, it represents the filename of the Qml file
    containing the activity. This file must be in the same module as the activity.

There are currently three ways to launch an activity.

From a selection of Series contained in a Vector
--------------------------------------------------

The service ``sight::module::ui::qt::launcher`` allows to launch an activity from a selection of Series.
Its role is to create the specific Activity associated with the selected data. The Series are contained in a
``sight::data::vector`` that can be filled by the user
when clicking on the Series selection widget (``sight::module::ui::qt::series::selector``)

This action should be followed by the service ``sight::module::ui::qt::activity::dynamic_view`` : this service listens the action
signals and launches the activity in a new tab.

From the activity wizard
---------------------------

The editor ``sight::module::ui::qt::activity::selector`` and the action ``sight::module::ui::qt::activity::launcher``
propose the list of available activities for the application. When the user selects an activity a signal is sent
with the activity identifier. The activity wizard (``sight::module::ui::qt::activity::wizard``) listens to this signal
and displays a widget to set the required data. The ``sight::data::ActivitySeries`` is then created and can be launched
by the ``sight::module::ui::qt::activity::dynamic_view``.

The process to create the activity with the different services works with signals and slots.

From the activity sequencer
--------------------------------

The service ``sight::module::ui::qt::activity::sequencer`` allows to define a list of activities that will be launched
sequentially. This service should be associated to ``sight::module::ui::qt::activity::view`` to display the current activity
in a container. Only one activity is launched at a time, the next activity will be available when all its requirements
are present.

A Qml implementation of the activity sequencer is also available in ``sight::module::ui::qml``. It proposes an activity 'stepper'
that displays the list of activities and allows the user to select any available activity.


Validators
------------

There are three types of validator :

Pre-build validator
********************

**This type of validator is only used when the activity series is created from a selection of Series**.
This type of validators checks if the current selection of data is correct to build the activity. It inherits of
sight::activity::IValidator and must implement the methods:

.. code-block:: cpp

    ValidationType validate(
           const sight::activity::extension::ActivityInfo& activityInfo,
           SPTR(sight::data::vector) currentSelection ) const;

Activity validator
*******************

This type of validator checks if the sight::data::ActivitySeries is correct to launch its associated activity.
It inherits of sight::activity::IActivityValidator and must implement the method:

.. code-block:: cpp

    ValidationType validate(const CSPTR(sight::data::ActivitySeries) &activity ) const;

The validator sight::activity::validator::DefaultActivity is applied if no other validator is defined. It checks if
all the required objects are present in the series and if all the parameters delivered to the AppConfig are present.

It provides some method useful to implement your own validator.

Object validator
****************

This type of validator checks if the required object is well formed. It can check a single object or a Vector or
a Composite containing one type of object. It inherits from ``sight::activity::IObjectValidator`` and must implement the
method:

.. code-block:: cpp

    ValidationType validate(const CSPTR(sight::data::object) &currentData ) const;


Activity launchers
-------------------

Different services are available to create/launch activities :

launcher
******************

This action allows to launch an activity according to the selected data.

.. figure:: ../media/SActivityLauncher.png
    :scale: 60
    :align: center


selector
*****************

The service ``sight::module::ui::qt::activity::selector`` displays the available activities according to the configuration.

When the activity is selected, the service sends a signal with the activity identifier. It can work in conjunction
with the ``sight::module::ui::qt::activity::wizard`` that creates or updates the activitySeries.

.. code-block:: xml

    <service uid="action_newActivity" type="sight::module::ui::qt::launcher">
        <!-- Filter mode 'include' allows all given activity id-s.
             Filter mode 'exclude' allows all activity id-s excepted given ones. -->
        <filter>
            <mode>include</mode>
            <id>2DVisualizationActivity</id>
            <id>3DVisualizationActivity</id>
            <id>VolumeRenderingActivity</id>
        </filter>
    </service>

filter (optional):
    it allows to filter the activity that can be proposed.

mode: 'include' or 'exclude':
    defines if the activity in the following list are proposed (include) or not (exclude).

id:
    id of the activity


wizard
*****************

This editor allows to select the data required by an activity in order to create the ``ActivitySeries``.
This editor displays a tab widget (one tab by data). It works on a ``sight::data::SeriesDB`` and adds the created activity
series into the seriesDB.

.. figure:: ../media/SActivityWizard.png
    :scale: 60
    :align: center

Example
********

To launch the activity, you will need to connect the services in you AppConfig:

.. code-block:: xml

    <extension implements="sight::app::extension::config">
        <id>myExample</id>
        <config>

            <object uid="seriesDB" type="sight::data::SeriesDB" />
            <!-- ... -- >

            <!-- Editor to select an activity. -->
            <service uid="activitySelector" type="sight::module::ui::qt::activity::selector" />

            <service uid="activityCreator" type="sight::module::ui::qt::activity::wizard" >
                <inout key="seriesDB" uid="seriesDB" />
                <ioSelectorConfig>SDBReaderIOSelectorConfig</ioSelectorConfig>
            </service>

            <service uid="dynamicView" type="sight::module::ui::qt::activity::dynamic_view" auto_connect="true">
                <mainActivity id="myMainActivity" closable="false" />
                <inout key="SERIESDB" uid="seriesDB" />
                <parameters>
                    <parameter replace="ICON_PATH" by="${appIconPath}" />
                </parameters>
            </service>

            <!-- Display the gui allowing to create a sight::data::ActivitySeries with the required data for
                 the selected activity. -->
            <connect>
                <signal>selector/activityIDSelected</signal>
                <slot>activityCreator/createActivity</slot>
            </connect>

            <!-- Launch the activity when it is created. -->
            <connect>
                signal>activityCreator/activityCreated</signal>
                <slot>dynamicView/launchActivity</slot>
            </connect>

        </config>
    </extension>


Activity sequencer
---------------------

The sequencer allows to define a list of the activities that will be launched sequentially.
This service should be associated to a view to display the current activity in a container. Only one
activity is launched at a time, the next activity will be available when all its requirements are present.

Three implementations exist for the sequencer:

- the "basic" sequencer with no interface, the slots 'next', 'previous' and 'goTo' allow to select the activity
  to launch
- the Qt implementation of the stepper (``sight::module::ui::qt::activity::sequencer``)
- the Qml implementation of the stepper (``sight::module::ui::qml::activity::sequencer``)

.. figure:: ../media/ActivitySequencer.png
    :scale: 60
    :align: center

    Activity stepper.

    The activity 'stepper' displays the list of activities and allows to select any available activity. And
    then launch the activity in the main container.


.. note::

    If you need to enable the next activity after a process in your activity, you must call the sequencer `checkNext`
    slot from your AppManager to check if the next activity is available. This slot can be call using the channel
    `validationChannel`.

Example for a XML based application
************************************

An XML configuration is available in ``activitiesConfig`` module.

The configuration can be launched by a 'config_controller':

.. code-block:: xml

    <service uid="activityLauncher" type="sight::app::config_controller">
        <appConfig id="ActivityLauncher" />
        <inout key="seriesDB" uid="mySeriesDB" />
        <parameter replace="WID_PARENT" by="activityView" />
        <parameter replace="ICON_PATH" by="${ICON_PATH}" />
        <parameter replace="ACTIVITY_READER_CONFIG" by="DefaultSessionReaderConfig" />
        <parameter replace="ACTIVITY_WRITER_CONFIG" by="ActivityWriterConfig" />
        <parameter replace="SEQUENCER_CONFIG" by="sequencerConfigName" />
    </service>

seriesDB:
    main seriesDB, it contains all the ActivitySeries launched by the sequencer. It is also used to load or
    save activities.

ACTIVITY_READER_CONFIG/ACTIVITY_WRITER_CONFIG (optional):
    configuration for activity reading/writing used by ``sight::module::io::session::reader`` and
    ``sight::module::io::session::writer``. By default it uses ``ActivityReaderConfig`` and ``ActivityWriterConfig``
    that load/save the activities with the `.apz` extension.

SEQUENCER_CONFIG
    represents the list of activities to launch, for example:

    .. code-block:: xml

        <extension implements="sight::service::registry::Config">
            <id>sequencerConfigName</id>
            <service>sight::module::ui::qt::activity::sequencer</service>
            <desc>Configuration for the sequencer</desc>
            <config>
                <activity id="activity1" name="my activity 1" />
                <activity id="activity2" name="my activity 2" />
                <activity id="activity3" name="my activity 3" />
            </config>
        </extension>

    - **id**: identifier of the activity
    - **name** (optional): name displayed in the activity stepper. If the name is not defined, the title of the
      activity will be used.

Example for Qml based application
**********************************

The Qml implementation of an activity launcher is available in ``sight::module::ui::qml``.
You can easily use the ``ActivityLauncher`` object in your Qml application to manage activities.

.. code-block:: qml

    ApplicationWindow {
        id: root
        width: 800
        height: 600
        visible: true

        ActivityLauncher {
            id: activityLauncher
            anchors.fill: parent
            activityIdsList: ["ExImageReadingActivity", "ExMesherActivity", "ExImageDisplayingActivity"]
            activityNameList: ["Read", "Mesher", "Display"]
        }

        onClosing: {
            activityLauncher.clear()
        }
    }

- **activityIdsList**: identifiers of the activities to launch
- **activityNameList**: name of the activities to launch, will be displayed in the stepper

For a Qml application, a qml file must be created in the same module as the activity definition, with the filename
described in ``appConfig.id`` attribute.

The main object should be an `Activity`. This object provides a template for the activity that will be launched, you
will need to define the associated AppManager.

.. code-block:: qml

    Activity {
        id: exImageDisplaying
        appManager: MesherManager {
            id: appManager
            frameBuffer: scene3D
        }

        // Your layout, object, service...
        // ...
        // the services should be register with 'exImageDisplayingActivity.registerService(service)'
        SliceSelector {
            id: sliceSelector

            // register the service when created
            onServiceCreated: {
                exImageDisplayingActivity.registerService(srv)
            }
        }
    }

