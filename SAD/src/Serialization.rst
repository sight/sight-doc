.. _SADSerialization:

Serialization
=======================

Overview
--------

Serialization is the process to save plain data structures from memory to hard drive. In Sight, ``io_session`` library
and ``sight::module::io::session::writer`` services provide tools to serialize all objects that inherit from
``sight::data::object`` to plain JSON [#]_ or, more common, in a standalone ZIP [#]_ archive. Even if they are still stored in
the main ZIP archive, or referenced in the JSON index, some data objects will be serialized as binary data in well known
format, when it is available. For example, by default, we use VTK [#]_ to store images and meshes, so an external third
party tool will be able to read them once extracted.

Of course, the opposite process, loading data from hard drive to memory data structures, is also available, using
``reader`` service and ``io_session`` library.

.. [#] Introducing JSON. http://json.org/
.. [#] Introducing ZIP. https://en.wikipedia.org/wiki/ZIP_(file_format)/
.. [#] Introducing VTK file format. https://kitware.github.io/vtk-examples/site/VTKFileFormats/


``sight::module::io::session::writer`` service
-----------------------------------------------

The purpose of this service is to serialize an input data object to hard drive. We can configure when and
how a file selection dialog appears, if the serialized data is protected by a password and when and how it is asked.
The output format can also be chosen, between plain JSON or compressed ZIP with *deflate* or *ZSTD* algorithm:

.. code-block:: xml

    <service type="sight::module::io::session::writer">
        <in key="data" uid="..." />
        <dialog extension=".sample" description="Sample Sight session file" policy="once"/>
        <password policy="once" encryption=salted/>
        <archive format="optimized"/>
    </service>

writer Parameters:
*******************

dialog (optional):
~~~~~~~~~~~~~~~~~~
- **extension**: defines the file extension that will be used for the session file. If the extension is not specified, default ".zip" will be used.
- **description**: allows to display a label in front of extension when the file dialog is shown.
- **policy**: defines when the file dialog will be shown:

  - **never**: never show the open dialog
  - **once**: show only once, store the location in memory as long as the service is started
  - **always**: always show the location dialog
  - **default**: default behavior, which is "never"

password (optional):
~~~~~~~~~~~~~~~~~~~~

- **policy**: defines if we should protect the session file using a password and when to ask for it:

  - **never**: a password will never be asked and the session file will never be encrypted.
  - **once**: a password will be asked only once and stored in the memory for subsequent uses. The session file will be encrypted.
  - **always**: a password will always be asked. The session file will be encrypted.
  - **default**: uses the builtin default behavior which is "never".

- **encryption**: defines password behavior:

  - **password**: uses password as is.
  - **salted**: uses password, but salts it. It means that encrypted file could not be open outside Sight.
  - **force**: force encryption, even without password. Use a pseudo-random hidden password, if no password are provided
  - **default**: uses the builtin default behavior which is "password".

archive (optional):
~~~~~~~~~~~~~~~~~~~

- **format**: defines the archive format.

  - **filesystem**: Use the filesystem to store files. It will output a JSON file and possibly binary files for "big" object like images.
  - **compatible**: Store files in a ZIP archive, with old deflate algorithm
  - **optimized**: Store files in a ZIP archive, with zstd algorithm
  - **default**: uses the builtin default behavior which is "optimized"


``sight::module::io::session::reader`` service
-----------------------------------------------

This service is the opposite of ``sight::module::io::session::writer`` service. It allows to load a session file from
hard drive and deserialize it to memory data structures. The input file can be encrypted and protected by a password.

.. code-block:: xml

    <service type="sight::module::io::session::reader">
        <inout key="data" uid="..." />
        <dialog extension=".sample" description="Sample Sight session file" policy="always"/>
        <password policy="once encryption=salted"/>
        <archive format="default"/>
    </service>

reader Parameters:
*******************

``dialog`` and ``password`` use the same parameters with the same meaning as for ``sight::module::io::session::writer``
service. However, ``archive`` is slightly different, because there is no need to specify the compression algorithm, just
if we use the filesystem or a ZIP archive.

archive (optional):
~~~~~~~~~~~~~~~~~~~

- **format**: defines the archive format.

  - **filesystem**: Reads files from the filesystem.
  - **archive**: Reads files from an session archive.
  - **default**: uses the builtin default behavior which is "archive"


Customizing serialization
-------------------------

All data objects defined in Sight can be serialized with no additional work, as everything is done by Sight with the
help of ``io_session`` library. However, for user defined data objects, or if the default serialization done by Sight
doesn't match your needs, it is necessary to implement dedicated serialization functions. The process is simple, there
are only two functions to implement, one for serialization and one for deserialization, and to register them.

serializer_t and deserializer_t
*******************************

The serialization functions must be compliant with types ``io::session::serializer_t`` and
``io::session::deserializer_t``

.. code-block:: cpp

    using serializer_t = std::function<void (
        zip::ArchiveWriter& archive,
        boost::property_tree::ptree& tree,
        data::object::csptr object,
        std::map<std::string, data::object::csptr>& children,
        const core::crypto::secure_string& password
    )>;

    using deserializer_t = std::function<data::object::sptr(
        zip::ArchiveReader& archive,
        const boost::property_tree::ptree& tree,
        const std::map<std::string, data::object::sptr>& children,
        data::object::sptr object,
        const core::crypto::secure_string& password
    )>;

archive
~~~~~~~

Reference to the archive reader/writer object. It may be used to store large binary data like images or meshes.

tree
~~~~

Reference to the property tree object. It is where the data is really stored and later, converted to JSON file.

object
~~~~~~

Const pointer to the object to serialize.

children
~~~~~~~~

Map of children objects to serialize. It is the responsibility of the user to choose the same keys for the serializer
and deserializer to fill this map.

password
~~~~~~~~

The password as a ``core::crypto::secure_string``, which is a ``std::string`` variant. It's main purpose is to
automatically clean the memory when it is unallocated.


Simple example for ``sight::data::string`` object
*************************************************

.. code-block:: cpp

    static constexpr auto CUSTOM_KEY = "custom key";

    // Serialization function
    void customSerialize(
        zip::ArchiveWriter&,
        boost::property_tree::ptree& tree,
        data::object::csptr object,
        std::map<std::string, data::object::csptr>&,
        const core::crypto::secure_string&
    )
    {
        // Cast to the right type (throws exception if type is incorrect)
        const auto string = sight::io::session::Helper::safeCast<data::string>(object);

        // Save the value
        tree.put(CUSTOM_KEY, string->getValue());
    }

    // Deserialization function
    data::string::sptr customDeserialize(
        zip::ArchiveReader&,
        const boost::property_tree::ptree& tree,
        const std::map<std::string, data::object::sptr>&,
        data::object::sptr object,
        const core::crypto::secure_string&
    )
    {
        // Create or reuse the object (throws exception if type is incorrect)
        auto string = sight::io::session::Helper::safeCast<data::string>(object);

        // Restore the value
        string->setValue(tree.get<std::string>(CUSTOM_KEY));

        return string;
    }


Versioning
**********

We choose to follow KISS methodology for versioning. Actually, the version management is left to the user discretion,
but we provide some basic helper functions ``sight::io::session::Helper::writeVersion()`` and
``sight::io::session::Helper::readVersion()`` to make it easier. The previous example can updated by adding this two
lines:

.. code-block:: cpp

    void customSerialize(...)
    {
        ...

        // Add a version number. Not mandatory, but could help for future release
        sight::io::session::Helper::writeVersion<data::string>(tree, 6);

        ...
    }

    data::string::sptr customDeserialize(...)
    {
        ...

        try
        {
            // Check version number (throws an exception if version < 4 or > 6)
            const int version = sight::io::session::Helper::readVersion<data::string>(tree, 4, 6);
            if(version > 2)
            {
                // Do something
            }
            else if(version > 5)
            {
                // Do something else
            }
            else
            {
                // Do something else
            }
            ...
        }
        catch (const std::exception& e)
        {
            // Handle the exception
        }

        ...
    }

It is still required to write specific code to handle difference between version, but everything should be managed in
the same function.

**note:** The helper functions are templated to make the version specific to the type. This is done to avoid collision
with other serializer/deserializer functions.


Registering serializer
**********************

We also need to register them, so ``io_session`` library could call them. you have to call
``sight::io::session::SessionWriter::setDefaultSerializer()`` and
``sight::io::session::SessionWriter::setDefaultDeserializer()``, with the appropriate serializer/deserializer functions.
Of course, the registration need to be done **before** using serialization. A good place could be in the plugin
``start()`` \ ``stop()`` code from the module.

.. code-block:: cpp

    void plugin::start()
    {
        // Register custom serializers (default one will be overridden if there are defaults for those classes)
        sight::io::session::SessionWriter::setDefaultSerializer(data::string::classname(), customSerialize);
        sight::io::session::SessionReader::setDefaultDeserializer(data::string::classname(), customDeserialize);
    }

    //------------------------------------------------------------------------------

    void plugin::stop()
    {
        // Unregister custom serializers (default ones will be used back if there are defaults for those classes)
        sight::io::session::SessionWriter::setDefaultSerializer(data::string::classname());
        sight::io::session::SessionReader::setDefaultDeserializer(data::string::classname());
    }

**note:** It is also possible to set custom serializers in a specific ``SessionWriter`` \ ``SessionReader`` and keep
default behaviors unchanged for other instances, by calling ``sight::io::session::SessionWriter::setSerializer()`` or
``sight::io::session::SessionReader::setDeserializer()``.


Saving binary data with encryption
**********************************

This example demonstrate the use of a third party tool (VTK) to save the data and how to store it, possibly encrypted,
in the session archive.

.. code-block:: cpp

    void serialize(
        zip::ArchiveWriter& archive,
        boost::property_tree::ptree& tree,
        data::object::csptr object,
        std::map<std::string, data::object::csptr>&,
        const core::crypto::secure_string& password
    )
    {
        const auto image = Helper::safeCast<data::image>(object);

        // Add a version number. Not mandatory, but could help for future release
        Helper::writeVersion<data::image>(tree, 1);

        // Convert the image to VTK
        const auto& vtkImage = vtkSmartPointer<vtkImageData>::make();
        io::vtk::toVTKImage(image, vtkImage);

        // Create the vtk writer
        const auto& vtkWriter = vtkSmartPointer<vtkXMLImageDataWriter>::make();
        vtkWriter->SetCompressorTypeToNone();
        vtkWriter->SetDataModeToBinary();
        vtkWriter->WriteToOutputStringOn();
        vtkWriter->set_inputData(vtkImage);

        // Write to internal string...
        vtkWriter->Update();

        // Create the output file inside the archive
        const auto& ostream = archive.openFile(
            std::filesystem::path(image->getUUID() + s_image),
            password
        );

        // Write back to the archive
        (*ostream) << vtkWriter->GetOutputString();
    }

    //------------------------------------------------------------------------------

    data::image::sptr deserialize(
        zip::ArchiveReader& archive,
        const boost::property_tree::ptree& tree,
        const std::map<std::string, data::object::sptr>&,
        data::object::sptr object,
        const core::crypto::secure_string& password
    )
    {
        // Create or reuse the object
        auto image = Helper::safeCast<data::image>(object);

        // Check version number. Not mandatory, but could help for future release
        Helper::readVersion<data::image>(tree, 0, 1);

        // Create the istream from the input file inside the archive
        const auto& uuid    = tree.get<std::string>(s_uuid);
        const auto& istream = archive.openFile(
            std::filesystem::path(uuid + s_image),
            password
        );

        // "Convert" it to a string
        const std::string content {std::istreambuf_iterator<char>(*istream), std::istreambuf_iterator<char>()};

        // Create the vtk reader
        const auto& vtk_reader = vtkSmartPointer<vtkXMLImageDataReader>::make();
        vtk_reader->ReadFromInputStringOn();
        vtk_reader->set_inputString(content);
        vtk_reader->Update();

        // Convert from VTK
        io::vtk::fromVTKImage(vtk_reader->GetOutput(), image);

        return image;
    }


