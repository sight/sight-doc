.. _SADGUI:

Graphical User Interface
========================

Overview
--------

Graphical User Interface (GUI) is the process of displaying the graphical
components of an application. In Sight, the ``sight::ui`` library provides abstract
tools to display components like windows, buttons, textfield, aso.

The software architecture provides a way of selecting different backends in order to manage the GUI components.
As a result, the ``sight::ui::qt`` library has been created to display components created using the Qt soup.
Presently, this backend is the only one supported by the applications.


Backend
-------

When creating an application, we need to specify which gui backend we want to use. To do so,
the chosen gui module must be activated and started in the profile.xml of the application. The
main gui module for any application is ``sight::module::ui``. The ``sight::module::ui`` module must be activated
regardless of the chosen backend.

.. code-block:: xml

    <activate id="sight::module::ui" version="0-1" />
    <activate id="sight::module::ui::qt" version="0-1" />

    <!-- ... -->

    <start id="sight::module::ui::qt" />

**Warning :** The gui backend module must be started before any other module in the profile.xml.


Configuration
-------------

Frames
~~~~~~

The frame is the main component of a GUI.
The main service used to represent a general frame is ``sight::ui::frame``.
The service ``sight::module::ui::frame`` is the main application frame.

The ``sight::module::ui::frame`` service is configurable with different parameters :

* Application name
* Application icon
* Minimum window size
* GUI elements (toolbar, menubar, aso.)


.. code-block:: xml

    <service uid="mainFrame" type="sight::module::ui::frame">
        <gui>
            <frame>
                <name>Application name</name>
                <icon>path_to_application_icon</icon>
                <minSize width="800" height="600"/>
            </frame>
            <menuBar />
            <toolBar >
                <toolBitmapSize height= "32" width="32" />
            </toolBar>
        </gui>
        <registry>
            <menuBar sid="menuBar" start="true" />
            <toolBar sid="toolBar" start="true" />
            <view sid="view" start="true" />
        </registry>
    </service>


Menus and actions
~~~~~~~~~~~~~~~~~

The menu bar is used to organize application action groups.
The main service used to display that kind of bar is ``sight::ui::menubar``.
The service ``sight::module::ui::menubar`` is the default implementation.
Every backend must provide its own implementation of this service.

The configuration is used to associate a menu label with the service representing the menu.

.. code-block:: xml

    <service uid="menuBar" type="sight::module::ui::menubar">
        <gui>
            <layout>
                <menu name="First Menu"/>
                <menu name="Second Menu"/>
            </layout>
        </gui>
        <registry>
            <menu sid="firstMenu" start="true" />
            <menu sid="secondMenu" start="true" />
        </registry>
    </service>


The main service used to display a menu is ``sight::ui::menu``.
The service ``sight::module::ui::menu`` is the default implementation.
Every backend must provide its own implementation of this service.

The configuration is used to associate an action name and the service performing the action.
An action can be configured with a shortcut, a style (default, check, radio) and/or an icon.
Several special actions can also be specified (QUIT, ABOUT, aso.).

.. code-block:: xml

    <service uid="myMenu" type="sight::module::ui::menu">
        <gui>
            <layout>
                <menuItem name="First Item" icon="icon_path" />
                <menuItem name="Checked Item" style="check" />
                <separator />
                <menuItem name="Quit" shortcut="Ctrl+Q" specialAction="QUIT" />
            </layout>
        </gui>
        <registry>
            <menuItem sid="actionFirstItem" start="false" />
            <menuItem sid="actionCheckedItem" start="false" />
            <menuItem sid="actionQuit" start="false" />
        </registry>
    </service>


A menu can also be displayed using a tool bar.
The main service used to display a tool bar is ``sight::ui::toolbar``.
The service ``sight::module::ui::toolbar`` is the default implementation.
Every backend must provide its own implementation of this service.

The configuration of a tool bar is the same as the one used to describe a menu.


Layouts
~~~~~~~

The layouts are used to organize the different parts of a GUI.
The main service used to manage layouts is ``sight::ui::view``.
The service ``sight::module::ui::view`` is the default implementation.
Every backend must provide its own implementation of this service.

Several types of layout can be used :

* Line layout
* Cardinal layout
* Tab layout


Every layout can be configured with a set of parameters (orientation, alignment, aso.).

.. code-block:: xml

    <service uid="subView" type="sight::module::ui::view">
        <gui>
            <layout type="sight:ui::layout::line" >
                <orientation value="horizontal" />
                <view caption="view1" />
                <view caption="view2" />
            </layout>
        </gui>
        <registry>
            <view sid="subView1" start="true" />
            <view sid="subView2" start="true" />
        </registry>
    </service>


Multi-threading
---------------

The ``sight::ui`` library has been designed to support multi-thread application.
When a GUI component needs to be accessed,
the function call must be encapsulated in a lambda declaration as shown in this example:

.. code-block:: cpp

        sight::service::registry::ActiveWorkers::getDefaultWorker()->postTask<void>(
        [&] {
                //TODO Write function calls
        }
        ).wait();

This encapsulation is required because all access to GUI components must be performed in the thread containing the GUI.
It moves the function calls from the current thread, to the GUI thread.
