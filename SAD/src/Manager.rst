Manager and updater services
==================================

In the Sight architecture, there are container objects like ``sight::data::map``, ``sight::data::vector`` and
``sight::data::series``. The ``sight::data::map`` is also an Object and represents a map
which associates a string with an Object.
The architecture provides a service to manage these objects ``sight::module::data::manage``.

We also need services to extract the sub-objects from the containers
and add the object in the application configuration (AppConfig)

``sight::module::data::manage``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This service manages an object (add/swap/remove) into a container object (composite, vector, series) or into any
object's fields.

Available operations on composite are:

- Adding an object
- Swapping an object
- Removing an object
- Removing an object if present
- Adding or swapping an object

.. code-block:: xml

    <object uid="composite" type="sight::data::map" />
    <object uid="image" type="sight::data::image" />

    <service uid="manager" type="sight::module::data::manage">
        <inout key="object" uid="image" />
        <inout key="composite" uid="composite" />
        <compositeKey>myImage</compositeKey>
    </service>

    <!-- Add the image in the composite when it is modified -->
    <connect>
        <signal>image/modified</signal>
        <slot>manager/addOrSwap</slot>
    </connect>

    <start uid="manager" />

``module::core::data::SGetImage``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Extracts a ``sight::data::image`` from a ``sight::data::image_series``

.. code-block:: xml

    <service uid="..." type="sight::module::data::SGetImage" >
        <inout key="imageSeries" uid="..." />
        <out key="image" uid="..." />
    </service>


``module::core::data::get_series``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Extracts a ``sight::data::series`` from a ``sight::data::series``

.. code-block:: xml

    <service uid="..." type="sight::module::data::get_series" >
        <inout key="seriesDB" uid="..."/>
        <out group="series" >
            <key index="4" uid="..." />
            <key index="2" uid="..." />
        </out>
    </service>


``module::core::data::get_camera``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Extracts ``sight::data::camera`` / ``sight::data::matrix4`` from a ``sight::data::camera_series``

.. code-block:: xml

    <service uid="..." type="sight::module::data::get_camera" >
        <inout key="camera_series" uid="..."/>
        <out group="camera" >
            <key index="0" uid="..." />
            <key index="1" uid="..." />
        </out>
        <out group="extrinsic" >
            <key index="1" uid="..." />
        </out>
    </service>



``module::core::data::get_mesh``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Extracts ``sight::data::mesh`` from a ``sight::data::model_series``

.. code-block:: xml

    <service uid="..." type="sight::module::data::get_mesh" >
        <inout key="modelSeries" uid="..." />
        <out group="mesh">
            <key index="3" uid="..."/>
            <key type="organ1" uid="..."/>
            <key type="organ2" matching="(.*)surface(.*)" uid="..."/>
        </out>
    </service>

