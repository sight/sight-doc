.. _SADBufferObjects:

Objects containing buffers
===========================

In ``sight``, we manage different type of large buffers, as in meshes and images. As the image can be of different types
(signed or not, integers/floating), we use a generic class ``sight::data::array`` that stores a buffer as a ``void*`` and
provides methods to access the buffer.

Mesh and Image use arrays to store their information (buffer, points, normals), but they are not accessible, so we
provide an API to access the data.

Array
-------

The array can be multi-dimensional, the number of dimensions is defined by the number of elements in the size vector.
It will perform the allocation, reallocation, destruction of the buffer.
The array contains a buffer stored as a ``void*``, its type is defined by ``sight::core::tools::Type`` that provides the basic
types ([u]int8, [u]int16, [u]int32, [u]int64, float and double).

Usage
*******

Allocation
~~~~~~~~~~~~~

The array buffer is allocated using the ``resize()`` methods. You can get the allocated size using ``getSizeInBytes()``.

.. warning::

    The allocated size can be different from the array size: it can happen if you called ``resize(..., false)``. It may
    be useful when you don't want to reallocate the image too often, but you need to be sure to allocate enough memory.

To resize the array, you must define the Type ([u]int[8|16|32|64], double, float) and the size of the buffer. You can
use ```setType(type)`` and ``resize(size)`` or directly call ``resize(size, type)``.

Buffer access
~~~~~~~~~~~~~~~

You can access buffer values using ``at<type>(const size_t& offset)`` or ``at<type>({x, y, z})`` methods.

.. warning::

    These methods may be slow if you use it intensively (a cast of the buffer is performed each time) and should
    not be used to parse the entire buffer (it is better to use iterators).

You can also retrieve the entire buffer  with ``getBuffer()`` but be careful no check will be done when you use it.

.. warning::
    The array must be locked for dump before accessing the buffer. It prevents the buffer to be dumped on the disk.

**Example:**

.. code-block:: cpp

    // 2D array of std::int16_t

    // prevent the buffer to be dumped on the disk
    const auto dumpLock = array->lock();

    // retrieve the value at index (x, y)
    value = array->at<std::int16_t>({x, y});

    // or you can compute the index like
    const auto size = array->getSize();
    const size_t index = x + y*size[0];
    value = array->at<std::int16_t>(index);


Iterators
~~~~~~~~~~~

To parse the buffer from beginning to end, the proper way is to use iterators. The iterator check (in debug) that you
don't iterate outside of the buffer.

The iteration depends on the given format. The format can be the buffer type ([u]int[8|16|32|64], double, float), but
can also be a simple struct like:

.. code-block:: cpp

    struct Color {
        std::uint8_t r;
        std::uint8_t g;
        std::uint8_t b;
        std::uint8_t a;
    };

This struct allows to parse the array as an RGBA buffer (RGBARGBARGBA....).

To get an iterator on the array, use ``begin<FORMAT>()`` and ``end<FORMAT>()`` methods.

.. warning::

    The iterator does not assert that the array type is the same as the given format. It only asserts (in debug) that
    the iterator does not iterate outside of the buffer bounds).

Example
********

.. code-block:: cpp

    const std::int16_t value = 19;
    auto array = std::make_shared<sight::data::array>();
    array->resize({1920, 1080}, sight::core::tools::Type::s_INT16);

    auto iter          = array->begin<std::int16_t>();
    const auto iterEnd = array->end<std::int16_t>();

    for (; iter != iterEnd; ++iter)
    {
        *iter = value;
    }

.. tip::

    If you need to know (x, y) indices, you can parse the array looping from the last dimension to the first, like:

    .. code-block:: cpp

        auto iter = array->begin<std::int16_t>();

        const auto size = array->getSize();
        for (size_t y=0 ; y<size[1] ; ++y)
        {
            for (size_t x=0 ; x<size[0] ; ++x)
            {
                // do something with x and y ....

                // retrieve the value
                value = *iter;

                // increment iterator
                ++iter;
            }
        }


Image
-------

An image contains a buffer (stored in an Array) and is defined by some parameters (size, spacing, pixel type, ...)
The buffer type is defined by ``sight::core::tools::Type`` that provides the basic types ([u]int8, [u]int16, [u]int32, [u]int64,
float and double).

The image size is a 3D size_t array but the third dimension can be 0 for a 2D image.
The image ``PixelFormat`` represents the buffer organization in components (GRAY_SCALE: 1 component, RGB and BGR: 3
components, RGBA and BGRA: 4 components).

Usage
********

Allocation
~~~~~~~~~~~~

The image buffer is allocated using the ``resize()`` methods. You can get the allocated size using ``getSizeInBytes()``
and ``getAllocatedSizeInBytes()``.

.. warning::

    The allocated size can be different from the image size: it can happen if you called ``setSize()`` without calling
    ``resize()``. It may be useful when you don't want to reallocate the image too often, but you need to be sure to
    allocate enough memory.

To resize the image, you must define the Type ([u]int[8|16|32|64], double, float), the size and the pixel
format of the buffer. You can use ``setSize(size)``, ``setType(type)`` and  ``setPixelFormalt(format)`` or directly call
``resize(size, type, format)``.

Buffer access
~~~~~~~~~~~~~~~

You can access voxel values using ``at<type>(IndexType id)`` or ``at<type>(IndexType x, IndexType y, IndexType z)``
methods.

.. warning::
    These methods may be slow if you use it intensively (a cast of the buffer is performed each time) and should not be used
    to parse the entire buffer (it is better to use iterators).

You can also use ``getPixelAsString()`` to retrieve the value as a string (useful for displaying information).

.. warning::

    The image must be locked for dump before accessing the buffer. It prevents the buffer to be dumped on the disk.

**Example:**

.. code-block:: cpp

    // 3D image of std::int16_t

    // prevent the buffer to be dumped on the disk
    const auto dumpLock = image->lock();

    // retrieve the value at index (x, y, z)
    value = image->at<std::int16_t>(x, y, z);

    // or you can compute the index like
    const auto size = image->getSize2();
    const size_t index = x + y*size[0] + z*size[0]*size[1];
    value = image->at<std::int16_t>(index);

Iterators
~~~~~~~~~~

To parse the buffer from beginning to end, the proper way is to use iterators. The iterator checks (in debug) that you
don't iterate outside of the buffer.

The iteration depends on the given format. The format can be the buffer type ([u]int[8|16|32|64], double, float), but
can also be a simple struct like:

.. code-block:: cpp

    struct Color {
        std::uint8_t r;
        std::uint8_t g;
        std::uint8_t b;
        std::uint8_t a;
    };

This struct allows to parse the image as an RGBA buffer (RGBARGBARGBA....).

To get an iterator on the image, use ``begin<FORMAT>()`` and ``end<FORMAT>()`` methods.

.. warning::

    The iterator does not assert that the image type is the same as the given format. It only asserts (in debug) that
    the iterator does not iterate outside of the buffer bounds).


Example
********

.. code-block:: cpp

    auto img = std::make_shared<sight::data::image>();
    img->resize(1920, 1080, 5, sight::core::tools::Type::s_UINT8, sight::data::image::PixelFormat::RGBA);
    auto iter    = img->begin<color>();
    const auto iterEnd = img->end<color>();
    for (; iter != iterEnd; ++iter)
    {
        iter->r = val1;
        iter->g = val2;
        iter->b = val2;
        iter->a = val4;
    }

.. tip::

    If you need to know (x, y, z) indices, you can parse the array looping from the last dimension to the first, like:

    .. code-block:: cpp

        const auto size = image->getSize2();

        auto iter    = image->begin<color>();

        for (size_t z=0 ; z<size[2] ; ++z)
        {
            for (size_t y=0 ; y<size[1] ; ++y)
            {
                for (size_t x=0 ; x<size[0] ; ++x)
                {
                    // do something with x and y ....

                    // retrieve the value
                    val1 = iter->r;
                    val2 = iter->g;
                    val3 = iter->b;
                    val4 = iter->a;

                    // increment iterator
                    ++iter;
                }
            }
        }


Mesh
-------

The ``sight::data::mesh`` represents a geometric structure composed of points, lines, triangles, quads or polygons.

Structure
***********

The mesh structure contains some information stored in ``sight::data::array``:

m_points
    Contains point coordinates (x,y,z)

m_cells
    Contains point indexes in m_points used to create cells: 3 indexes are necessary to create a triangle cell, 4 for
    a quad cell.

And some additional arrays to store the mesh attributes (normals, texture coordinates and colors for points and
cells).

Example
~~~~~~~~

- m_num_points = number of mesh points  * 3
- m_points = [ x0, y0, z0, x1, y1, z1, x2, y2, z2, x3, y3, z3, ... ]
- m_num_cells = number of mesh cells
- m_cell_type_ts.size = m_num_cells
- m_cells = [0, 1, 2, 0, 1, 3, 0, 1, 3, 5... ] ( correspond to point id )

Gets the points coordinates of the third cell:

.. code-block::

    m_cell_type_ts[2] => cell type = quad
    index of p1 = m_cells[6] = 0
    index of p2 = m_cells[6+1] = 1
    index of p3 = m_cells[6+2] = 3
    index of p4 = m_cells[6+3] = 5
    p1 = [ x0=m_points[0]  y0 z0 ] ( 0 * 3 = 0 )
    p2 = [ x1=m_points[3]  y1 z1 ] ( 1 * 3 = 3 )
    p3 = [ x3=m_points[9]  y3 z3 ] ( 3 * 3 = 9 )
    p4 = [ x5=m_points[15] y5 z5 ] ( 5 * 3 = 15 )

There are other arrays to stock normal by points, normal by edges, color by points or color by cells:

- Normal arrays contains normal vector (x,y,z)
- normals.size = number of mesh points (respectively cells)
- normals = [ x0, y0, z0, x1, y1, z1, x2, y2, z2, x3, y3, z3, ... ]
- Color arrays contains RGBA colors
- colors.size = number of mesh points (respectively cells) * 4
- colors = [ r0, g0, b0, a0, r1, g1, b1, a1, ... ]

Usage
******

Allocation
~~~~~~~~~~~~

The two methods ``reserve(...)`` and ``resize(...)`` allocate the mesh arrays. The difference between the two methods is
that ``resize(...)`` modifies the number of points and cells.

The ``push_point()`` and ``push_cell()`` methods add new points or cells, they increment the number of points/cells and
allocate more memory if needed. It is recommended to call ``reserve()`` method before if you know the number of
points and cells, it avoids allocating more memory than needed.
You can call ``shrink_to_fit()`` to reduce the allocated memory to the real number of points and cells.

The ``set_point()`` and ``set_cell()`` methods change the value of a point/cell at a given index.

**Example with resize(), set_point() and set_cell():**

.. code-block:: cpp

   auto mesh = std::make_shared<sight::data::mesh>();

   mesh->resize(NB_POINTS, NB_CELLS, CELL_TYPE, EXTRA_ARRAY);
   const auto lock = mesh->lock(); // prevents the buffers from being dumped on the disk

   for (size_t i = 0; i < NB_POINTS; ++i)
   {
       const std::uint8_t val                                   = static_cast<uint8_t>(i);
       const sight::data::mesh::color_value_t color[4]          = {val, val, val, val};
       const float float_val                                    = static_cast<float>(i);
       const sight::data::mesh::normal_value_t normal[3]        = {float_val, float_val, float_val};
       const sight::data::mesh::tex_coord_value_t tex_coords[2] = {float_val, float_val};
       const size_t value                                       = 3*i;
       mesh->set_point(i, static_cast<float>(value), static_cast<float>(value+1), static_cast<float>(value+2));
       mesh->set_point_color(i, color);
       mesh->set_point_normal(i, normal);
       mesh->set_point_tex_coord(i, tex_coords);
   }

   for (size_t i = 0; i < NB_CELLS; ++i)
   {
       mesh->set_cell(i, i, i+1, i+2);

       const sight::data::mesh::color_value_t val             = static_cast<sight::data::mesh::color_value_t >(i);
       const sight::data::mesh::color_value_t color[4]        = {val, val, val, val};
       const float float_val                                    = static_cast<float>(i);
       const sight::data::mesh::normal_value_t normal[3]      = {float_val, float_val, float_val};
       const sight::data::mesh::tex_coord_value_t tex_coords[2] = {float_val, float_val};
       mesh->set_cell_color(i, color);
       mesh->set_cell_normal(i, normal);
       mesh->set_cell_tex_coord(i, tex_coords);
   }


**Example with reseve(), push_point() and push_cell():**

.. code-block:: cpp

   auto mesh = std::make_shared<sight::data::mesh>();

   mesh->reserve(NB_POINTS, NB_CELLS, CELL_TYPE, EXTRA_ARRAY);
   const auto lock = mesh->lock(); // prevents the buffers from being dumped on the disk

   for (size_t i = 0; i < NB_POINTS; ++i)
   {
       const std::uint8_t val                                   = static_cast<uint8_t>(i);
       const sight::data::mesh::color_value_t color[4]          = {val, val, val, val};
       const float float_val                                    = static_cast<float>(i);
       const sight::data::mesh::normal_value_t normal[3]        = {float_val, float_val, float_val};
       const sight::data::mesh::tex_coord_value_t tex_coords[2] = {float_val, float_val};
       const size_t value                                   = 3*i;
       const auto id = mesh->push_point(static_cast<float>(value), static_cast<float>(value+1), static_cast<float>(value+2));
       mesh->set_point_color(id, color);
       mesh->set_point_normal(id, normal);
       mesh->set_point_tex_coord(id, tex_coords);
   }

   for (size_t i = 0; i < NB_CELLS; ++i)
   {
       const auto id = mesh->push_cell(i, i+1, i+2);

       const sight::data::mesh::color_value_t val               = static_cast<sight::data::mesh::color_value_t >(i);
       const sight::data::mesh::color_value_t color[4]          = {val, val, val, val};
       const float float_val                                    = static_cast<float>(i);
       const sight::data::mesh::normal_value_t normal[3]        = {float_val, float_val, float_val};
       const sight::data::mesh::tex_coord_value_t tex_coords[2] = {float_val, float_val};
       mesh->set_cell_color(id, color);
       mesh->set_cell_normal(id, normal);
       mesh->set_cell_tex_coord(id, tex_coords);
   }

.. warning::

    The mesh must be locked for dump before accessing the points or cells. It prevents the arrays to be dumped on the
    disk.

Iterators
~~~~~~~~~~

To access the mesh points and cells, you should use the following iterators:

 - ``sight::data::iterator::point_iterator``: to iterate through mesh points
 - ``sight::data::iterator::const_point_iterator``: to iterate through mesh points read-only
 - ``sight::data::iterator::cell_iterator``: to iterate through mesh cells
 - ``sight::data::iterator::const_cell_iterator``: to iterate through mesh cells read-only

**Example to iterate through points:**

.. code-block:: cpp

   auto mesh = std::make_shared<sight::data::mesh>();
   mesh->resize(25, 33, sight::data::mesh::cell_type_t::triangle);
   auto iter    = mesh->begin<sight::data::iterator::point_iterator >();
   const auto iterEnd = mesh->end<sight::data::iterator::point_iterator >();
   float p[3] = {12.f, 16.f, 18.f};

  for (; iter != iterEnd; ++iter)
  {
      iter->point->x = p[0];
      iter->point->y = p[1];
      iter->point->z = p[2];
  }


**Example to iterate through cells:**

.. code-block:: cpp

   auto mesh = std::make_shared<sight::data::mesh>();
   mesh->resize(25, 33, sight::data::mesh::cell_type_t::triangle);
   auto iter         = mesh->begin<sight::data::iterator::const_cell_iterator >();
   const auto endItr = mesh->end<sight::data::iterator::const_cell_iterator >();

   auto itrPt = mesh->begin<sight::data::iterator::const_point_iterator >();
   float p[3];

   for(; iter != endItr; ++iter)
   {
       const auto nbPoints = iter->nbPoints;

       for(size_t i = 0 ; i < nbPoints ; ++i)
       {
           auto pIdx = static_cast<sight::data::iterator::const_cell_iterator::difference_type >(iter->pointIdx[i]);

           sight::data::iterator::const_point_iterator pointItr(itrPt + pIdx);
           p[0] = pointItr->point->x;
           p[1] = pointItr->point->y;
           p[2] = pointItr->point->z;
       }
   }


``push_cell()`` and ``set_cell()`` may not be very efficient, you can use cell_iterator to define the cell. But be careful
to properly define all the cell attributes.

**Example of defining cells using iterators:**

.. code-block:: cpp

   auto mesh = std::make_shared<sight::data::mesh>();
   mesh->resize(25, 33, sight::data::mesh::cell_type_t::quad);
   auto it          = mesh->begin<sight::data::iterator::cell_iterator >();
   const auto itEnd = mesh->end<sight::data::iterator::cell_iterator >();

   size_t count = 0;
   for (; it != itEnd; ++it)
   {
       // define the point indices
       for (size_t i = 0; i < 4; ++i)
       {
           sight::data::mesh::cell_value_type pt_idx = val;
           it->pointIdx[i] = pt_idx;
       }
   }
