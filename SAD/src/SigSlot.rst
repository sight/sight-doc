.. _SADSigSlot:

Signal-slot communication
=========================

Overview
--------

"Signals and slots" is a language construct introduced in Qt [#]_
for communication between objects.

.. [#] http://wiki.qt.io/Qt_signal-slot_quick_start

The concept is that objects and services(explained in 2.3) can send signals containing event information which can be
received by other services using special functions known as slots.


Sight implementation
---------------------

In the Sight architecture, the library ``sight::core::com`` provides a set of tools
dedicated to communication. These communications are based on the signal and
slot concept.

``sight::core::com`` provides the following features :

-  function and method wrapping
-  direct slot calling
-  asynchronous slot calling
-  ability to work with multiple threads
-  auto-disconnection of slot and signals
-  arguments loss between slots and signals

Slots
-----

Slots are wrappers for functions and class methods that can be attached
to a ``sight::core::thread::worker``. The purpose of this class is to provide
synchronous and asynchronous mechanisms for method and function calling.

Slots have a common base class : SlotBase. This allows the storage of them in
the same container. Slots are designed such that they can be called, even where only the argument type is known.

Examples :

A slot wrapping the function sum, which is a function with
the signature int (int, int) :

.. code-block:: c++

    sight::core::com::slot< int (int, int) >::sptr slotSum = sight::core::com::new_slot( &sum );

A slot wrapping the function start with signature void() of
the object ``a`` which class type is ``A`` :

.. code-block:: c++

    sight::core::com::slot< void() >::sptr slotStart = sight::core::com::new_slot(&A::start, &a);

Execution of the slots using the run method :

.. code-block:: c++

    slotSum->run(40,2);
    slotStart->run();

Execution of the slots using the method call, which returns the result
of the execution :

.. code-block:: c++

    int result = slotSum->call(40,2);
    slotStart->call();

A slot declaration and execution, through a SlotBase :

.. code-block:: c++

    auto slotLen = std::make_shared<sight::core::com::slot< size_t (std::string) >>( &len );
    sight::core::com::slotBase::sptr slotBaseLen = slotLen;
    sight::core::com::slotBase::sptr slotBaseSum = slotSum;
    slotBaseSum->run(40,2);
    slotBaseSum->run<int, int>(40,2);

    // This one needs the  explicit argument type
    slotBaseLen->run<std::string>("R2D2");
    result = slotBaseSum->call<int>(40,2);
    result = slotBaseSum->call<int, int, int>(40,2);
    result = slotBaseLen->call<size_t, std::string>("R2D2");

Signals
-------

Signals allow to perform grouped calls on slots. For this purpose, a signal
class provides a mechanism to connect slots.

Examples:

The following instruction declares a signal with a void signature.

.. code-block:: c++

    auto sig = std::make_shared<sight::core::com::signal< void() >>();

The connection between a signal and a slot of the same information type:

.. code-block:: c++

    sig->connect(slotStart);

The following instruction will trigger the execution of all
slots connected to this signal:

.. code-block:: c++

    sig->emit();

It is possible to connect multiple slots with the same information type to
the same signal and trigger their simultaneous execution.

Signals can take several arguments as a signature which will trigger their connected slots
by passing the right arguments.

In the following example a signal is declared of type void(int, int). The signal is connected
to two different types of slot, void (int) and int (int, int).

.. code-block:: c++

    using namespace sight::core::com;
    auto sig2 = std::make_shared<signal< void(int, int) >>();
    auto slot1 = std::make_shared<slot< int(int, int) >>(...);
    auto slot2 = std::make_shared<slot< void(int) >>(...);

    sig2->connect(slot1);
    sig2->connect(slot2);

    sig2->emit(21, 42);

Here 2 points need to be highlighted :

-  A signal cannot return a value. Consequently their return type is void.
   Thus, the return value of a slot, triggered by a signal, equally cannot be retrieved.

-  To successfully trigger a slot using a signal, the minimum requirement as to the number of arguments or
   fitting argument types has to be given by the signal. In the last example the slot slot2 only
   requires one argument of type int, but the signal is emitting two arguments of type int.
   Because the signal signature fulfills the slot's argument number and argument type, the signal
   can successfully trigger the slot slot2. The slot slot2 takes the first emitted argument which
   fits its parameter (here 21, the second argument is ignored).


Disconnection
~~~~~~~~~~~~~

The disconnect method is called between one signal and one slot, to stop their existing connection.
A disconnection assumes a signal slot connection. Once a signal slot connection is disconnected, it
cannot be triggered by this signal. Both connection and disconnection of a signal slot connection can be
done at any time.

.. code-block:: c++

    sig2->disconnect(slot1);
    sig2->emit(21, 42); // do not trigger slot1 anymore

The instructions above will cause the execution of slot2. Due to the disconnection between sig2 and slot1,
the slot slot1 is not triggered by sig2.

Connection handling
~~~~~~~~~~~~~~~~~~~

The connection between a slot and a signal returns a connection handler:

.. code-block:: c++

    sight::core::com::Connection connection = signal->connect(slot);

Each connection handler provides a mechanism which allows a
signal slot connection to be disabled temporarily. The slot stays connected to the signal, but it will
not be triggered while the connection is blocked :

.. code-block:: c++

    sight::core::com::Connection::Blocker lock(connection);
    signal->emit();
    // 'slot' will not be executed while 'lock' is alive or until lock is
    // reset

Connection handlers can also be used to disconnect a slot and a signal
:

.. code-block:: c++

    connection.disconnect();
    // slot is not connected anymore

Auto-disconnection
~~~~~~~~~~~~~~~~~~

Slots and signals can handle an automatic disconnection :

-  on slot destruction : every signal slot connection to this slot will be destroyed

-  on signal destruction : every slot connection to the signal will be destroyed

All related connection handlers will be invalidated when an automatic
disconnection occurs.

Manage slots or signals in a class
----------------------------------

The library ``sight::core::com`` provides two helper classes to manage signals or slots in
a structure.

has_slots
~~~~~~~~~~~~~~~~

The class ``has_slots`` offers mapping between a key (string defining the slot name)
and a slot. ``has_slots`` allows the management of many slots using a map. To use
this helper in a class, the class must inherit from ``has_slots`` and must register the slots
in the constructor:

.. code-block:: c++

    struct class_with_slots : public has_slots
    {
      typedef Slot< int()> GetValueSlotType;

      class_with_slots()
      {
          new_slot("sum", &class_with_slots::sum, this);
          new_slot("getValue", &class_with_slots::getValue, this);
      }

      int sum(int a, int b)
      {
          return a+b;
      }

      int getValue()
      {
          return 4;
      }
    };

Then, slots can be used as below :

.. code-block:: c++

    class_with_slots obj;
    obj.slot("sum")->call<int>(5,9);
    obj.slot< class_with_slots::GetValueSlotType >("getValue")->call();

has_signals
~~~~~~~~~~~~~~~~~~

The class ``has_signals`` provides mapping between a key (string defining the signal name) and a signal.
``has_signals`` allows the management of many signals using a map, similar to ``has_slots``. To use this helper in a class, the class must inherit from
``has_signals`` as seen below and must register signals in the constructor:

.. code-block:: c++

    struct class_with_signals : public has_signals
    {
      typedef sight::core::com::Signal< void()> SignalType;

      class_with_signals()
      {
          new_signal< SignalType >("sig");
      }
    };

Then, signals can be used as below:

.. code-block:: c++

    class_with_signals obj;
    Slot< void()>::sptr slot = sight::core::com::new_slot(&anyFunction)
    obj.signal("sig")->connect( slot );
    obj.signal< SignalsTesthas_signals::SignalType >("sig")->emit();
    obj.signal("sig")->disconnect( slot );

Signals and slots used in objects and services
-------------------------------------------------------

Signals are used in both objects and services, whereas slots are only used in services. The abstract
class ``sight::data::object`` inherits from the ``has_signals`` class as a basis to use signals:

.. code-block:: c++

    class Object : public sight::core::com::has_signals
    {
      /// Key in m_signals map of signal m_sigObjectModified
      static const sight::core::com::Signals::SignalKeyType signals::MODIFIED;
      //...

      /// Type of signal m_sigObjectModified
      typedef sight::core::com::Signal< void ( CSPTR( sight::service::ObjectMsg ) ) >
                    ObjectModifiedSignalType;

      /// Signal that emits an ObjectMsg when an object is modified
      ObjectModifiedSignalType::sptr m_sigObjectModified;

      Object()
      {
          m_sigObjectModified = new_signal< ObjectModifiedSignalType >(signals::MODIFIED);
          //...
      }
    }

Moreover the abstract class ``sight::service::base`` inherits from the ``has_slots`` class and the ``has_signals`` class,
as a basis to communicate through signals and slots.
Actually, the methods ``start()``, ``stop()``, ``swap()`` and ``update()`` are all slots.
Here is an extract with ``update()``:

.. code-block:: c++

    class base : public sight::core::com::has_slots, public sight::core::com::has_signals
    {
      using shared_future_t = std::shared_future< void > ;

      /// Key in m_slots map of slot m_slot_update
      static const sight::core::com::slots::key_t slots::UPDATE;

      /// Type of signal m_slot_update
      using update_slot_t = sight::core::com::slot<shared_future_t()> ;

      base()
      {
          //...
          new_slot( slots::UPDATE, &base::update, this ) ;
          //...
      }

      //...
    }


To automatically connect object signals and service slots, it is possible to override the method
``base::auto_connections()``. Please note that to be effective the attribute "auto_connect"
of the service must be set to "true" in the xml configuration (see :ref:`SADAppConfig`).
The default implementation of this method connect the ``signals::MODIFIED`` object signal to the
``slots::UPDATE`` slot.

.. code-block:: c++

    base::KeyConnectionsMap base::auto_connections() const
    {
        return {
            { "data1", sight::data::object::signals::MODIFIED, slots::UPDATE },
            { "data2", sight::data::object::signals::MODIFIED, slots::UPDATE }
        };
    }

Object signals
------------------------

Objects have signals that can be used to inform of modifications.
The base class ``sight::data::object`` has the following signals available.

=============================== ======================================================================
Objects                         Available messages
=============================== ======================================================================
Object                          {``modified``, ``addedFields``, ``changedFields``, ``removedFields``}
=============================== ======================================================================

Thus all objects in Sight can use the previous signals. Some object classes define extra signals.

=============================== ===========================================================================
  Objects                       | Available messages
=============================== ===========================================================================
Composite                       | {``addedObjects``, ``changedObjects``, ``removedObjects``}
Graph                           | {``updated``}
Image                           | {``bufferModified``, ``landmarkAdded``, ``landmarkRemoved``, ``landmarkDisplayed``,
                                | ``distanceAdded``, ``distanceRemoved``, ``distanceDisplayed``, ``sliceIndexModified``,
                                | ``sliceTypeModified``, ``visibilityModified``, ``transparencyModified``}
Mesh                            | {``vertexModified``, ``pointColorsModified``, ``cellColorsModified``,
                                | ``pointNormalsModified``, ``cellNormalsModified``, ``pointTexCoordsModified``,
                                | ``cellTexCoordsModified``}
ModelSeries                     | {``reconstructionsAdded``, ``reconstructionsRemoved``}
PlaneList                       | {``planeAdded``, ``planeRemoved``, ``visibilityModified``}
Plane                           | {``selected``}
PointList                       | {``pointAdded``, ``pointRemoved``}
Reconstruction                  | {``meshModified``, ``visibilityModified``}
ResectionDB                     | {``resectionAdded``, ``safePartAdded``}
Resection                       | {``reconstructionAdded``, ``pointTexCoordsModified``}
Vector                          | {``addedObjects``, ``removedObjects``}
...                             | ...
=============================== ===========================================================================

Proxy
-----

The class ``sight::service::registry::Proxy`` is a communication element and singleton in the architecture.
It defines a proxy for
signal/slot connections. The proxy concept is used to declare
communication channels: all signals registered in a proxy's channel are
connected to all slots registered in the same channel.
This concept is useful to create multiple connections
or when the slots/signals have not yet been created (possible in dynamic programs).

The following shows an example where one signal is connected to several slots:

.. code-block:: c++

    const std::string CHANNEL = "myChannel";

    sight::service::registry::Proxy::sptr proxy
        = sight::service::registry::Proxy::getDefault();

    auto sig = std::make_shared<sight::core::com::signal< void() >>();

    auto slot1 = sight::core::com::new_slot( &myFunc1 );
    auto slot2 = sight::core::com::new_slot( &myFunc2 );
    auto slot3 = sight::core::com::new_slot( &myFunc3 );

    proxy->connect(CHANNEL, sig);

    proxy->connect(CHANNEL, slot1);
    proxy->connect(CHANNEL, slot2);
    proxy->connect(CHANNEL, slot3);

    sig->emit(); // All slots are called
