******************************************
Architecture Description
******************************************

.. toctree::
   :maxdepth: 2

   src/Overview
   src/ObjService
   src/SigSlot
   src/Component
   src/AppConfig
   src/Properties
   src/ErrorHandling
   src/Activities
   src/Thread
   src/Serialization
   src/PatientFolder
   src/GUI
   src/GenericScene
   src/Manager
   src/BufferObjects
